<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable();

            $table->string('email');
            $table->string('full_name');
            $table->string('phone_number');

            $table->tinyInteger('queue_type');
            $table->tinyInteger('boost_type');
            $table->tinyInteger('wins')->default(0);
            $table->tinyInteger('current_rank')->default(0);
            $table->tinyInteger('current_lp')->nullable()->default(0);
            $table->tinyInteger('desired_rank')->nullable()->default(0);

            $table->string('summoner_name', 16);
            $table->string('lol_username')->nullable();
            $table->string('lol_password')->nullable();
            
            $table->enum('role1', ['Top', 'Mid', 'Jungle', 'AD Carry', 'Support'])->nullable();
            $table->enum('role2', ['Top', 'Mid', 'Jungle', 'AD Carry', 'Support'])->nullable();
            $table->enum('spell1', ['Barrier', 'Cleanse', 'Exhaust', 'Flash', 'Ghost', 'Heal', 'Ignite', 'Smite', 'Teleport'])->nullable();
            $table->enum('spell2', ['Barrier', 'Cleanse', 'Exhaust', 'Flash', 'Ghost', 'Heal', 'Ignite', 'Smite', 'Teleport'])->nullable();
            $table->text('champions')->nullable();

            $table->enum('server', ['TR', 'EUW', 'EUNE', 'RU']);
            $table->decimal('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart');
    }
}
