<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_extras', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id');
            $table->enum('role1', ['Top', 'Mid', 'Jungle', 'AD Carry', 'Support'])->nullable();
            $table->enum('role2', ['Top', 'Mid', 'Jungle', 'AD Carry', 'Support'])->nullable();
            $table->enum('spell1', ['Barrier', 'Cleanse', 'Exhaust', 'Flash', 'Ghost', 'Heal', 'Ignite', 'Smite', 'Teleport'])->nullable();
            $table->enum('spell2', ['Barrier', 'Cleanse', 'Exhaust', 'Flash', 'Ghost', 'Heal', 'Ignite', 'Smite', 'Teleport'])->nullable();
            $table->text('champions')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_extras');
    }
}
