<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Models\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use App\Models\Transaction;
use App\Models\Cart;
use App\Models\OrderExtra;
use App\Models\User;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use App\Events\UserManuallyCreated;
use App\Events\OrderCreated;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/* main pages */
Route::get('/', function () {
    return view('index');
})->name('home');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/join_us', function () {
    return view('join_us');
})->name('join_us');

Route::get('/contact_us', function () {
    return view('contact_us');
})->name('contact_us');

Route::post('/contact_us', [\App\Http\Controllers\ContactUsController::class, 'send'])->name('contact_us.send');
Route::post('/join_us', [\App\Http\Controllers\JoinUsController::class, 'send'])->name('join_us.send');

Route::get('/privacy_policy', function () {
    return view('privacy_policy');
})->name('privacy_policy');

Route::get('/terms_of_service', function () {
    return view('terms_of_service');
})->name('terms_of_service');

Route::get('/demopage', function () {
    return view('demopage');
})->name('demopage');

Route::get('/open_orders', function () {
    return view('open_orders');
})->name('open_orders');

Route::get('/ongoing_orders', function () {
    return view('ongoing_orders');
})->name('ongoing_orders');

Route::get('/finished_orders', function () {
    return view('finished_orders');
})->name('finished_orders');

Route::get('/demo_page_chat', function () {
    return view('demo_page_chat');
})->name('demo_page_chat');


Route::get('/boost', [\App\Http\Controllers\CalculatorController::class, 'index'])->name('boost');


/* Cart */
/*Route::name('cart.')->prefix('/cart/')->group(function () {
    Route::post('checkout', [\App\Http\Controllers\CartController::class, 'checkoutPost'])->name('checkout.post');
    Route::get('/', [\App\Http\Controllers\CartController::class, 'index'])->name('index');
    Route::post('/add', [\App\Http\Controllers\CartController::class, 'add'])->name('add');

    Route::post('checkout', [\App\Http\Controllers\CartController::class, 'checkoutPost'])->name('checkout.post');
});
*/

Route::get('/cart', [\App\Http\Controllers\CartController::class, 'index'])->name('cart.index');
Route::post('/cart', [\App\Http\Controllers\CartController::class, 'add'])->name('cart.add');
Route::post('/cart/checkout', [\App\Http\Controllers\CartController::class, 'checkoutPost'])->name('cart.checkout.post');
Route::get('/cart/checkout', [\App\Http\Controllers\CartController::class, 'checkout'])->name('cart.checkout.index');

/* End Cart */









Route::get('payment_success', function (Request $request) {
    
    session()->forget('cart');
    session()->forget('cartID');

    return view('paymentsuccess');
})->name('paymentsuccess');

Route::get('payment_error', function (Request $request) {

    print_r($request->all());
    return view('paymenterror');
})->name('paymenterror');



Route::post('payment_callback', function (Request $request) {
    $merchant_key     = 'rq2Ckdh3zzcWS2Ss';
    $merchant_salt    = '7Pd2undrARfxhz4A';

    $hash = base64_encode(hash_hmac('sha256', $request->input('merchant_oid') . $merchant_salt . $request->input('status') . $request->input('total_amount'), $merchant_key, true));

    if ($hash != $request->input('hash')) {
        die('PAYTR notification failed: bad hash');
    }

    if ($request->input('status') == 'success') {
        $trans_id = preg_replace("/[a-zA-Z\s-]/", "", $request->input('merchant_oid'));
        $transaction = Transaction::where('id', $trans_id)->where('status', 'pending')->first();

        if($transaction){

            $cart = Cart::find($transaction->cart_id);
            if($cart) {

                $tiedUser = User::where('email', $cart->email)->first();
                //create user
                if (!$tiedUser) {
                    $randPass = Str::random(10);
                    $user = User::create([
                            'name' => $cart->full_name,
                            'email' => $cart->email,
                            'password' => Hash::make($randPass),
                        ]);
                    $userId = $user->id;

                    UserManuallyCreated::dispatch($cart->email, $randPass);
                    OrderCreated::dispatch($cart->email);
                } else {
                    OrderCreated::dispatch($cart->email);
                    $userId = $tiedUser->id;
                }
                $order = new Order;

                $order->user_id       = $userId;
                $order->email         = $cart->email;
                $order->full_name     = $cart->full_name;
                $order->phone_number  = $cart->phone_number;

                $order->queue_type    = $cart->queue_type;
                $order->boost_type    = $cart->boost_type;
                $order->wins          = $cart->wins;
                $order->current_rank  = $cart->current_rank;
                $order->current_lp    = $cart->current_lp;
                $order->desired_rank  = $cart->desired_rank;

                $order->summoner_name = $cart->summoner_name;
                $order->lol_username      = $cart->lol_user;
                $order->lol_password      = $cart->lol_pass;

                $order->server        = $cart->server;
                $order->price         = ($request->input('payment_amount') / 100);

                $order->save();

                $orderExtras = new OrderExtra;
                $orderExtras->order_id     = $order->id;
                $orderExtras->role1        = $cart->role1;
                $orderExtras->role2        = $cart->role2;
                $orderExtras->spell1       = $cart->spell1;
                $orderExtras->spell2       = $cart->spell2;
                $orderExtras->champions    = $cart->champions;

                $orderExtras->save();

                $cart->delete();
                $transaction->status = "completed";
                $transaction->save();
            }
        }
    } else if($request->input('status') == "failed") {
        $trans_id = preg_replace("/[a-zA-Z\s-]/", "", $request->input('merchant_oid'));
        $transaction = Transaction::where('id', $trans_id)->where('status', 'pending')->first();
        if($transaction) {

            //email them about failure

            if($request->input('failed_reason_code') == 6) {
                $transaction->status = "abandoned";
                $transaction->status_number = $request->input('failed_reason_code');
                $transaction->save();
            }else {
                $transaction->status = "failed";
                $transaction->status_number = $request->input('failed_reason_code');
                $transaction->save();
            }
        }
    }

    return response('OK', 200);
    
});


Route::get('/m/{boostId}', [\App\Http\Controllers\ChatsController::class, 'getMessages'])->name('boostMessaging');
//Route::get('messages/{boostId}', [\App\Http\Controllers\ChatsController::class, 'fetchMessages']);
Route::post('messages/{boostId}', [\App\Http\Controllers\ChatsController::class, 'sendMessage']);



Route::get('/demopage', function () {
    return view('demopage');
})->name('demopage');

Route::get('/open_orders', function () {
    return view('open_orders');
})->name('open_orders');

Route::get('/ongoing_orders', function () {
    return view('ongoing_orders');
})->name('ongoing_orders');

Route::get('/finished_orders', function () {
    return view('finished_orders');
})->name('finished_orders');

Route::get('/demo_page_chat', function () {
    return view('demo_page_chat');
})->name('demo_page_chat');


/*
Route::get('/checkout/userinfo', function(Request $request) {


    $request->session()->reflash();

    if($request->old('serializedState')){
        $request->session()->flash('serializedState', $request->old('serializedState'));
    }
    

    return view('checkout-userinfo')->with([
        'serializedState' => $request->session()->get('serializedState'),
        'duoSelected' => $request->session()->get('duoSelected'),
    ]);
});
Route::post('/checkout/userinfo', [\App\Http\Controllers\CalculatorController::class, 'checkoutUserInfo'])->name('checkout.userinfo');
Route::post('/checkout', [\App\Http\Controllers\CalculatorController::class, 'checkoutPost'])->name('checkout.submit');
Route::get('/checkout', [\App\Http\Controllers\CalculatorController::class, 'checkout'])->name('checkout');

*/

Route::middleware(['auth'])->group(function() {

    //dashboard home
    Route::get('/dashboard', function () {
        return view('dashboard.index');
    })->name('dashboard.home');

    Route::middleware('role:normal')->group(function() {
        Route::get('orders', [\App\Http\Controllers\OrderController::class, 'index'])->name('orders.index');
    });

    //all admin routes
    Route::prefix('admin')->name('admin.')->middleware(['role:admin'])->group(function() {

        //order routes
        Route::get('orders/{order}/delete', [\App\Http\Controllers\Admin\OrderController::class, 'delete'])->name('orders.delete');
        Route::get('orders/finished', [\App\Http\Controllers\Admin\OrderController::class, 'finished'])->name('orders.finished');
        Route::get('orders/{order}/assign', [\App\Http\Controllers\Admin\OrderController::class, 'assign'])->name('orders.assign');
        Route::post('orders/{order}/assign', [\App\Http\Controllers\Admin\OrderController::class, 'assignSubmit'])->name('orders.assign.submit');
        Route::resource('orders', \App\Http\Controllers\Admin\OrderController::class);
        //end order routes

        //list all ongoing boosts
        Route::get('boosts', [\App\Http\Controllers\Admin\BoostsController::class, 'index'])->name('boosts.index');
        Route::patch('boosts/{boost}/takeaway', [\App\Http\Controllers\Admin\BoostsController::class, 'takeaway'])->name('boosts.takeaway');

        Route::get('user-management', [\App\Http\Controllers\Admin\UserManagementController::class, 'index'])->name('user-management.index');

        Route::get('financial', [\App\Http\Controllers\Admin\FinancialController::class, 'index'])->name('financial.index');
        Route::get('financial/{user}/edit', [\App\Http\Controllers\Admin\FinancialController::class, 'edit'])->name('financial.edit');
        Route::patch('financial/{user}', [\App\Http\Controllers\Admin\FinancialController::class, 'update'])->name('financial.update');

        //user Routes
        Route::get('user-roles', [\App\Http\Controllers\Admin\UserRolesController::class, 'index'])
           ->name('user-roles.index');
        Route::get('user-roles/{user}/edit', [\App\Http\Controllers\Admin\UserRolesController::class, 'edit'])
           ->name('user-roles.edit');
        Route::patch('user-roles/{user}', [\App\Http\Controllers\Admin\UserRolesController::class, 'update'])
           ->name('user-roles.update');
    });
    //end admin routes


    //all booster routes
    Route::prefix('booster')->name('booster.')->middleware('role:booster')->group(function() {
       Route::get('financial', function() {
           return view('dashboard.booster.financial.index');
       })->name('financial.index');

       Route::get('orders', [\App\Http\Controllers\Booster\OrderController::class, 'index'])->name('orders.index');
       Route::post('orders', [\App\Http\Controllers\Booster\OrderController::class, 'store'])->name('orders.store');
       Route::get('orders/ongoing', [\App\Http\Controllers\Booster\OrderController::class, 'ongoingOrders'])->name('orders.ongoing');
       Route::patch('orders/{order}/cancel', [\App\Http\Controllers\Booster\OrderController::class, 'cancel'])->name('orders.cancel');
       Route::patch('orders/{order}/finish', [\App\Http\Controllers\Booster\OrderController::class, 'finish'])->name('orders.finish');
       Route::get('orders/finished', [\App\Http\Controllers\Booster\OrderController::class, 'finished'])->name('orders.finished');
    });
    //end booster routes

});

require __DIR__.'/auth.php';
