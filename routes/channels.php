<?php

use Illuminate\Support\Facades\Broadcast;
use App\Models\Boost;
use App\Models\User;
use Illuminate\Support\Facades\Log;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.Models.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('chat.{boostId}', function ($user, $boostId) {

    $boost = Boost::find($boostId);
    if($user->id === $boost->order->user_id || $user->id === $boost->booster_id) {
        return true;
    }
    return false;
});

Broadcast::channel('messages.{userId}', function ($user, $userId) {

    if ($user->id === $userId) {
        return true;
    }
    return false;
});
