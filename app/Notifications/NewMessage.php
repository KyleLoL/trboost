<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewMessage extends Notification
{
    use Queueable;

    private $boostId;
    private $from;
    private $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($boostId, $from, $message)
    {
        $this->boostId = $boostId;
        $this->from = $from;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'boostId' => $this->boostId,
            'message' => $this->message,
            'from' => $this->from
        ];
    }
}
