<?php

namespace App\Listeners;

use App\Events\UserManuallyCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\UserCreatedMail;
use Illuminate\Support\Facades\Mail;

class SendUserCreatedEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserCreated  $event
     * @return void
     */
    public function handle(UserManuallyCreated $event)
    {
        Mail::to($event->email)->send(new UserCreatedMail($event->email, $event->pass));
    }
}
