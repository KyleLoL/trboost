<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'email',
        'full_name',
        'summoner_name',
        'lol_username',
        'lol_password',
        'queue_type',
        'boost_type',
        'wins',
        'current_rank',
        'desired_rank',
        'role1',
        'role2',
        'spell1',
        'spell2',
        'champions',
        'price'
    ];

    protected $table = 'cart';
}
