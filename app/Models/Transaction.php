<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'cart_id',
        'status',
        'status_number'
    ];

    protected $table = 'transaction';


    public function cart()
    {
        return $this->belongsToMany(Cart::class, 'id', 'cart_id');
    }
}
