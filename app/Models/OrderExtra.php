<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderExtra extends Model
{
    protected $fillable = [
        'order_id',
        'role1',
        'role2',
        'spell1',
        'spell2',
        'premium',
        'streaming',
        'offline'
    ];
    public $timestamps = false;
    protected $table = 'order_extras';

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
