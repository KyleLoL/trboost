<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RankDivision extends Model
{
    protected $fillable = [
        'number'
    ];

    public $timestamps = false;
    protected $table = 'rank_divisions';
}
