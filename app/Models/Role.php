<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name',
    ];

    public $timestamps = false;
    protected $table = 'roles';

    public function user()
    {
        $this->hasOne(User::class);
    }

}
