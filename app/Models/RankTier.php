<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RankTier extends Model
{
    protected $fillable = [
        'name',
    ];

    public $timestamps = false;
    protected $table = 'rank_tiers';
}
