<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rank extends Model
{
    protected $fillable = [
        'tier_id',
        'division_id',
        'price',
    ];

    public $timestamps = false;
    protected $table = 'ranks';


    public function rankTier()
    {
        return $this->hasOne(RankTier::class, 'id', 'tier_id');
    }
    public function rankDivision()
    {
        return $this->hasOne(RankDivision::class, 'id', 'division_id');
    }

    public function getRank()
    {
        if($this->tier_id <= 6)
            return $this->rankTier->name.' '.$this->rankDivision->number;

        return $this->rankTier->name;
    }
}
