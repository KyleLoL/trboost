<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Boost extends Model
{
    protected $fillable = [
        'order_id',
        'booster_id',
        'status',
    ];

    public $timestamps = true;
    protected $table = 'boosts';

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'booster_id');
    }
    public function messages()
    {
        return $this->hasMany(Message::class);
    }
}
