<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'email',
        'queue_type',
        'boost_type',
        'wins',
        'current_rank',
        'desired_rank',
        'price',
        'summoner_name'
    ];

    protected $table = 'orders';

    public function getStatusAttribute($value)
    {
        switch($value) {
            case 0:
                return "open";
                break;

            case 1:
                return "paused";
                break;

            case 2:
                return "finished";
                break;
        }
    }

    public function getBoosterPriceAttribute()
    {
        return $this->price * 0.4;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function orderExtra()
    {
        return $this->hasOne(OrderExtra::class);
    }
    public function boosts()
    {
        return $this->hasMany(Boost::class);
    }

    public function currentRank()
    {
        return $this->hasOne(Rank::class, 'id', 'current_rank');
    }
    public function desiredRank()
    {
        return $this->hasOne(Rank::class, 'id', 'desired_rank');
    }
}
