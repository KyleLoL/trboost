<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, string $roleName)
    {
        $user = Auth::user();
        if($roleName == 'normal' && $user->role_id == 0) {
            return $next($request);
        }
        if(strtolower($user->role->name) == strtolower($roleName)) {
            return $next($request);
        }

        return redirect(route('dashboard.home'));
    }
}
