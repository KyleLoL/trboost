<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Requests\StoreCartRequest;
use App\Http\Requests\CartCheckoutRequest;
use App\Models\Transaction;
use App\Models\Cart;
use App\Models\Rank;

class CartController extends Controller
{
    public function index()
    {

        if(session('cartID')) {
            $cart = Cart::find(session('cartID'));

            if(!$cart) {
                session()->forget('cartID');
                $cart = null;
            }
        }else if(session('cart')) {
            $cart = json_decode(session('cart')) ?: null;
            if(is_null($cart)) {
                session()->forget('cart');
            }else {
                $cart = (array) $cart;
            }
        }else { $cart = null; }

        if(is_null($cart)) {
            return view('cart.index')->with([
                'cart' => null
            ]);
        }


        if ($cart['current_rank'] != 0) {
            $currentRank = Rank::find($cart['current_rank']);

            if (!is_null($currentRank)) {
                $currentRank = $cart['boost_type'] == 2 ? $currentRank->rankTier->name : $currentRank->getRank();
            }
        } else {
            $currentRank = "Unranked";
        }

        $desiredRank = Rank::find($cart['desired_rank']);
        if (!is_null($desiredRank)) {
            $desiredRank = $desiredRank->getRank();
        }else {
            $desiredRank = null;
        }


        //all roles
        $roles = ['Top', 'Mid', 'Jungle', 'AD Carry', 'Support'];

        //all spells
        $spells = ['Barrier','Cleanse','Exhaust','Flash','Ghost','Heal','Ignite','Smite','Teleport'];


        try {
            $price = !is_null($cart) ? $this->calculatePrice($cart) : null;
        } catch (\Exception $e) {
            die('error: '.$e->getMessage());
            return;
        }


        return view('cart.index')->with([
            'cart'        => $cart,
            'currentRank' => $currentRank,
            'desiredRank' => $desiredRank,
            'roles'       => $roles,
            'spells'      => $spells,
            'price'       => $price,
        ]);
        
    }

    public function add(StoreCartRequest $request)
    {
        switch($request->input('boostType')) {
            case 1:
                $currentRank = Rank::where('tier_id', $request->input('currentTier'))->where('division_id', $request->input('currentDivision'))
                    ->first()->id;
                
                $desiredRank = Rank::where('tier_id', $request->input('desiredTier'))->where('division_id', $request->input('desiredDivision'))
                ->first()->id;
            break;

            case 2:
                if(is_null($request->input('currentTier'))) {
                    $currentRank = 0;
                }else {
                    $currentRank = Rank::where('tier_id', $request->input('currentTier'))->where('division_id', 1)
                    ->first()->id;
                }
                $desiredRank = 0;
            break;

            case 3:
                $currentRank = Rank::where('tier_id', $request->input('currentTier'))->where('division_id', $request->input('currentDivision'))
                ->first()->id;
                $desiredRank = 0;
            break;
        }


        if($request->input('soloType') == 1) {
            $queueType = $request->input('queueType') == 1 ? 1 : 3;
        }else {
            $queueType = $request->input('queueType') == 1 ? 2 : 4;
        }

        if($request->input('soloType') == 1) {
            $role1 = $request->input('extraRoles.0') ?: null;
            $role2 = $request->input('extraRoles.1') ?: null;

            $spell1 = $request->input('extraSpells.0') ?: null;
            $spell2 = $request->input('extraSpells.1') ?: null;
            $champions = null;
        }else {
            $role1 = null;
            $role2 = null;
            $spell1 = null;
            $spell2 = null;
            $champions = null;
        }




        $data = [
            'server'        => $request->input('server'),
            'queue_type'    => $queueType,
            'boost_type'    => $request->input('boostType'),
            'wins'          => $request->input('boostType') > 1 ? $request->input('netWins') : 0,
            'current_lp'     => $request->input('boostType') == 1 ? $request->input('currentLP') : 0,
            'current_rank'  => $currentRank,
            'desired_rank'  => $desiredRank,

            'role1'         => $role1,
            'role2'         => $role2,
            'spell1'        => $spell1,
            'spell2'        => $spell2,
            'champions'     => $champions
        ];

        if($request->session()->exists('cartID')) {
            $cart = Cart::find($request->session()->get('cartID'));

            if($cart) {

                $cart->queue_type = $data['queue_type'];
                $cart->boost_Type = $data['boost_type'];
                $cart->wins = $data['wins'];
                $cart->current_rank = $data['current_rank'];
                $cart->current_lp = $data['current_lp'];
                $cart->desired_rank = $data['desired_rank'];

                $cart->role1 = $data['role1'];
                $cart->role2 = $data['role2'];

                $cart->spell1 = $data['spell1'];
                $cart->spell2 = $data['spell2'];
                $cart->champions = $data['champions'];

                $cart->server = $data['server'];
                $cart->price = $this->calculatePrice($data);

                $cart->save();
                if($request->session()->exists('cart')) {
                    $request->session()->forget('cart');
                }
            }else {
                $request->session()->forget('cartID');
                $request->session()->put('cart', json_encode($data));
            }

        }else {
            $request->session()->put('cart', json_encode($data));
        }


        /*
        $cart = Cart::findOrNew($request->session()->get('cartID'));

        if(Auth::check()) {
            $cart->user_id = Auth::id();
        }

        $cart->queue_type = $queueType;
        $cart->boost_type = $request->input('boost_type');
        $cart->wins = $request->input('netWins') ?: 0;
        $cart->current_rank = $currentRank;
        $cart->desired_rank = $desiredRank;
        */

        /* Extras */


        /*
        $cart->save();
        $request->session()->put('cartID', $cart->id);
        */

        return redirect()->route('cart.index');
    }

    public function checkout()
    {
        if(session('cartID')) {
            $cart = Cart::find(session('cartID'));
            if($cart) {
                try {
                    $trans_id = Transaction::where('cart_id', $cart->id)->where('status', 'pending')->orderByDesc('created_at')->firstOrFail();
                } catch (\Exception $e) {
                    return redirect()->route('cart.index');
                }
                $token = $this->callPayTR($cart, $trans_id->id);
            }
        }

        if(!isset($token)) {
            die('error');
        }

        return view('checkout')->with([
            'iframeToken' => $token
        ]);
    }

    public function checkoutPost(CartCheckoutRequest $request)
    {

        if($request->session()->exists('cartID')) {

            $cart = Cart::find($request->session()->get('cartID'));
            if($cart) {

                $cart->email = $request->input('email');
                $cart->full_name = $request->input('fullname');
                $cart->phone_number = $request->input('phone');
                $cart->summoner_name = $request->input('summoner_name');
                $cart->lol_username = $request->has('lol_user') ? $request->input('lol_user') : null;
                $cart->lol_password = $request->has('lol_pass') ? $request->input('lol_pass') : null;
                
                //generate new transaction ID
                $transaction = new Transaction;
                $transaction->cart_id = $cart->id;
                $transaction->save();

                return redirect()->route('cart.checkout.index');
            }
        }
        try {
            $price = $this->calculatePrice($request->except('_token'));
            $cart = new Cart;
            $cart->email = $request->input('email');
            $cart->full_name = $request->input('fullname');
            $cart->phone_number = $request->input('phone');
            $cart->summoner_name = $request->input('summoner_name');
            $cart->lol_username = $request->has('lol_user') ? $request->input('lol_user') : null;
            $cart->lol_password = $request->has('lol_pass') ? $request->input('lol_pass') : null;

            $cart->queue_type = $request->input('queue_type');
            $cart->boost_Type = $request->input('boost_type');
            $cart->wins = in_array($request->input('boost_type'), array(2,3)) ? $request->input('wins') : 0;
            $cart->current_rank = $request->input('current_rank');
            $cart->current_lp = $request->has('current_lp') ? $request->input('current_lp') : 0;
            $cart->desired_rank = $request->has('desired_rank') ? $request->input('desired_rank') : null;

            $cart->role1 = $request->has('role1') ? $request->input('role1') : null;
            $cart->role2 = $request->has('role2') ? $request->input('role2') : null;

            $cart->spell1 = $request->has('spell1') ? $request->input('spell1') : null;
            $cart->spell2 = $request->has('spell2') ? $request->input('spell2') : null;
            $cart->champions = $request->has('champions') ? $request->input('champions') : null;

            $cart->server = $request->input('server');
            $cart->price = $price;

            $cart->save();

            //generate new transaction ID
            $transaction = new Transaction;
            $transaction->cart_id = $cart->id;
            $transaction->save();

            $request->session()->put('cartID', $cart->id);
            $request->session()->forget('cart');

        } catch (\Exception $e) {
            die('error: ' . $e->getMessage());
            return;
        }
        
        return redirect()->route('cart.checkout.index');
    }

    private function calculatePrice($data)
    {

        function addPercentage($total, $percent)
        {
            return $total + ($total / 100) * $percent;
        }

        //initial settings
        $price = 0;
        $lpDiscount = 0; // 0%
        $serverPercent = 0; // 0%


        //get server percentage
        if ($data['server'] != "TR") $serverPercent = 50; // 50%

        //if division boost
        if ($data['boost_type'] == 1) {

            //set division boost settings
            if ($data['server'] != "TR" && in_array($data['queue_type'], array(2,4))) {
                $serverPercent = 70; // 70%
            }
            $duoPercent = 200; // 200%
            $ranks = Rank::all();
            $prices = [];

            //populate prices array
            foreach ($ranks as $rank) {
                $prices[] = $rank->price;
            }

            //if masters is selected...default division is just 1
            if (!isset($data['desiredDivision'])) {
                $data['desiredDivision'] = 1;
            }

            //get current and desired rank ID
            $currentRankID = $data['current_rank'];
            $desiredRankID = $data['desired_rank'];

            //techinically $currentRankID in the context of $prices array is +1,
            //we don't want the true starting rank so this is fine
            for ($i = $currentRankID; $i < $desiredRankID; $i++) {

                //calculate LP discount for first rank (if start is iron 4, we discount iron 3...iron4 > iron3)
                if ($i == $currentRankID) {
                    if ($data['current_lp'] >= 20 && $data['current_lp'] <= 39) {
                        $lpDiscount = -10; // -10% discount
                    } else if ($data['current_lp'] >= 40 && $data['current_lp'] <= 59) {
                        $lpDiscount = -20; // -20% discount
                    } else if ($data['current_lp'] >= 60 && $data['current_lp'] <= 79) {
                        $lpDiscount = -30; // -30% discount
                    } else if ($data['current_lp'] >= 80) {
                        $lpDiscount = -40; // -40% discount
                    }
                } else {
                    $lpDiscount = 0;
                }

                //get discounted price based on LP
                $discounted = addPercentage($prices[$i], $lpDiscount);


                //apply euw specific pricings
                if ($data['server'] == "EUW" && $i >= 21) {

                    switch ($i) {
                        case 21:
                            $euwPercent = 150; //increase d4 -> d3 by 150%
                            break;
                        case 22:
                            $euwPercent = 235; //increase d3 -> d2 by 235%
                            break;
                        case 23:
                            $euwPercent = 312; //increase d2 -> d1 by 312%
                            break;
                        case 24:
                            $euwPercent = 325; //increase d1 -> master by 325%
                            break;
                    }
                    $price += addPercentage($discounted, $euwPercent);
                    continue;
                }


                $price += addPercentage($discounted, $serverPercent);
            } //end for loop


        } else if ($data['boost_type'] == 2) {
            //set placement boost settings
            $duoPercent = 20; // 20%
            if($data['current_rank'] != 0) {
                $currentRank = Rank::find($data['current_rank'])->tier_id;
            }else {
                $currentRank = 0;
            }
            $placementPrice = $currentRank > 0 ? DB::table('placement_prices')
            ->where('tier_id', $currentRank)->first()->price : 9; //9 is for unranked 

            $price = addPercentage($placementPrice * $data['wins'], $serverPercent);
        } else if ($data['boost_type'] == 3) {

            $currentRank = Rank::find($data['current_rank']);
            //set netwins boost settings
            if ($data['server'] == "EUW" && $currentRank->tier_id == 6) { //6 = diamond tier
                $serverPercent = 185; // 185%
            }
            $duoPercent = 50; // 50%

            //get current rank ID
            $currentRankID = $currentRank->id;
            $netwinsPrice = DB::table('net_prices')->where('rank_id', $currentRankID)->first()->price;

            $price = addPercentage($netwinsPrice * $data['wins'], $serverPercent);
        }

        //if solo
        if (in_array($data['queue_type'], array(1,3))) {
            if ($data['role1']) {
                $roleMultiplier = 1;
                switch ($data['role1']) {
                    case 1:
                        $roleMultiplier = 1.25;
                        break;
                    case 2:
                        $roleMultiplier = 1.20;
                        break;
                    case 3:
                        $roleMultiplier = 1.20;
                        break;
                    case 4:
                        $roleMultiplier = 1.30;
                        break;
                    case 5:
                        $roleMultiplier = 1.30;
                        break;
                }
                $price *= $roleMultiplier;
            }

            if ($data['spell1']) {
                $price += 10;
            }
        } else if (in_array($data['queue_type'], array(2,4))) { //if duo is selected..apply duo percentages
            $price = addPercentage($price, $duoPercent);
        }


        return number_format($price, 2, '.', '');
    }

    private function callPayTR($cart, $trans_id)
    {
        $merchant_id     = 245863;
        $merchant_key     = 'rq2Ckdh3zzcWS2Ss';
        $merchant_salt    = '7Pd2undrARfxhz4A';

        $email = $cart['email'];
        $payment_amount    = $cart->price * 100;
        $merchant_oid = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 4) . "test". $trans_id;
        $user_name = $cart->full_name;
        $user_address = "-";
        $user_phone = $cart->phone_number;
        $merchant_ok_url = route('paymentsuccess');
        $merchant_fail_url = route('paymenterror');

        $user_basket = base64_encode(json_encode(array(
            array("Elo Boosting Service", $cart->price, 1), // 1st Product (Product Name - Unit Price - Piece)
        )));

        if (isset($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else {
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        $user_ip = $ip;
        $timeout_limit = "5";
        $debug_on = 1;
        $test_mode = 1;
        $no_installment    = 1;
        $max_installment = 0;
        $currency = "";

        $hash_str = $merchant_id . $user_ip . $merchant_oid . $email . $payment_amount . $user_basket . $no_installment . $max_installment . $currency . $test_mode;
        $paytr_token = base64_encode(hash_hmac('sha256', $hash_str . $merchant_salt, $merchant_key, true));

        $post_vals = array(
            'merchant_id' => $merchant_id,
            'user_ip' => $user_ip,
            'merchant_oid' => $merchant_oid,
            'email' => $email,
            'payment_amount' => $payment_amount,
            'paytr_token' => $paytr_token,
            'user_basket' => $user_basket,
            'debug_on' => $debug_on,
            'no_installment' => $no_installment,
            'max_installment' => $max_installment,
            'user_name' => $user_name,
            'user_address' => $user_address,
            'user_phone' => $user_phone,
            'merchant_ok_url' => $merchant_ok_url,
            'merchant_fail_url' => $merchant_fail_url,
            'timeout_limit' => $timeout_limit,
            'currency' => $currency,
            'test_mode' => $test_mode
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.paytr.com/odeme/api/get-token");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vals);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        //XXX: DİKKAT: lokal makinanızda "SSL certificate problem: unable to get local issuer certificate" uyarısı alırsanız eğer
        //aşağıdaki kodu açıp deneyebilirsiniz. ANCAK, güvenlik nedeniyle sunucunuzda (gerçek ortamınızda) bu kodun kapalı kalması çok önemlidir!
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $result = @curl_exec($ch);

        if (curl_errno($ch)){
            throw ValidationException::withMessages([
                'PayTR' => [
                    'There has been an error connecting to the payment server.',
                ],
            ]);
            
            //throw new \Exception("PAYTR IFRAME connection error. err:" . curl_error($ch));
            //need to log this somewhere
        }

        curl_close($ch);

        $result = json_decode($result, 1);

        if ($result['status'] == 'success') 
        {
            $token = $result['token'];
        } else {
            die("PAYTR IFRAME failed. reason:" . $result['reason']);
        }

        //throw new \PayTRException("PAYTR IFRAME failed. reason:" . $result['reason']);
        //need to log this somewhere

        return $token;
    }
}