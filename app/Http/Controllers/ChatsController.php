<?php

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Notifications\NewMessage;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Boost;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ChatsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getMessages($boostId)
    {


        $notifs = Auth::user()->notifications()->whereRaw('SUBSTR(data, 13, 1) = '.$boostId.'')->update(['read_at' => now()]);

        $weekAgo = Carbon::today()->sub(7, 'days');
        $boosterName = User::find(Boost::find($boostId)->booster_id)->name;
        $messages = Message::where('boost_id', $boostId)->orderBy('created_at')->get();
        return view('chat')->with([
            'weekAgo' => $weekAgo,
            'boosterName' => $boosterName,
            'boostId' => $boostId,
            'messages' => $messages
        ]);
    }

    public function fetchMessages($boostId)
    {
        return Message::where('boost_id', $boostId)->get();
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request, $boostId)
    {
        $user = Auth::user();

        $message = $user->messages()->create([
            'boost_id' => $boostId,
            'message' => $request->input('message'),
        ]);

        $date = $message->created_at->format('g:i A, ').'Today';
        $name = $user->name;
        $messageText = $message->message;

        broadcast(new MessageSent($date, $name, $messageText, $boostId))->toOthers();

        $boost = Boost::find($boostId);
        if($boost->booster_id == Auth::id()) {
            $recipient = User::find($boost->order->user_id);
        }else {
            $recipient = User::find($boost->booster_id);
        }
    
        $recipient->notify(new NewMessage($boostId, Auth::user()->name, $messageText));

        return ['status' => 'Message Sent!'];
    }
}
