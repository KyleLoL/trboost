<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Models\RankTier;
use App\Models\RankDivision;
use App\Models\Rank;
use App\Models\Order;
use App\Models\OrderExtra;
use App\Models\Boost;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderCreated;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //open orders count
        $openOrdersCount = Order::whereDoesntHave('boosts', function (Builder $query) {
            $query->where('status', '=', 2)->orWhere('status', '=', 0);
        })->count();

        $orders = Order::whereDoesntHave('boosts', function (Builder $query) {
            $query->where('status', '=', 2)->orWhere('status', '=', 0);
        })->get();


        //ongoing boosts count
        $ongoingBoostsCount = Boost::where('status', 0)->count();

        //finished boosts count
        $finishedBoostsCount = Boost::where('status', 2)->count();

        return view('dashboard.admin.orders.index')->with([
            'orders' => $orders,
            'openOrderCount' => $openOrdersCount,
            'boostsCount' => $ongoingBoostsCount,
            'finishedBoostsCount' => $finishedBoostsCount,
        ]);
    }

    public function assign($id)
    {
        $boosters = User::where('role_id', 1)->get();
        return view('dashboard.admin.orders.assign')->with([
            'boosters' => $boosters,
            'orderID' => $id
        ]);
    }
    public function assignSubmit(Request $request, $id)
    {
        $boost = new Boost;
        $boost->order_id = $id;
        $boost->booster_id = $request->input('booster_id');
        $boost->save();

        return redirect()->route('admin.boosts.index')->with('status', 'Order has been assigned to booster!');
    }

    public function finished()
    {
        //open orders count
        $openOrdersCount = Order::whereDoesntHave('boosts', function (Builder $query) {
            $query->where('status', '=', 2)->orWhere('status', '=', 0);
        })->count();

        //ongoing boosts count
        $ongoingBoostsCount = Boost::where('status', 0)->count();

        //finished boosts count
        $finishedBoostsCount = Boost::where('status', 2)->count();
        $finishedOrders = Order::whereHas('boosts', function (Builder $query) {
            $query->where('status', 2);
        })->get();

        return view('dashboard.admin.orders.finished')->with([
            'orders' => $finishedOrders,
            'openOrderCount' => $openOrdersCount,
            'boostsCount' => $ongoingBoostsCount,
            'finishedBoostsCount' => $finishedBoostsCount
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.admin.orders.create')->with([
            'rankTiers' => RankTier::all(),
            'rankDivisions' => RankDivision::all(),
            'ranks' => Rank::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $currentRank = Rank::where('tier_id', $request->input('currentTier'))
            ->where('division_id', $request->input('currentDivision'))->first();

        $tiedUser = User::where('email', $request->input('email'))->first();
        //create user
        if(!$tiedUser) {
            $randPass = Str::random(10);
            $user = User::create([
                'name' => $request->input('summonerName'),
                'email' => $request->input('email'),
                'password' => Hash::make($randPass),
            ]);
            $userId = $user->id;

            Mail::to($request->input('email'))->send(new OrderCreated($request->input('email'), $randPass));
            //event(new Registered($user));
        }else {
            $userId = $tiedUser->id;
        }

        $order = new Order;
        $order->user_id = $userId;
        $order->email = $request->input('email');
        $order->queue_type = $request->input('queue');
        $order->boost_type = $request->input('boost');
        $order->current_rank = $currentRank->id;
        if($request->input('boost') == 1) {
            $desiredRank = Rank::where('tier_id', $request->input('desiredTier'))
                ->where('division_id', $request->input('desiredDivision'))->first();

            $order->desired_rank = $desiredRank->id;
        }else {
            $order->wins = $request->amountWins;
        }
        $order->price = $request->input('price');
        $order->summoner_name = $request->input('summonerName');
        $order->username = $request->input('username');
        $order->password = $request->input('password');
        $order->save();

        //if any extras were set



        $orderExtra = new OrderExtra;
        $orderExtra->order_id = $order->id;
        $orderExtra->role1 = $request->input('roles1') ?: null;
        $orderExtra->role2 = $request->input('roles2') ?: null;
        $orderExtra->spell1 = $request->input('spell1') ?: null;
        $orderExtra->spell2 = $request->input('spell2') ?: null;
        $orderExtra->premium = $request->input('premium') ? true : false;
        $orderExtra->streaming = $request->input('streaming') ? true : false;
        $orderExtra->offline = $request->input('offline') ? true : false;

        $orderExtra->save();





        return redirect()->route('admin.orders.index')->with('status', 'Order Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('dashboard.admin.orders.edit')->with([
            'order' => Order::find($id),
            'rankTiers' => RankTier::all(),
            'rankDivisions' => RankDivision::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currentRank = Rank::where('tier_id', $request->input('currentTier'))
            ->where('division_id', $request->input('currentDivision'))->first();


        $order = Order::find($id);
        $order->queue_type = $request->input('queue');
        $order->boost_type = $request->input('boost');
        $order->current_rank = $currentRank->id;
        if($request->input('boost') == 1) {
            $desiredRank = Rank::where('tier_id', $request->input('desiredTier'))
                ->where('division_id', $request->input('desiredDivision'))->first();

            $order->desired_rank = $desiredRank->id;
        }else {
            $order->desired_rank = null;
            $order->wins = $request->amountWins;
        }
        $order->price = $request->input('price');
        $order->summoner_name = $request->input('summonerName');
        $order->username = $request->input('username');
        $order->password = $request->input('password');
        $order->save();

        $orderExtra = OrderExtra::find($order->id);
        $orderExtra->role1 = $request->input('roles1') ?: null;
        $orderExtra->role2 = $request->input('roles2') ?: null;
        $orderExtra->spell1 = $request->input('spell1') ?: null;
        $orderExtra->spell2 = $request->input('spell2') ?: null;
        $orderExtra->premium = $request->input('premium') ? true : false;
        $orderExtra->streaming = $request->input('streaming') ? true : false;
        $orderExtra->offline = $request->input('offline') ? true : false;

        $orderExtra->save();

        return redirect()->route('admin.orders.index')->with('status', 'Order Updated!');
    }

    public function delete($id)
    {
        $order = Order::find($id);
        return view('dashboard.admin.orders.delete')->with([
            'order' => $order
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $boost = Boost::where('order_id', $id)->where('status', 2)->first();
        $user = User::find($boost->booster_id);
        $user->pay -= $boost->order->booster_price;
        $user->save();
        Boost::where('order_id', $id)->delete();
        OrderExtra::where('order_id', $id)->delete();
        Order::destroy($id);
        return redirect()->route('admin.orders.index')->with('status', 'Order Deleted!');
    }
}
