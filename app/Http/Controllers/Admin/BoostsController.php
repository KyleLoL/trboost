<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Boost;


class BoostsController extends Controller
{
    /**
     * Display a listing of users
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ongoingBoosts = Boost::where('status', 0)->get();
        return view('dashboard.admin.boosts.index')->with([
            'boosts' => $ongoingBoosts
        ]);
    }

    public function takeaway($id)
    {
        $boost = Boost::find($id);
        $boost->status = 1;
        $boost->save();

        return redirect()->route('admin.boosts.index')->with('status', 'Order has been taken away!');
    }
}
