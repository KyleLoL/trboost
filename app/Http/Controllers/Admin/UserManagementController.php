<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;


class UserManagementController extends Controller
{
    /**
     * Display a listing of users
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.admin.user-management.index')->with([
            'users' => User::where('id', '!=', Auth::id())->get(),
        ]);
    }

    /**
     * Display roles to edit user with
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('dashboard.admin.user-roles.edit')->with([
            'user' => User::find($id),
            'roles' => Role::all(),
        ]);
    }

    /**
     * Update User Role
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($id == Auth::id()) {
            abort(401);
        }
        $request->validate([
            'role' => 'required|in:0,1,2',
        ]);
        $user = User::find($id);
        $user->role_id = $request->input('role');
        $user->save();

        return redirect()->route('admin.user-roles.index')->with('status', 'Updated Role for '.$user->name.'!');
    }
}
