<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;


class FinancialController extends Controller
{
    /**
     * Display a listing of boosters
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.admin.financial.index')->with([
            'boosters' => User::where('id', '!=', Auth::id())->where('role_id', 1)->get(),
        ]);
    }

    /**
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('dashboard.admin.financial.edit')->with([
            'user' => User::find($id),
        ]);
    }

    /**
     * Update User Role
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($id == Auth::id()) {
            abort(401);
        }
        $request->validate([
            'pay' => 'required|numeric|min:0',
        ]);
        $user = User::find($id);
        $user->pay = $request->input('pay');
        $user->save();

        return redirect()->route('admin.financial.index')->with('status', 'Updated Pay for '.$user->name.'!');
    }
}
