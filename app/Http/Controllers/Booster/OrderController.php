<?php

namespace App\Http\Controllers\Booster;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Order;
use App\Models\Role;
use App\Models\Boost;
use App\Models\User;


//`boost` table status
//  0 = ongoing
//  1 = dropped

//`order` table status
//  0 = ongoing
//  1 = paused
//  2 = finished
//  3 = canceled (? not sure if will use)

class OrderController extends Controller
{
    /**
     * Display a listing of users
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::whereDoesntHave('boosts', function (Builder $query) {
            $query->where('status', '=', 2)->orWhere('status', '=', 0)->orWhere('booster_id', '=', Auth::id());
        })->get();

        //number of boosts this user is currently doing
        $boostCount = Boost::where('booster_id', Auth::id())->where('status', 0)->count();

        return view('dashboard.booster.orders.index')->with([
            'orders' => $orders,
            'boostCount' => $boostCount
        ]);
    }

    public function finished()
    {
        $finishedOrders = Boost::where('booster_id', Auth::id())->where('status', 2)->get();

        return view('dashboard.booster.orders.finished')->with([
            'orders' => $finishedOrders,
        ]);
    }

    public function ongoingOrders()
    {
        //orders belonging to logged in booster

        $ongoingBoosts = Boost::where('booster_id', Auth::id())->where('status', 0)->get();

        return view('dashboard.booster.orders.ongoing')->with([
            'orders' => $ongoingBoosts,
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'order_id' => 'required|integer|exists:App\Models\Order,id',
        ]);

        $boost = new Boost;
        $boost->order_id = $request->input('order_id');
        $boost->booster_id = Auth::id();
        $boost->save();

        return redirect()->route('booster.orders.ongoing')->with('status', 'Order Picked Up!');
    }

    public function cancel(Request $request, $id)
    {
        $boost = Boost::where('id', $id)->where('booster_id', Auth::id())->first();
        $boost->status = 1;
        $boost->save();

        return redirect()->route('booster.orders.index')->with('status', 'Order has been dropped!');
    }

    public function finish(Request $request, $id)
    {
        $boost = Boost::where('id', $id)->where('booster_id', Auth::id())->first();
        $boost->status = 2;
        $boost->save();

        $order = Order::find($boost->order_id);
        $order->status = 2;
        $order->save();

        $user = User::find(Auth::id());
        $user->pay += $boost->order->booster_price;
        $user->save();

        return redirect()->route('booster.orders.index')->with('status', 'Order has been finished!');
    }

    /**
     * Display roles to edit user with
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('dashboard.admin.user-roles.edit')->with([
            'user' => User::find($id),
            'roles' => Role::all(),
        ]);
    }

    /**
     * Update User Role
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($id == Auth::id()) {
            abort(401);
        }
        $request->validate([
            'role' => 'required|in:0,1,2',
        ]);
        $user = User::find($id);
        $user->role_id = $request->input('role');
        $user->save();

        return redirect()->route('admin.user-roles.index')->with('status', 'Updated Role for '.$user->name.'!');
    }
}
