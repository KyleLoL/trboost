<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactUs;

class ContactUsController extends Controller
{
    public function send(Request $request) 
    {
        $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|email',
            'subject' => 'required|min:3',
            'message' => 'required|min:20'
        ]);
        $inputs = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'subject' => $request->input('subject'),
            'message' => $request->input('message'),
        ];
        $emailAddress = "support@eloboosttr.com";
        Mail::to($emailAddress)->send(new ContactUs($inputs));

        return redirect()->route('contact_us')->with('status', 'Message has been successfully sent!');
    }
}
