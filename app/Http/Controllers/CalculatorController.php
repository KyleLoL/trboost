<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RankTier;
use App\Models\RankDivision;
use App\Models\Rank;
use App\Models\Order;
use App\Models\OrderExtra;
use Illuminate\Support\Facades\DB;
use \Illuminate\Validation\ValidationException;

class CalculatorController extends Controller
{
    public function index()
    {
        $rankTiers = RankTier::all();
        $rankDivisions = RankDivision::all();
        $rankPrices = Rank::all();

        $placementPrices = DB::table('placement_prices')->get();
        $netPrices = DB::table('net_prices')->get();

        return view('boost')->with([
            'tiers' => $rankTiers,
            'divisions' => $rankDivisions,
            'prices' => $rankPrices,
            'placementPrices' => $placementPrices,
            'netPrices' => $netPrices
        ]);
    }


    public function checkout()
    {

        if(session('orderID')) {
            $order = Order::find(session('orderID'));

            if($order->status == 0) {
                try {

                $data = array('email' => $order->email, 'price' => $order->price, 'fullname' => 'Kyle Aldridge', 'phone' => '+017164958197');
                $iframeToken = $this->callPayTR(session('orderID'), $data);
                }catch(ValidationException $e) {
                    die($e->getMessage());
                }
            }else {
                die('order status not 0');
            }
        }


        return view('checkout')->with([
            'iframeToken' => $iframeToken,
            'orderID' => session('orderID')
        ]);
    }


    public function checkoutUserInfo(Request $request) {
        $servers = ['TR', 'EUW', 'EUNE', 'RU'];

        $request->validate([
            'soloType' => 'required|in:1,2',
            'queueType' => 'required|in:1,2',
            'boostType' => 'required|in:1,2,3',
            'server' => 'required|in:' . implode(',', $servers),
        ]);

        if ($request->input('boostType') == 1) {
            $request->validate([
                'currentTier' => 'required|integer|exists:rank_tiers,id',
                'currentDivision' => 'required|integer|exists:rank_divisions,id',
                'currentLP' => 'required|integer|min:0|max:100',
                'desiredTier' => 'required|integer|exists:rank_tiers,id',
                'desiredDivision' => 'nullable|exists:rank_divisions,id',
            ]);
        } else if ($request->input('boostType') == 2) {
            $request->validate([
                'currentTier' => 'sometimes|exists:rank_tiers,id',
                'netWins' => 'required|integer|min:1'
            ]);

        } else if ($request->input('boostType') == 3) {
            $request->validate([
                'currentTier' => 'required|integer|exists:rank_tiers,id',
                'netWins' => 'required|integer|min:1',
                'currentDivision' => 'required|integer|exists:rank_divisions,id',
            ]);
        }

        if ($request->input('soloType') == 1) {
            $request->validate([
                'extraRoles' => 'nullable|array:0,1',
                'extraRoles.*' => 'integer|distinct|min:1|max:5',
                'extraSpells' => 'nullable|array:0,1',
                'extraSpells.*' => 'integer|distinct|min:1|max:9'
            ]);
        }


        $serializedState = serialize($request->except('_token'));
        $duoSelected = ($request->input('soloType') == 2);

        $request->session()->flash('serializedState', $serializedState);
        $request->session()->flash('duoSelected', $duoSelected);

        return view('checkout-userinfo')->with([
            'serializedState' => $serializedState,
            'duoSelected' => $duoSelected
        ]);
    }

    public function checkoutPost(Request $request) {
        $servers = ['TR','EUW','EUNE','RU'];

        $request->merge(unserialize($request->input('serializedState')));

        $request->validate([
            'fullname' => 'required|min:3',
            'phone' => 'required|min:10|regex:/^\+{1,1}[0-1]{2,2}[0-9]{10,}$/',
            'summonerName' => 'required|min:3|max:16',
            'email' => 'required|email',

            'soloType' => 'required|in:1,2',
            'queueType' => 'required|in:1,2',
            'boostType' => 'required|in:1,2,3',
            'server' => 'required|in:' . implode(',', $servers),
        ]);


        if($request->input('soloType') == 1) {
            $request->validate([
                'username' => 'required|min:3|max:40',
                'password' => 'required|min:3|max:40',
            ]);
        }
        

        //current rank info
        $currentTier = $request->input('currentTier') ?: null;
        $currentDivision = 1;
        $desiredTier = null;
        $desiredDivision = null;
        $desiredRank = null;
        $currentRank = null;


        //if division boost
        if ($request->input('boostType') == 1) {
            $request->validate([
                'currentTier' => 'required|integer|exists:rank_tiers,id',
                'currentDivision' => 'required|integer|exists:rank_divisions,id',
                'currentLP' => 'required|integer|min:0|max:100',
                'desiredTier' => 'required|integer|exists:rank_tiers,id',
                'desiredDivision' => 'nullable|exists:rank_divisions,id',
            ]);

            //current rank info
            $currentDivision = $request->input('currentDivision');

            //desired rank info
            $desiredTier = $request->input('desiredTier');
            $desiredDivision = !is_null($request->input('desiredDivision')) ? $request->input('desiredDivision') : 1;


            $desiredRank = Rank::where('tier_id', $desiredTier)->where('division_id', $desiredDivision)->first();
            $desiredRank = $desiredRank->id;
        }else if($request->input('boostType') == 2) {
            $request->validate([
                'currentTier' => 'sometimes|exists:rank_tiers,id',
                'netWins' => 'required|integer|min:1'
            ]);
            $currentRank = ($currentTier != null) ? Rank::where('tier_id', $currentTier)->where('division_id', $currentDivision)->first()->id : "none";
            $netwins = $request->input('netWins');
        }else if($request->input('boostType') == 3) {
            $request->validate([
                'currentTier' => 'required|integer|exists:rank_tiers,id',
                'netWins' => 'required|integer|min:1',
                'currentDivision' => 'required|integer|exists:rank_divisions,id',
            ]);
            $currentDivision = $request->input('currentDivision');
            $netwins = $request->input('netWins');
        }

        if($currentRank != "none" && !$currentRank) {
            $currentRank = Rank::where('tier_id', $currentTier)->where('division_id', $currentDivision)->first();
            $currentRank = $currentRank->id;
        }else {
            $currentRank = null;
        }

        
        $order = new Order;
        $order->user_id = 21;
        $order->email = $request->input('email');

        //calculate queue type
        //soloType = 1 & queueType = 1 | result = 1 (solo solo/duo ladder)
        //soloType = 2 & queueType = 1 | result = 2 (duo solo/duo ladder)
        //soloType = 1 & queueType = 2 | result = 3 (solo flex ladder)
        //soloType = 2 & queueType = 2 | result = 4 (duo flex ladder)
        if($request->input('soloType') == 1) {
            $queueType = $request->input('queueType') == 1 ? 1 : 3;
        }else {
            $queueType = $request->input('queueType') == 1 ? 2 : 4;
        }

        $order->queue_type = $queueType;
        $order->boost_type = $request->input('boostType');
        if($request->boostType != 1) {
            $order->wins = $request->input('netWins');
        }
        $order->current_rank = $currentRank;
        $order->desired_rank = $desiredRank;

        $order->summoner_name = $request->input('summonerName');
        $order->username = $request->input('soloType') == 1 ? $request->input('username') : null;
        $order->password = $request->input('soloType') == 1 ? $request->input('password') : null;
        $order->server = $request->input('server');

        $price = $this->calculatePrice($request->except('_token'));
        //$price = 18.00;
        $order->price = $price;


        $iframeToken = null;
        DB::transaction(function () use ($order, $price, $request, &$iframeToken) {
            $order->save();
            $orderID = $order->id;

            //if solo then we check extras
            if ($request->input('soloType') == 1) {
                $request->validate([
                    'extraRoles' => 'nullable|array:0,1',
                    'extraRoles.*' => 'integer|distinct|min:1|max:5',
                    'extraSpells' => 'nullable|array:0,1',
                    'extraSpells.*' => 'integer|distinct|min:1|max:9'
                ]);
                
                $orderExtra = new OrderExtra;
                $orderExtra->order_id = $orderID;
                $orderExtra->role1 = $request->input('extraRoles.0') ?: null;
                $orderExtra->role2 = $request->input('extraRoles.1') ?: null;
                $orderExtra->spell1 = $request->input('extraSpells.0') ?: null;
                $orderExtra->spell2 = $request->input('extraSpells.1') ?: null;
                $orderExtra->champions = $request->input('extraChamps') ?: null;

                $orderExtra->save();
            }



            $data = array('email' => $request->input('email'), 'price' => $price, 'fullname' => 'Kyle Aldridge', 'phone' => '+017164958197');
            $iframeToken = $this->callPayTR($orderID, $data);
        
        });


        $request->session()->put('orderID', $order->id);
        return redirect()->route('checkout')->with([
            'iframeToken' => $iframeToken
        ]);
    }

    private function callPayTR($orderID, $data)
    {
        $merchant_id     = 245863;
        $merchant_key     = 'rq2Ckdh3zzcWS2Ss';
        $merchant_salt    = '7Pd2undrARfxhz4A';

        $email = $data['email'];
        $payment_amount    = $data['price'] * 100;
        $merchant_oid = "Testingaa" . $orderID;
        $user_name = $data['fullname'];
        $user_address = "-";
        $user_phone = $data['phone'];
        $merchant_ok_url = route('paymentsuccess');
        $merchant_fail_url = route('paymenterror');

        $user_basket = base64_encode(json_encode(array(
            array("Elo Boosting Service", $data['price'], 1), // 1st Product (Product Name - Unit Price - Piece)
        )));

        if (isset($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        } elseif (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else {
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        $user_ip = $ip;
        $timeout_limit = "1";
        $debug_on = 1;
        $test_mode = 1;
        $no_installment    = 1;
        $max_installment = 0;
        $currency = "";

        $hash_str = $merchant_id . $user_ip . $merchant_oid . $email . $payment_amount . $user_basket . $no_installment . $max_installment . $currency . $test_mode;
        $paytr_token = base64_encode(hash_hmac('sha256', $hash_str . $merchant_salt, $merchant_key, true));

        $post_vals = array(
            'merchant_id' => $merchant_id,
            'user_ip' => $user_ip,
            'merchant_oid' => $merchant_oid,
            'email' => $email,
            'payment_amount' => $payment_amount,
            'paytr_token' => $paytr_token,
            'user_basket' => $user_basket,
            'debug_on' => $debug_on,
            'no_installment' => $no_installment,
            'max_installment' => $max_installment,
            'user_name' => $user_name,
            'user_address' => $user_address,
            'user_phone' => $user_phone,
            'merchant_ok_url' => $merchant_ok_url,
            'merchant_fail_url' => $merchant_fail_url,
            'timeout_limit' => $timeout_limit,
            'currency' => $currency,
            'test_mode' => $test_mode
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://www.paytr.com/odeme/api/get-token");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_vals);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        //XXX: DİKKAT: lokal makinanızda "SSL certificate problem: unable to get local issuer certificate" uyarısı alırsanız eğer
        //aşağıdaki kodu açıp deneyebilirsiniz. ANCAK, güvenlik nedeniyle sunucunuzda (gerçek ortamınızda) bu kodun kapalı kalması çok önemlidir!
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $result = @curl_exec($ch);

        if (curl_errno($ch)){
            throw ValidationException::withMessages([
                'PayTR' => [
                    'There has been an error connecting to the payment server.',
                ],
            ]);
            
            //throw new \Exception("PAYTR IFRAME connection error. err:" . curl_error($ch));
            //need to log this somewhere
        }

        curl_close($ch);

        $result = json_decode($result, 1);

        if ($result['status'] == 'success') 
        {
            $token = $result['token'];
        } else {
            throw ValidationException::withMessages([
                'PayTR' => [
                    'There has been an error displaying the payment form.',
                ],
            ]);
        }

        //throw new \PayTRException("PAYTR IFRAME failed. reason:" . $result['reason']);
        //need to log this somewhere

        return $token;
    }

    private function calculatePrice($data) {
        
        function addPercentage($total, $percent)
        {
            return $total + ($total / 100) * $percent;
        }

        //initial settings
        $price = 0;
        $lpDiscount = 0; // 0%
        $serverPercent = 0; // 0%


        //get server percentage
        if($data['server'] != "TR") $serverPercent = 50; // 50%

        //if division boost
        if($data['boostType'] == 1) {

            //set division boost settings
            if($data['server'] != "TR" && $data['soloType'] == 2) {
                $serverPercent = 70; // 70%
            }
            $duoPercent = 200; // 200%
            $ranks = Rank::all();
            $prices = [];

            //populate prices array
            foreach($ranks as $rank) {
                $prices[] = $rank->price;
            }

            //if masters is selected...default division is just 1
            if(!isset($data['desiredDivision'])) { $data['desiredDivision'] = 1; }

            //get current and desired rank ID
            $currentRankID = Rank::where('tier_id', $data['currentTier'])->where('division_id', $data['currentDivision'])->first()->id;
            $desiredRankID = Rank::where('tier_id', $data['desiredTier'])->where('division_id', $data['desiredDivision'])->first()->id;

            //techinically $currentRankID in the context of $prices array is +1,
            //we don't want the true starting rank so this is fine
            for($i = $currentRankID; $i<$desiredRankID; $i++) {

                //calculate LP discount for first rank (if start is iron 4, we discount iron 3...iron4 > iron3)
                if($i == $currentRankID) {
                    if ($data['currentLP'] >= 20 && $data['currentLP'] <= 39) {
                        $lpDiscount = -10; // -10% discount
                    } else if ($data['currentLP'] >= 40 && $data['currentLP'] <= 59) {
                        $lpDiscount = -20; // -20% discount
                    } else if ($data['currentLP'] >= 60 && $data['currentLP'] <= 79) {
                        $lpDiscount = -30; // -30% discount
                    } else if ($data['currentLP'] >= 80) {
                        $lpDiscount = -40; // -40% discount
                    }
                }else {
                    $lpDiscount = 0;
                }

                //get discounted price based on LP
                $discounted = addPercentage($prices[$i], $lpDiscount);

      
                //apply euw specific pricings
                if ($data['server'] == "EUW" && $i >= 21) {
     
                    switch($i) {
                        case 21:
                            $euwPercent = 150; //increase d4 -> d3 by 150%
                            break;
                        case 22:
                            $euwPercent = 235; //increase d3 -> d2 by 235%
                            break;
                        case 23:
                            $euwPercent = 312; //increase d2 -> d1 by 312%
                            break;
                        case 24:
                            $euwPercent = 325; //increase d1 -> master by 325%
                            break;
                    }
                    $price += addPercentage($discounted, $euwPercent);
                    continue;
                }

   
                $price += addPercentage($discounted, $serverPercent);

            }//end for loop


        }else if($data['boostType'] == 2) {
            //set placement boost settings
            $duoPercent = 20; // 20%
            $placementPrice = isset($data['currentTier']) ? DB::table('placement_prices')
                                                                ->where('tier_id', $data['currentTier'])->first()->price : 9; //9 is for unranked 

            $price = addPercentage($placementPrice * $data['netWins'], $serverPercent);
        }else if($data['boostType'] == 3) {
            //set netwins boost settings
            if ($data['server'] == "EUW" && $data['currentTier'] == 6) { //6 = diamond tier
                $serverPercent = 185; // 185%
            }
            $duoPercent = 50; // 50%

            //get current rank ID
            $currentRankID = Rank::where('tier_id', $data['currentTier'])->where('division_id', $data['currentDivision'])->first()->id;
            $netwinsPrice = DB::table('net_prices')->where('rank_id', $currentRankID)->first()->price;
            
            $price = addPercentage($netwinsPrice * $data['netWins'], $serverPercent);
        }

        //if solo
        if($data['soloType'] == 1) {
            if(array_key_exists('extraRoles', $data)) {
                $roleMultiplier = 1;
                switch($data['extraRoles'][0]) {
                    case 1:
                        $roleMultiplier = 1.25;
                        break;
                    case 2:
                        $roleMultiplier = 1.20;
                        break;
                    case 3:
                        $roleMultiplier = 1.20;
                        break;
                    case 4:
                        $roleMultiplier = 1.30;
                        break;
                    case 5:
                        $roleMultiplier = 1.30;
                        break;
                }
                $price *= $roleMultiplier;
            }

            if (array_key_exists('extraSpells', $data)) {
                $price += 10;
            }
        }else if($data['soloType'] == 2) { //if duo is selected..apply duo percentages
            $price = addPercentage($price, $duoPercent);
        }


        return number_format($price, 2, '.', '');
    }
}