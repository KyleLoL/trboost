<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CartCheckoutRequest extends FormRequest
{

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        //DIVISION
        //if division boost, highest rank is d1 for solo
        $validator->sometimes('current_rank', 'required|integer|max:24|exists:ranks,id', function ($input) {
            return ($input->boost_type == 1 && in_array($input->queue_type, array(1,3)));
        });

        //if division boost, require desired rank max is masters for solo
        $validator->sometimes('desired_rank', 'required|integer|gt:current_rank|max:25|exists:ranks,id', function ($input) {
            return ($input->boost_type == 1 && in_array($input->queue_type, array(1, 3)));
        });

        //if duo boost division
        $validator->sometimes('current_rank', 'required|integer|max:20|exists:ranks,id', function ($input) {
            return ($input->boost_type == 1 && in_array($input->queue_type, array(2, 4)));
        });
        $validator->sometimes('desired_rank', 'required|integer|gt:current_rank|max:21|exists:ranks,id', function ($input) {
            return ($input->boost_type == 1 && in_array($input->queue_type, array(2, 4)));
        });

        //require current_lp if division boost
        $validator->sometimes('current_lp', 'required|integer|min:0|max:100', function ($input) {
            return $this->boost_type == 1;
        });


        //PLACEMENTS
        //current rank can be unranked or exist in the database
        $validator->sometimes('current_rank', 'required|integer|min:0|max:29', function ($input) {
            return $input->boost_type == 2;
        });

        //NET WINS
        $validator->sometimes('current_rank', 'required|integer|exists:ranks,id', function ($input) {
            return $input->boost_type == 3;
        });

        //require netWins if Placements or Net Wins
        $validator->sometimes('wins', 'required|integer|min:1|max:100', function ($input) {
            return in_array($input->boost_type, array(2, 3));
        });




        //validate extraRoles
        $validator->sometimes('role1', 'nullable|integer|min:1|max:5', function ($input) {
            return in_array($input->queue_type, array(1,3));
        });

        $validator->sometimes('role2', 'required|integer|min:1|max:5|different:role1', function ($input) {
            return !is_null($input->role1);
        });

        //validate extraSpells
        $validator->sometimes('spell1', 'nullable|integer|min:1|max:9', function ($input) {
            return in_array($input->queue_type, array(1, 3));
        });

        $validator->sometimes('spell2', 'required|integer|min:1|max:9|different:spell1', function ($input) {
            return !is_null($input->spell1);
        });

        //validate champions
        //


        $validator->sometimes(['lol_user', 'lol_pass'], 'required|min:3', function ($input) {
            return in_array($input->queue_type, array(1,3));
        });
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $servers = ['TR', 'EUW', 'EUNE', 'RU'];

        return [
            'server'        => 'required|in:'.implode(',', $servers),
            'queue_type'    => 'required|integer|min:1|max:4',
            'boost_type'    => 'required|integer|min:1|max:3',
            'server'        => 'required|in:' . implode(',', $servers),
            'fullname'      => 'required|min:3',
            'email'         => 'required|email',
            'phone'         => 'required|min:10',
            'summoner_name' => 'required|min:3|max:16'
        ];
    }
}
