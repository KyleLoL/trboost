<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCartRequest extends FormRequest
{

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        //require currentTier if Division boost or Net Wins boost
        $validator->sometimes('currentTier', 'required|exists:rank_tiers,id', function ($input) {
            return in_array($input->boostType, array(1, 3));
        });

        //require currentDivision if Division boost or Net Wins boost
        $validator->sometimes('currentDivision', 'required|exists:rank_divisions,id', function ($input) {
            return in_array($input->boostType, array(1, 3));
        });


        //DIVISION BOOST
        //validate desiredTier,desiredDivision, currentLP if division boost
        $validator->sometimes('desiredTier', 'required|exists:rank_tiers,id', function ($input) {
            return $input->boostType == 1;
        });
        $validator->sometimes('desiredDivision', 'required|exists:rank_divisions,id', function ($input) {
            return $input->boostType == 1;
        });
        $validator->sometimes('currentLP', 'required|integer|min:0|max:100', function ($input) {
            return $this->boostType == 1;
        });

        //make sure desiredDivision is bigger than currentDivision IF currentTier and desiredTier are the same
        //and boost type is division
        $validator->sometimes('desiredDivision', 'gt:currentDivision', function ($input) {
            if($input->boostType == 1) {
                return $input->currentTier == $input->desiredTier;
            }

            return false;
        });




        //PLACEMENTS
        //currentTier can be unranked or exist in the database
        $validator->sometimes('currentTier', 'nullable|exists:rank_tiers,id', function ($input) {
            return $input->boostType == 2;
        });

        //require netWins if Placements or Net Wins
        $validator->sometimes('netWins', 'required|integer|min:1|max:100', function ($input) {
            return in_array($input->boostType, array(2,3));
        });




        //validate extraRoles
        $validator->sometimes('extraRoles', 'nullable|array:0,1', function ($input) {
            return $input->soloType == 1;
        });
        $validator->sometimes('extraRoles.*', 'integer|distinct|min:1|max:5', function ($input) {
            return !is_null($input->extraRoles);
        });

        //validate extraSpells
        $validator->sometimes('extraSpells', 'nullable|array:0,1', function ($input) {
            return $input->soloType == 1;
        });
        $validator->sometimes('extraSpells.*', 'integer|distinct|min:1|max:9', function ($input) {
            return !is_null($input->extraSpells);
        });

        //validate champions
        //
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $servers = ['TR', 'EUW', 'EUNE', 'RU'];

        return [
            'soloType' => 'required|integer|min:1|max:2',
            'queueType' => 'required|integer|min:1|max:2',
            'boostType' => 'required|integer|min:1|max:3',
            'server' => 'required|in:'.implode(',', $servers),
        ];
    }
}
