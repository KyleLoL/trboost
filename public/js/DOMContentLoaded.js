document.addEventListener( 'DOMContentLoaded', function () {
                new Splide( '.splide', {
                    type: 'loop',
                    direction: 'ttb',
                    fixedHeight: '420px',
                    height: '420px',
                    autoplay: true,
                    interval: 2000
                }).mount();
            } );