<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('home') }}">
        <div class="sidebar-brand-icon rotate-n-15">
            <img src="https://lolboost.novundev.com/img/logo.png" />
        </div>
        <div class="sidebar-brand-text mx-3">EloBoost</div>
    </a>
    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Menu
    </div>



    <!-- Nav Item -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('dashboard.home') }}">
            <i class="fas fa-fw fa-home"></i>
            <span>Home</span></a>
    </li>

    <!-- Nav Item -->
    <li class="nav-item">
        <a class="nav-link" href="#">
            <i class="fas fa-fw fa-comments"></i>
            <span>Live Chat</span></a>
    </li>

    @if(Auth::user()->role_id == 0)
    <!-- Nav Item -->
    <li class="nav-item">
        <a class="nav-link" href="">
            <i class="fas fa-fw fa-chart-line"></i>
            <span>Open Orders</span></a>
    </li>

    <!-- Nav Item -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('orders.index') }}">
            <i class="fas fa-fw fa-tags"></i>
            <span>My Orders</span></a>
    </li>

    <!-- Nav Item -->
    <li class="nav-item">
        <a class="nav-link" href="#">
            <i class="fas fa-fw fa-dollar-sign"></i>
            <span>Order History</span></a>
    </li>
    @elseif(Auth::user()->role_id == 1)
        <!-- Nav Item -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('booster.orders.index') }}">
                    <i class="fas fa-fw fa-tags"></i>
                    <span>Open Orders</span></a>
            </li>

            <!-- Nav Item -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('booster.orders.ongoing') }}">
                    <i class="fas fa-fw fa-chart-line"></i>
                    <span>My Boosts</span></a>
            </li>

            <!-- Nav Item -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('booster.orders.finished') }}">
                    <i class="fas fa-fw fa-history"></i>
                    <span>Finished Boosts</span></a>
            </li>

            <!-- Nav Item -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('booster.financial.index') }}">
                    <i class="fas fa-fw fa-dollar-sign"></i>
                    <span>Financials</span></a>
            </li>
    @else
        <!-- Nav Item -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.orders.index') }}">
                    <i class="fas fa-fw fa-tags"></i>
                    <span>Orders</span></a>
            </li>

            <!-- Nav Item -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.boosts.index') }}">
                    <i class="fas fa-fw fa-chart-line"></i>
                    <span>Boosts</span></a>
            </li>

            <!-- Nav Item -->
            <li class="nav-item">
                <a class="nav-link" href="{{ route('admin.user-management.index') }}">
                    <i class="fas fa-fw fa-users"></i>
                    <span>User Management</span></a>
            </li>

            <!-- Nav Item -->
            <li class="nav-item">
                <a class="nav-link" href="#">
                    <i class="fas fa-fw fa-book"></i>
                    <span>Logs</span></a>
            </li>
    @endif


    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
