<x-layout>

    <section class="section">
        <div class="section__wrapper">
            <header class="section__header flex">
                <h2 class="section__header__title">Checkout <span>Page</span></h2>
                <p class="section__header__sub"><a href="/">Home</a> / <span style="color: #ff4719;">Checkout</span></p>
            </header>
            <section>
                {{ $iframeToken }}<br />
                <script src="https://www.paytr.com/js/iframeResizer.min.js"></script>
                <iframe src="https://www.paytr.com/odeme/guvenli/{{ $iframeToken }}" id="paytriframe" frameborder="0" scrolling="no" style="width: 100%;"></iframe>
	            <script>iFrameResize({},"#paytriframe");</script>
            </section>
        </div>
    </section>
</x-layout>