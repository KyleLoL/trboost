<x-layout>

        @push('header-scripts')
        <script src="https://use.fontawesome.com/d983741785.js"></script>
        <script src="https://kit.fontawesome.com/870b7dbf87.js" crossorigin="anonymous"></script>
        @endpush


        @push('sheets')
        <link rel="stylesheet" href="css/my_custom_inline_style.css">
        <link rel='stylesheet' href='css/ExternalCss/bootstrap.min.css' type='text/css' media='all' />
        <link rel='stylesheet' href='css/ExternalCss/hostwhmcs.css' type='text/css' media='all' />
        <style>
            span.wpcf7-list-item {
                display: inline-block;
                margin: 0 0 0 1em;
            }
            .navbar {
                position: initial;
                min-height: 0;
                margin-bottom: 0px;
            }
            .navbar:before, .navbar:after {
                content: normal;
            }
            ol, ul {
                margin-bottom: 0;
            }
            .navbar__list__item--active {
                color: rgb(255, 71, 25) !important;
                font-weight: 900 !important;
            }
            .navbar__brand {
                color: #ff4719 !important;
            }
        </style>
        @endpush


            <div id="pageHeader" class="global ">

                <div class="container">

                    <div class="section--title">

                        <div class="row">

                            <div class="col-md-6">

                                <h2 class="join_us_h2">Join<span> Us</span></h2>                

                            </div>

                            

                            <div class="col-md-6 ">

                                <div class="page-header--breadcrumb woo-breadcrumbs">

                                    <ul class="breadcrumb">

                                        <li>

                                            <a href="/" class="home_a">Home</a>

                                        </li>

                                        <li>

                                            Join Us

                                        </li>

                                    </ul>

                                </div> 

                            </div>

                        </div>

                    </div>

                </div>

            </div>



            <div id="aboutDesc">

                <div class="container">

                    <p>

                        Please fill out the Information below and we will get back to you!

                    </p>

                    <div role="form" class="wpcf7" id="wpcf7-f5-p2214-o1" lang="en-US" dir="ltr">

                        <div class="screen-reader-response">

                    

                        </div>

                        <form action="#"" method="post" class="wpcf7-form" novalidate="novalidate">

                            <div class="contact--form-status">

                    

                            </div>

                            <div class="row">

                                <div class="col-md-6">

                                    <div class="form-group">

                                        <label>

                                            Your Name

                                        </label>

                                        <br />

                                        <span class="wpcf7-form-control-wrap contactName">

                                            <input type="text" name="contactName" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Name" />

                                        </span>

                                        <br />

                                        <span class="highlight"></span>
                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="form-group">

                                        <label>

                                            Your E-mail

                                        </label>

                                        <br />

                                        <span class="wpcf7-form-control-wrap contactEmail">

                                            <input type="email" name="contactEmail" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false" placeholder="Email" />

                                        </span>

                                        <br />

                                        <span class="highlight">

                                            

                                        </span>

                                    </div>

                                </div>

                            </div>



                            <div class="row">

                                <div class="col-md-4">

                                    <div class="form-group">

                                        <label>

                                            Your Skype

                                        </label>

                                        <br />

                                    <span class="wpcf7-form-control-wrap skype">

                                        <input type="text" name="skype" value="" size="40" class="wpcf7-form-control wpcf7-text form-control" aria-invalid="false" placeholder="We don&#039;t need both Skype and Discord, one is Enough" />

                                    </span>

                                    <br />

                                    <span class="highlight">

                                        

                                    </span>

                                    </div>

                                </div>

                                <div class="col-md-4">

                                        <div class="form-group">

                                            <label>

                                                Your Discord

                                            </label>

                                            <br />

                                            <span class="wpcf7-form-control-wrap discord">

                                                <input type="text" name="discord" value="" size="40" class="wpcf7-form-control wpcf7-text form-control" aria-invalid="false" placeholder="We don&#039;t need both Skype and Discord, one is Enough" />

                                            </span>

                                            <br />

                                            <span class="highlight">

                                                

                                            </span>

                                        </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="form-group">

                                        <label>

                                            Your OP.GG Link

                                        </label>

                                        <br />

                                        <span class="wpcf7-form-control-wrap summonername">

                                            <input type="text" name="summonername" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="OP.GG Link" />

                                        </span>

                                        <br />

                                        <span class="highlight">

                                            

                                        </span>

                                    </div>

                                </div>

                            </div>



                            <div class="row">

                                <div class="col-md-4">

                                    <div class="form-group">

                                        <label>

                                            Your age

                                        </label>

                                        <br />

                                        <span class="wpcf7-form-control-wrap age">

                                            <input type="text" name="age" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Age" />

                                        </span>

                                        <br />

                                        <span class="highlight">

                                            

                                        </span>

                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="form-group">

                                        <label>

                                            What roles do you play?

                                        </label>

                                        <br />

                                        <span class="wpcf7-form-control-wrap country">

                                            <input type="text" name="country" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="The Roles You Play" />

                                        </span>

                                        <br />

                                        <span class="highlight">

                                            

                                        </span>

                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="form-group">

                                        <label>

                                            What champions do you play?

                                        </label>

                                        <br />

                                        <span class="wpcf7-form-control-wrap region">

                                            <input type="text" name="region" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Champions You Play" />

                                        </span>

                                        <br />

                                        <span class="highlight">

                                            

                                        </span>

                                    </div>

                                </div>

                            </div>



                            <div class="row">

                                <div class="col-md-6">

                                    <div class="form-group">

                                        <label>

                                            Did you Boost before? If yes where?

                                        </label>

                                        <br />

                                        <span class="wpcf7-form-control-wrap experience">

                                            <input type="text" name="experience" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Experience" />

                                        </span>

                                        <br />

                                        <span class="highlight">

                                            

                                        </span>

                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="form-group">

                                        <label>

                                            What is the Highest Rank you ever reached?

                                        </label>

                                        <br />

                                        <span class="wpcf7-form-control-wrap highestleague">

                                            <input type="text" name="highestleague" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Highest Rank" />

                                        </span>

                                        <br />

                                        <span class="highlight">

                                            

                                        </span>

                                    </div>

                                </div>

                            </div>



                            <div class="row">

                                <div class="col-md-6">

                                    <div class="form-group">

                                        <label>

                                            How many hours can you do Boosting per Day?

                                        </label>

                                        <br />

                                        <span class="wpcf7-form-control-wrap rankup">

                                            <input type="text" name="rankup" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Hours Per Day" />

                                        </span>

                                        <br />

                                        <span class="highlight">

                                            

                                        </span>

                                    </div>

                                </div>

                                <div class="col-md-6">

                                    <div class="form-group">

                                        <label>

                                            What is your Current Rank?

                                        </label>

                                        <br />

                                        <span class="wpcf7-form-control-wrap currentleague">

                                            <input type="text" name="currentleague" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Current Rank" />

                                        </span>

                                        <br />

                                        <span class="highlight">

                                            

                                        </span>

                                    </div>

                                </div>

                            </div>



                            <div class="form-group">

                                <label>

                                    What Position are you Applying for?

                                </label>

                                <br />

                                <span class="wpcf7-form-control-wrap WhatPositionareyouApplyingfor">

                                    <span class="wpcf7-form-control wpcf7-checkbox wpcf7-validates-as-required">

                                        <span class="wpcf7-list-item first">

                                            <span class="wpcf7-list-item-label">

                                                ELO Booster

                                            </span>

                                            <input type="checkbox" name="WhatPositionareyouApplyingfor[]" value="Elo Booster" />

                                        </span>

                                        <span class="wpcf7-list-item last">

                                            <span class="wpcf7-list-item-label">

                                                Manager

                                            </span>

                                            <input type="checkbox" name="WhatPositionareyouApplyingfor[]" value="Manager" />

                                        </span>

                                    </span>

                                </span>

                            </div>

                            <div class="form-group">

                                <label>

                                    Please give us a brief Description about yourself!

                                </label>

                                <br />

                                <span class="wpcf7-form-control-wrap contactMessage">

                                    <textarea name="contactMessage" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea form-control" aria-invalid="false" placeholder="

            What makes you think you are a good booster and suitable for our website?

            Any general or specific information you'd like to share with us?

            What is your main goal of this job?"></textarea>

                                </span>

                                <br />

                                <span class="highlight">

                                    

                                </span>

                            </div>

                            <p>

                                <button type="submit" class="btn--primary btn--ripple">

                                    Send

                                </button>

                            </p>

                            <div class="wpcf7-response-output wpcf7-display-none">

                                

                            </div>

                        </form>

                    </div>

                </div>

            </div>

</x-layout>
