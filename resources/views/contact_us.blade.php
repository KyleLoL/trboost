<x-layout>

		@push('header-scripts')
	    <script type="text/javascript" src="js/scrollFunction.js"></script>
	    <script src="https://use.fontawesome.com/d983741785.js"></script>
        <script src="https://kit.fontawesome.com/870b7dbf87.js" crossorigin="anonymous"></script>
		@endpush



		@push('sheets')
		<link href="https://fonts.googleapis.com/css?family=Barlow:300,400,500,600,700&display=swap" rel="stylesheet">

		<link rel="stylesheet" href="css/style_tpl_contact.css">

		<style>
			.alert {
  position: relative;
  padding: 0.75rem 1.25rem;
  margin-bottom: 1rem;
  border: 1px solid transparent;
  border-radius: 0.35rem;
}

.alert-heading {
  color: inherit;
}

.alert-link {
  font-weight: 700;
}

.alert-dismissible {
  padding-right: 4rem;
}

.alert-dismissible .close {
  position: absolute;
  top: 0;
  right: 0;
  z-index: 2;
  padding: 0.75rem 1.25rem;
  color: inherit;
}

.alert-primary {
  color: #293c74;
  background-color: #dce3f9;
  border-color: #cdd8f6;
}

.alert-primary hr {
  border-top-color: #b7c7f2;
}

.alert-primary .alert-link {
  color: #1c294e;
}

.alert-secondary {
  color: #45464e;
  background-color: #e7e7ea;
  border-color: #dddde2;
}

.alert-secondary hr {
  border-top-color: #cfcfd6;
}

.alert-secondary .alert-link {
  color: #2d2e33;
}

.alert-success {
  color: #0f6848;
  background-color: #d2f4e8;
  border-color: #bff0de;
}

.alert-success hr {
  border-top-color: #aaebd3;
}

.alert-success .alert-link {
  color: #093b29;
}

.alert-info {
  color: #1c606a;
  background-color: #d7f1f5;
  border-color: #c7ebf1;
}
</style>
		@endpush

		@if (session('status'))
        <div class="row">
            <div class="col-xl-12 col-md-12 mb-4">
                <div class="alert alert-success" role="alert" style="text-align: center;">
                    {{ session('status') }}
                </div>
            </div>
        </div>
    	@endif

			<div class="tpl-contact ">

				<section class="ps-contact">

		            <div class="st-inner">

		                <div class="st-cnt">

		                    <div class="st-cols">

		                        <div class="col-left">

		                            <div class="content">

		                                <div class="inner">

		                                    <div class="part-contact">

		                                        <div class="head">

		                                            <div class="wrap-title">

		                                                <h2 class="title" style="color:#ff4719;">

		                                                    Contact us

		                                                </h2>

		                                                <div class="subtitle">

		                                                    <p>

		                                                        <strong>

		                                                            Disclaimer:

		                                                        </strong>

		                                                            Before you open the ticket, please contact us through the 

		                                                         <strong>

		                                                            Live Chat

		                                                         </strong> (24/7 online) or create a ticket on 

		                                                         <strong>

		                                                            Discord

		                                                         </strong> to get a response immediately

		                                                    </p>

		                                                </div>

		                                            </div>

		                                        </div>

		                                        <form class="form" action="{{ route('contact_us.send') }}" method="POST">
													@csrf
		                                            <div class="interface">

		                                                <div class="row">

		                                                    <div class="col-left-label">

		                                                        <div class="label">

		                                                            Name:

		                                                        </div>

		                                                    </div>

		                                                    <div class="col-right-control">

		                                                        <div class="control">

		                                                            <input type="text" name="name" minlength="3" value="" placeholder="JohnDoe" autocomplete="off" required>

		                                                        </div>

		                                                    </div>

		                                                </div>

		                                                <div class="row">

		                                                    <div class="col-left-label">

		                                                        <div class="label">

		                                                            Email:

		                                                        </div>

		                                                    </div>

		                                                    <div class="col-right-control">

		                                                        <div class="control">

		                                                            <input type="text" name="email" value="" placeholder="your@email.com" autocomplete="off" required>

		                                                        </div>

		                                                    </div>

		                                                </div>

		                                                <div class="row">

		                                                    <div class="col-left-label">

		                                                        <div class="label">

		                                                            Subject:

		                                                        </div>

		                                                    </div>

		                                                    <div class="col-right-control">

		                                                        <div class="control">

		                                                            <input type="text" name="subject" minlength="3" value="" placeholder="Question" autocomplete="off" required>

		                                                        </div>

		                                                    </div>

		                                                </div>

		                                                <div class="row">

		                                                    <div class="col-left-label">

		                                                        <div class="label">

		                                                            Question:

		                                                        </div>

		                                                    </div>

		                                                    <div class="col-right-control">

		                                                        <div class="control">

		                                                            <textarea name="message" minlength="20" placeholder="Share your thoughts..." autocomplete="off" spellcheck="false" required></textarea>

		                                                        </div>

		                                                    </div>

		                                                </div>

		                                                <div class="row">

		                                                    <div class="col-left-label">

		                                                        <div class="label">

		                                                            reCaptcha:

		                                                        </div>

		                                                    </div>

		                                                    <div class="col-right-control">

		                                                        <div class="g-recaptcha" data-sitekey="6Ld3yHIUAAAAAJAMBpRcxaKq05aM-b4LLWbL67GY" data-size="normal" data-theme="light" id="recaptcha-element">

		                                                            

		                                                        </div>

		                                                    </div>

		                                                </div>

		                                                <div class="row">

		                                                    <div class="col-left-label">

		                                                        

		                                                    </div>

		                                                    <div class="col-right-control">

		                                                        <div class="wrap-btn-send">

		                                                            <button name="submit" class="btn-send" style="background-color:#ff4719;">

		                                                                Send Message

		                                                            </button>

		                                                        </div>

		                                                    </div>

		                                                </div>

		                                            </div>

		                                        </form>

		                                    </div>

		                                </div>

		                            </div>

		                        </div>

		                        <div class="col-right">

		                            <div class="content">

		                                <div class="inner">

		                                    <div class="part-reach-us">

		                                        <div class="head">

		                                            <div class="wrap-title">

		                                                <h2 class="title" style="color:#ff4719;">

		                                                    You can reach us

		                                                </h2>

		                                            </div>

		                                        </div>

		                                        <ul class="list">

		                                            <li class="item mail">

		                                                <div class="cnt">

		                                                    <div class="text">

		                                                        <a href="mailto:support@eloboosttr.com">

		                                                            <span class="__cf_email__" data-cfemail="" style="font-size: 16px;font-weight: 500;color: #737373;">

		                                                            	support@eloboosttr.com

		                                                            </span>

		                                                        </a>

		                                                    </div>

		                                                    <a class="link" href="mailto:support@eloboosttr.com" target="_blank">

		                                                        

		                                                    </a>

		                                                </div>

		                                            </li>

		                                            <li class="item discord">

		                                                <div class="cnt">

		                                                    <div class="text">

		                                                        <a href="https://discord.gg/NeGdUfqNwB" target="_blank">Discord server of EloBoostTR</a>

		                                                    </div>

		                                                    <div class="status online">

		                                                        

		                                                    </div>

		                                                    <a class="link" href="https://discord.gg/NeGdUfqNwB" target="_blank">

		                                                        

		                                                    </a>

		                                                </div>

		                                            </li>

		                                        </ul>

		                                    </div>

		                                    <div class="part-information">

		                                        <div class="head">

		                                            <div class="wrap-title">

		                                                <h2 class="title" style="color:#ff4719;">

		                                                    Legal Informations

		                                                </h2>

		                                            </div>

		                                        </div>

		                                        <div class="cnt">

		                                            <div class="info-chunk">

		                                                <h6 class="name">

		                                                    Tax Number:

		                                                </h6>

		                                                <p class="text">

		                                                    1180343860

		                                                </p>

		                                            </div>

		                                            <div class="info-chunk">

		                                                <h6 class="name">

		                                                    Registered Office:

		                                                </h6>

		                                                <p class="text">

		                                                    Kavaklıdere Mah. Esat cad. NO: 12/1

		                                                    <br>

		                                                    Çankaya, Ankara

		                                                </p>

		                                            </div>

		                                            <div class="info-chunk">

		                                                <h6 class="name">

		                                                    Tax Administration:

		                                                </h6>

		                                                <p class="text">

		                                                    Kavaklıdere Vergi Dairesi

		                                                </p>

		                                            </div>

		                                        </div>

		                                    </div>

		                                    <div class="part-jobs">

		                                        <div class="head">

		                                            <div class="wrap-title">

		                                                <h2 class="title" style="color:#ff4719;">

		                                                    Jobs

		                                                </h2>

		                                            </div>

		                                        </div>

		                                        <div class="cnt">

		                                            <div class="info-chunk">

		                                                <p class="text">We are looking for boosters.

		                                                    <br>

		                                                    More informations about open spots, you can find

			                                                <a href="join_us" target="_self" style="text-decoration: none;color:#ff4719;">

			                                                    <strong>here</strong>

			                                                </a>

		                                                </p>

		                                            </div>

		                                        </div>

		                                    </div>

		                                </div>

		                            </div>

		                        </div>

		                    </div>

		                </div>

		            </div>

		        </section>

			</div>
</x-layout>