<x-layout_boost>
    @push('sheets')
    <link rel="stylesheet" href="./css/calculator.css">
    @endpush
    <section class="section">
        <div class="section__wrapper">
            <header class="section__header flex">
                <h2 class="section__header__title">Elo Boost <span>Sayfası</span></h2>
                <p class="section__header__sub">Ana Sayfa / Elo Boost</p>
            </header>
            <section>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="calc_container">
                    <div class="calculator">

                        <div class="calculator__row calculator__row--visible">
                            <div class="calculator__header">
                                <span class="calculator__title">1: Boost sıra türünü seçiniz Solo veya Duo</span>
                            </div>
                            <div class="calculator__content" id="intro">
                                <div class="rank__type__widget">
                                    <p>SOLO(Tekli)</p>
                                    <i class="rank__type__widget__icon lni lni-user option" data-option=1></i>
                                    <p>Booster hesabınızda oynar.</p>
                                </div>

                                <div class="rank__type__widget">
                                    <p>DUO(İkili)</p>
                                    <i class="rank__type__widget__icon lni lni-users option" data-option=2></i>
                                    <p>Siz kendi hesabınızda, booster ile oynarsınız.</p>
                                </div>
                            </div>
                        </div>

                        <!-- Ladder -->
                        <div class="calculator__row" id="action2">
                            <div class="calculator__header">
                                <span class="calculator__title">2: DERECELİ SIRA TÜRÜNÜ SEÇİN</span>
                            </div>

                            <div class="calculator__content">
                                <div class="ladder__widget">
                                    <a href="#" class="btn btn--block btn--white option" data-option=1>5v5 Dereceli Tek/Çift</a>
                                </div>
                                <div class="ladder__widget">
                                    <a href="#" class="btn btn--block btn--white option" data-option=2>5v5 Dereceli Esnek</a>
                                </div>
                            </div>
                        </div>

                        <!-- Boost Type -->
                        <div class="calculator__row" id="action3">
                            <div class="calculator__header">
                                <span class="calculator__title">3: BOOST HİZMET TÜRÜNÜ SEÇİN</span>
                            </div>

                            <div class="calculator__content">
                                <div class="boost__widget">
                                    <a href="#" class="btn btn--block btn--white option" data-option=1>Lig(Aşama) Yükseltme</a>
                                </div>
                                <div class="boost__widget">
                                    <a href="#" class="btn btn--block btn--white option" data-option=2>Yerleştirme Maçları</a>
                                </div>
                                <div class="boost__widget">
                                    <a href="#" class="btn btn--block btn--white option" data-option=3>Win Boost(Net Wins)</a>
                                </div>
                            </div>
                        </div>

                        <!-- Start/End Rank -->

                        <div class="calculator__row" id="action4">
                            <div class="calculator__content">
                                <div class="rank__widget">
                                    Güncel Liginiz:
                                    <img class="rank__widget__img" src="https://cdn.l9eloboosting.com/img/tiers2/iron.png" />
                                    <select class="rank__widget__select" name="current_tier" onchange="rankState(this, 'action4')">
                                        @foreach($tiers as $tier)
                                        @if($tier->id < 7)
                                        <option value="{{ $tier->id }}">{{ $tier->name }}</option>
                                        @endif
                                        @endforeach
                                    </select>

                                    <select class="rank__widget__select" name="current_division" onchange="rankState(this, 'action4')">>
                                        @foreach($divisions as $division)
                                        <option value="{{ $division->id }}">{{ $division->number }}</option>
                                        @endforeach
                                    </select>

                                    <div>
                                        <div>
                                        LP:<br />
                                        <input type="text" class="winsForm" placeholder="0" onkeyup="discountLP(this.value); resultState.priceListener();" />
                                        </div>
                                        <div class="discount" id="discountDiv">Discount Applied</div>
                                    </div>
                                </div>
                                <div class="rank__widget">
                                    Ulaşmak İstediğiniz Lig:
                                    <img class="rank__widget__img" src="https://cdn.l9eloboosting.com/img/tiers2/iron.png" />
                                    <select class="rank__widget__select" name="desired_tier" onchange="rankState(this, 'action4')">
                                        @foreach($tiers as $tier)
                                        @if($tier->id <= 7)
                                        <option value="{{ $tier->id }}">{{ $tier->name }}</option>
                                        @endif
                                        @endforeach
                                    </select>

                                    <select class="rank__widget__select" name="desired_division" onchange="rankState(this, 'action4')">
                                        @foreach($divisions as $division)
                                        @if($division->id == 1) 
                                        <option value="{{ $division->id }}" disabled>{{ $division->number }}</option>
                                        @elseif($division->id == 2)
                                        <option value="{{ $division->id }}" selected>{{ $division->number }}</option>
                                        @else
                                        <option value="{{ $division->id }}">{{ $division->number }}</option>
                                        @endif
 
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        
                        </div>

                        <div class="calculator__row" id="action4-placements">
                            <div class="calculator__content" style="flex-direction: column;">
                                <div class="rank__widget">
                                    Geçen Sezonki Liginiz
                                    <img class="rank__widget__img" src="https://cdn.l9eloboosting.com/img/tiers2/unranked.png" />
                                    <select class="rank__widget__select" name="current_tier" onchange="rankState(this, 'action4-placements')">
                                        <option value="">Unranked</option>
                                        @foreach($tiers as $tier)
                                        <option value="{{ $tier->id }}">{{ $tier->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="winsFormContainer">
                                İstenen Maç Sayısı:<br />
                                <input type="text" class="winsForm" placeholder="0" onkeyup="updateNetWins(this.value);" />
                                </div>
                            </div>
                        </div>

                        <div class="calculator__row" id="action4-netwins">
                            <div class="calculator__content" style="flex-direction: column;">
                                <div class="rank__widget">
                                    Güncel Liginiz
                                    <img class="rank__widget__img" src="https://cdn.l9eloboosting.com/img/tiers2/iron.png" />
                                    <select class="rank__widget__select" name="current_tier" onchange="rankState(this, 'action4-netwins')">
                                        @foreach($tiers as $tier)
                                        @if($tier-> id < 7)
                                        <option value="{{ $tier->id }}">{{ $tier->name }}</option>
                                        @endif
                                        @endforeach
                                    </select>

                                    <select class="rank__widget__select" name="current_division" onchange="rankState(this, 'action4-netwins')">
                                        @foreach($divisions as $division)
                                        <option value="{{ $division->id }}">{{ $division->number }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="winsFormContainer">
                                İstenen Maç(Win/Kazanç) Sayısı:<br />
                                <input type="text" class="winsForm" placeholder="0" onkeyup="updateNetWins(this.value);" />
                                </div>
                            </div>
                        </div>


                        <div class="extras hidden">
                                <span class="extras__title">Ekstralar:</span><br />
                                
                                <div class="extras__row">
                                    <div class="rank__widget">
                                        Booster'ının rollerini seçmek ister misin?:

                                        <label class="switch" for="roleCheckbox">
                                        <input type="checkbox" onchange="switchRoles(); resultState.priceListener(); rolesListener();" id="roleCheckbox">
                                        <span class="slider round"></span>
                                        </label>
                                        
                                        <div class="hidden" id="rolesForm">
                                        <select class="rank__widget__select" name="firstRole" onChange="disableRole(this.selectedIndex); resultState.priceListener(); rolesListener();">
                                            <option value="1">Üst</option>
                                            <option value="2">Orta</option>
                                            <option value="3">Orman</option>
                                            <option value="4">Nişancı</option>
                                            <option value="5">Destek</option>
                                        </select>

                                        <select class="rank__widget__select" name="secondRole" onChange="rolesListener();">
                                            <option value="1" disabled>Üst</option>
                                            <option value="2" selected>Orta</option>
                                            <option value="3">Orman</option>
                                            <option value="4">Nişancı</option>
                                            <option value="5">Destek</option>
                                        </select>
                                        </div>
                                    </div>

                                    <div class="rank__widget">
                                        Booster'ının sihirdar büyülerini seçmek ister misin?:

                                        <label class="switch" for="spellCheckbox">
                                        <input type="checkbox" onchange="switchSpells(); resultState.priceListener(); spellsListener();" id="spellCheckbox">
                                        <span class="slider round"></span>
                                        </label>
                                        <div class="hidden" id="spellsForm">
                                        <select class="rank__widget__select" name="firstSpell" onChange="disableSpell(this.selectedIndex); resultState.priceListener(); spellsListener();">
                                            <option value="1">Bariyer</option>
                                            <option value="2">Arındır</option>
                                            <option value="3">Bitkinlik</option>
                                            <option value="4">Sıçra</option>
                                            <option value="5">Hayalet</option>
                                            <option value="6">Şifa</option>
                                            <option value="7">Tutuştur</option>
                                            <option value="8">Çarp</option>
                                            <option value="9">Işınlan</option>
                                        </select>

                                        <select class="rank__widget__select" name="secondSpell" onChange="spellsListener();">
                                            <option value="1" disabled>Bariyer</option>
                                            <option value="2" selected>Arındır</option>
                                            <option value="3">Bitkinlik</option>
                                            <option value="4">Sıçra</option>
                                            <option value="5">Hayalet</option>
                                            <option value="6">Şifa</option>
                                            <option value="7">Tutuştur</option>
                                            <option value="8">Çarp</option>
                                            <option value="9">Işınlan</option>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="extras__row hidden" id="champsForm">
                                    <div style="margin: 3rem auto 0 auto">
                                    Booster'ının hangi şampiyonları oynamasını tercih edersin?:
                                    <input type="text" class="extras__form" name="selectedChamps" onkeyup="champsListener()" placeholder="Şampiyonların isimlerini buraya yaz, her birinin adını virgül ile ayır." />
                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="checkout">
                        <div class="hidden"><b>Tek/Çift:</b> <div id="option1"></div></div>
                        <div class="hidden"><b>Dereceli Sıra:</b> <div id="option2"></div></div>
                        <div class="hidden"><b>Hizmet(Boost) Türü:</b> <div id="option3"></div></div>
                        <div class="hidden"><b>Güncel Lig:</b> <div id="current__rank"></div></div>
                        <div class="hidden"><b>İstenen Lig:</b> <div id="desired__rank"></div></div>
                        <div class="hidden"><b>Win Sayısı(Net Wins):</b> <div id="netWins"></div></div>
                        <div class="hidden extraResults"><b>Ekstralar</b>
                        
                            <div id="roles" class="hidden"></div>
                            <div id="spells" class="hidden"></div>
                            <div id="champs" class="hidden"></div>
                        </div>
                        <div class="hidden"><b>Fiyat:</b> <div id="price"></div></div>
                        <div style="margin: 1rem 0;">
                            <b>Sunucu:</b> 
                            <select class="server__selection" name="server" onchange="resultState.setServer(this); resultState.priceListener();">
                                <option value="0">Türkiye(TR)</option>
                                <option value="1">Avrupa Batı(EUW)</option>
                                <option value="2">Avrupa Kuzey ve Doğu(EUNE)</option>
                                <option value="3">Rusya(RU)</option>
                            </select>
                        </div>

                        <input type="button" class="btn btn--primary" style="cursor: pointer; display: none;" id="submitForm" value="Checkout" />
                    </div>
                </div>
            </section>
        </div>
    </section>
    @include('includes/user-feedback');
    @include('includes/global-features');

    <script>

    document.querySelector('#submitForm').addEventListener('click', function(e) {
        e.preventDefault();

        if(resultState.validate()) {
            let form = document.createElement('form');
            form.action = '{{ route("cart.add") }}';
            form.method = 'POST';

            function createInput(name, value) {
                if(Array.isArray(value)) {
                    let inputArray = [];
                    for(val of value) {
                        let newInput = createInput(name, val);
                        inputArray.push(newInput);
                    }

                    return inputArray;
                }

                let newInput = document.createElement('input');
                newInput.name = name;
                newInput.value = value;
                return newInput;
            }


            let soloType = createInput('soloType', resultState.option1);
            let queueType = createInput('queueType', resultState.option2);
            let boostType = createInput('boostType', resultState.option3);
            let server = createInput('server', resultState.server);
            let csrf = createInput('_token', '{{ csrf_token() }}');
           

            for(a of [soloType, queueType, boostType, server, csrf]) {
                form.appendChild(a);
            }

            //if solo then include extras
            if(resultState.option1 == 1 && resultState.roleSwitch) {
                let extraRoles = createInput('extraRoles[]', resultState.roles);
                form.appendChild(extraRoles[0]);
                form.appendChild(extraRoles[1]);
            }
            if(resultState.option1 == 1 && resultState.spellSwitch) {
                let extraSpells = createInput('extraSpells[]', resultState.spells);
                form.appendChild(extraSpells[0]);
                form.appendChild(extraSpells[1]);
            }

            if(resultState.option3 == 1) {
                let currentTier = createInput('currentTier', resultState.currentTier);
                let currentDivision = createInput('currentDivision', resultState.currentDivision);
                let currentLP = createInput('currentLP', resultState.lp);

                let desiredTier = createInput('desiredTier', resultState.desiredTier);
                let desiredDivision = createInput('desiredDivision', resultState.desiredDivision ? resultState.desiredDivision : 1);

                for(a of [currentTier, currentDivision, currentLP, desiredTier, desiredDivision]) {
                    form.appendChild(a);
                }
            }else if(resultState.option3 == 2) {
                if(resultState.currentTier) {
                    let currentTier = createInput('currentTier', resultState.currentTier);
                    form.appendChild(currentTier);
                }
                let netWins = createInput('netWins', resultState.netWins);
                form.appendChild(netWins);

            }else if(resultState.option3 == 3) {
                let currentTier = createInput('currentTier', resultState.currentTier);
                let currentDivision = createInput('currentDivision', resultState.currentDivision);
                let netWins = createInput('netWins', resultState.netWins);
                form.appendChild(currentTier);
                form.appendChild(currentDivision);
                form.appendChild(netWins);
            }
            


            document.body.appendChild(form);
            form.submit();

        }else {
            alert('You forgot to fill out something!');
        }
    });

    function champsListener() {
        let champs = document.querySelector('input[name=selectedChamps]').value;
        let champResult = document.querySelector('.extraResults #champs');
        if(champs.length == 0){
            champResult.classList.add('hidden');
        }else {
            let alteredChamps = champs.replaceAll(',', ' > ');
            champResult.innerHTML = '<b>Champs:</b> '+alteredChamps;
            champResult.classList.remove('hidden');
        }
    }

    function spellsListener() {
        let firstRole = document.querySelector('select[name=firstSpell]');
        let secondRole = document.querySelector('select[name=secondSpell]');
        let roleResult = document.querySelector('.extraResults #spells');
        resultState.spells = [firstRole.options[firstRole.selectedIndex].value, secondRole.options[secondRole.selectedIndex].value];
        roleResult.innerHTML = '<b>Sihirdar Büyüleri:</b> '+firstRole.options[firstRole.selectedIndex].innerText + ' | ' + secondRole.options[secondRole.selectedIndex].innerText;
    }

    function rolesListener() {
        let firstRole = document.querySelector('select[name=firstRole]');
        let secondRole = document.querySelector('select[name=secondRole]');
        let roleResult = document.querySelector('.extraResults #roles');
        resultState.roles = [firstRole.options[firstRole.selectedIndex].value, secondRole.options[secondRole.selectedIndex].value];
        roleResult.innerHTML = '<b>Roller:</b> '+firstRole.options[firstRole.selectedIndex].innerText + ' | ' + secondRole.options[secondRole.selectedIndex].innerText;
    }
    function switchSpells(off = null) {
        let spellResult = document.querySelector('.extraResults #spells');
        let champResult = document.querySelector('.extraResults #champs');
        if(off == false) {
            resultState.spellSwitch = false;
            document.querySelector('#spellsForm').classList.add('hidden');
            document.querySelector('#champsForm').classList.add('hidden');
            if(!resultState.roleSwitch) document.querySelector('.extraResults').classList.add('hidden');

            document.querySelector('#spellCheckbox').checked = false;
            spellResult.classList.add('hidden');
            champResult.classList.add('hidden');
        }else if(off == null) {
            document.querySelector('#spellsForm').classList.toggle('hidden');
            document.querySelector('#champsForm').classList.toggle('hidden');
            resultState.spellSwitch = !resultState.spellSwitch;
            if(!resultState.roleSwitch) document.querySelector('.extraResults').classList.toggle('hidden');
            spellResult.classList.toggle('hidden');
            champResult.classList.toggle('hidden');
        }
    }

    function disableSpell(selectedIndex) {
        let secondSpell = document.querySelector('select[name=secondSpell]');
        for(let i = 0; i<secondSpell.options.length; i++) {
            secondSpell.options[i].removeAttribute('disabled');
        }
        secondSpell.options[selectedIndex].setAttribute('disabled', 'disabled');
        if(secondSpell.selectedIndex == selectedIndex) {
            secondSpell.selectedIndex = (secondSpell.selectedIndex-1 >= 0) ? secondSpell.selectedRole-1 : secondSpell.selectedIndex+1;
        }
    }

    function switchRoles(off = null) {
        let roleResult = document.querySelector('.extraResults #roles');
        if(off == false) {
            resultState.roleSwitch = false;
            document.querySelector('#rolesForm').classList.add('hidden');
            if(!resultState.spellSwitch) document.querySelector('.extraResults').classList.add('hidden');
            document.querySelector('#roleCheckbox').checked = false;
            roleResult.classList.add('hidden');
        }else if(off == null) {
            document.querySelector('#rolesForm').classList.toggle('hidden');
            resultState.roleSwitch = !resultState.roleSwitch;
            if(!resultState.spellSwitch) document.querySelector('.extraResults').classList.toggle('hidden');
            roleResult.classList.toggle('hidden');
        }

        
    }
    function disableRole(selectedIndex) {
        let secondRole = document.querySelector('select[name=secondRole]');
        for(let i = 0; i<secondRole.options.length; i++) {
            secondRole.options[i].removeAttribute('disabled');
        }
        secondRole.options[selectedIndex].setAttribute('disabled', 'disabled');
        if(secondRole.selectedIndex == selectedIndex) {
            secondRole.selectedIndex = (secondRole.selectedIndex-1 >= 0) ? secondRole.selectedRole-1 : secondRole.selectedIndex+1;
        }
    }
    let resultState = {
        optionLabels: {
            option1: ['Solo(Tekli)', 'Duo(İkili)'],
            option2: ['5v5 Dereceli Tek/Çift', '5v5 Dereceli Esnek'],
            option3: ['Lig Yükseltme', 'Yerleştirme Maçları', 'Win Boost']
        },
        lp: 0,
        spellSwitch: false,
        roleSwitch: false,
        server: 'TR',
        currentTier: 1,
        currentDivision: 1,
        desiredTier: 1,
        desiredDivision: 2,

        validate() {
            return true;
        },
        
        setOption(num, val) {
            this['option'+num] = val;
            this.optionListener(num, val);
        },
        setServer(e) {
            if(e.selectedIndex == 0) this.server = 'TR';
            if(e.selectedIndex == 1) this.server = 'EUW';
            if(e.selectedIndex == 2) this.server = 'EUNE';
            if(e.selectedIndex == 3) this.server = 'RU';
        },
        
        optionListener(num, val) {
            let optionDom = document.querySelector('#option'+num);
            optionDom.parentElement.classList.remove('hidden');
            optionDom.innerText = this.optionLabels['option'+num][val-1];
        },
        priceListener() {
            const rankId = function (tier, division) {
                return (4 * (parseInt(tier-1)) + parseInt(division));
            }

            const addPercentage = function (total, percent)
            {
                return total + (total / 100) * percent;
            }

            
            let optionActions = ['action4', 'action4-placements', 'action4-netwins'];
            let currentTier = document.querySelector('#'+optionActions[this.option3-1]+' select[name=current_tier]');
            let currentTierVal = (currentTier.options[currentTier.selectedIndex].value) ? currentTier.options[currentTier.selectedIndex].value : 0;

            let currentDivision = '';
            let currentDivisionVal = 0;
            let price = 0;
            let duoPercent = 0;
            let serverPercent = 0;
            let lpDiscount = 0;
            let euwPercent = 0;

            //get server percentage
            if(this.server != "TR") serverPercent = 50; // 50%

            if(this.option3 == 1 || this.option3 == 3) {
                currentDivision = document.querySelector('#'+optionActions[this.option3-1]+' select[name=current_division]');
                currentDivisionVal = currentDivision.options[currentDivision.selectedIndex].value;
            }
            if(this.option3 == 1) {
                const rankPrices = [@foreach($prices as $price){{ $price->price }}, @endforeach];
                let desiredTier = document.querySelector('#'+optionActions[this.option3-1]+' select[name=desired_tier]');
                let desiredTierVal = desiredTier.options[desiredTier.selectedIndex].value;
                let desiredDivision = document.querySelector('#'+optionActions[this.option3-1]+' select[name=desired_division]');
                let desiredDivisionVal = desiredDivision.options[desiredDivision.selectedIndex].value;
                
                let currentRankID = rankId(currentTierVal, currentDivisionVal);
                let desiredRankID = rankId(desiredTierVal, desiredDivisionVal);


                //set division boost settings
                if(this.server != "TR" && this.option1 == 2) {
                    serverPercent = 70; // 70%
                }
                duoPercent = 200; // 200%

                if(desiredRankID <= currentRankID) {
                    price = 0;
                }else {

                    /*price = rankPrices.filter((el, index) => {

                        if(index >= currentRankID &&) return false;
                        if(index > currentRankID-1 && index < desiredRankID-1) {

                            return true;
                        }else { return false; }
                    }).reduce((a,b) => a+b, 0);*/

                    
                    for(let i = currentRankID; i<desiredRankID; i++) {
                        //calculate LP discount for first rank (if start is iron 4, we discount iron 3...iron4 > iron3)
                        if(i == currentRankID) {
                            if (this.lp >= 20 && this.lp <= 39) {
                                lpDiscount = -10; // -10% discount
                            } else if (this.lp >= 40 && this.lp <= 59) {
                                lpDiscount = -20; // -20% discount
                            } else if (this.lp >= 60 && this.lp <= 79) {
                                lpDiscount = -30; // -30% discount
                            } else if (this.lp >= 80) {
                                lpDiscount = -40; // -40% discount
                            }
                        }else {
                            lpDiscount = 0;
                        }


                        //get discounted price based on LP
                        let discounted = addPercentage(rankPrices[i], lpDiscount);

            
                        //apply euw specific pricings
                        if (this.server == "EUW" && i >= 21) {
            
                            switch(i) {
                                case 21:
                                    euwPercent = 150; //increase d4 -> d3 by 150%
                                    break;
                                case 22:
                                    euwPercent = 235; //increase d3 -> d2 by 235%
                                    break;
                                case 23:
                                    euwPercent = 312; //increase d2 -> d1 by 312%
                                    break;
                                case 24:
                                    euwPercent = 325; //increase d1 -> master by 325%
                                    break;
                            }
                            price += addPercentage(discounted, euwPercent);
                            continue;
                        }

        
                        price += addPercentage(discounted, serverPercent);
                    }//end for loop

                }

            }else if(this.option3 == 2) {
                const placementPrices = {id0: 9, @foreach($placementPrices as $price) id{{ $price->tier_id }}: {{ $price->price }},@endforeach};
                let currentRankID = currentTierVal;
                price = addPercentage(placementPrices['id'+currentRankID]*this.netWins, serverPercent);
                duoPercent = 20; //20 percent

            }else if(this.option3 == 3) {

                //set netwins boost settings
                if (this.server == "EUW" && currentTierVal == 6) { //6 = diamond tier
                    serverPercent = 185; // 185%
                }
                duoPercent = 50; // 50%

                const netPrices = {@foreach($netPrices as $price) id{{ $price->rank_id }}: {{ $price->price }},@endforeach};
                let currentRankID = rankId(currentTierVal, currentDivisionVal);
                price = addPercentage(netPrices['id'+currentRankID]*this.netWins, serverPercent);

            }


            if(this.option1 == 1) {
                if(this.roleSwitch) {
                    let rolePrices = {
                        Üst: 1.25,
                        Orman: 1.20,
                        Orta: 1.20,
                        Nişancı: 1.30,
                        Destek: 1.30
                    };

                    let role = document.querySelector('select[name=firstRole]');
                    price = (price*rolePrices[role.options[role.selectedIndex].innerText]).toFixed(2);
                }

                if(this.spellSwitch) {
                    price = ((Math.floor(100 * price) / 100)+10).toFixed(2);
                }

            }else if(this.option1 == 2) {
                price = addPercentage(price, duoPercent);
            }


            price = (Math.floor(100 * price) / 100).toFixed(2);
            document.querySelector('#price').parentElement.classList.remove('hidden');
            document.querySelector('#price').innerText = price;

            document.querySelector('#submitForm').style.display = "block";
        },

        setNetWins(val) {
            let newVal = parseInt(val) ? Math.round(Math.abs(val)) : 0;
            this.netWins = newVal;
            let netWins = document.querySelector('#netWins');
            netWins.parentElement.classList.remove('hidden');
            netWins.innerText = newVal;
        },

        rankListener(type) {
            let optionActions = ['action4', 'action4-placements', 'action4-netwins'];
                let tier = document.querySelector('#'+optionActions[this.option3-1]+' select[name='+type+'_tier]');
                let division = document.querySelector('#'+optionActions[this.option3-1]+' select[name='+type+'_division]');
                let rankResult = document.querySelector('#'+type+'__rank');
                let innerRankText = '';
                rankResult.parentElement.classList.remove('hidden');

                if(this.option3 > 1) {
                    document.getElementById('desired__rank').parentElement.classList.add('hidden');
                }

                if(this.option3 != 2) {
                    //if its masters+
                    if(tier.options[tier.selectedIndex].value >= 7) {
                        innerRankText = tier.options[tier.selectedIndex].innerText
                    }else {
                        innerRankText = tier.options[tier.selectedIndex].innerText + ' ' + division.options[division.selectedIndex].innerText;
                    }
                }else {
                    innerRankText = tier.options[tier.selectedIndex].innerText;
                }

                rankResult.innerText = innerRankText;
        }

    };
    let intro = document.getElementById('intro');


    let actions = {
        intro: (e) => {
            e.preventDefault();
            if(!e.target.dataset.option) { return false; }

            //remove seelected class from nearby buttons
            let optionButtons = document.querySelectorAll('#intro .option');
            optionButtons.forEach((element) => {
                element.classList.remove('selected');
            });

            //mark this button as selected
            e.target.classList.add('selected');

            let optionLabels = ['Solo', 'Duo'];
            let optionVal = e.target.dataset.option;
            let container = document.getElementById('action2');
            container.classList.add('calculator__row--visible');

            //add event for next step/action
            container.addEventListener('click', actions.action2);

            //update result state and text
            resultState.setOption(1, optionVal);

            if(optionVal == 2) {
                document.querySelector('.extras').classList.add('hidden');
                switchRoles(false);
                switchSpells(false);
                let desiredTiers = document.querySelector('select[name=desired_tier]');
                desiredTiers.options[6].setAttribute('disabled', 'disabled');

                let currentTierSelect = document.querySelector('#action4 select[name=current_tier]');
                currentTierSelect.options[5].setAttribute('disabled', 'disabled');
                if(desiredTiers.selectedIndex >= 5) {
                    desiredTiers.selectedIndex = 5;
                    document.querySelector('select[name=desired_division]').selectedIndex = 0;

                    currentTierSelect.selectedIndex = 0;
                    
                    rankState(currentTierSelect, 'action4');
                    changeRankImage(desiredTiers.previousElementSibling, 'Diamond');
                    resultState.priceListener();
                }
            }else {
                let currentTierSelect = document.querySelector('#action4 select[name=current_tier]');
                currentTierSelect.options[5].removeAttribute('disabled');
                let desiredTiers = document.querySelector('select[name=desired_tier]');

                desiredTiers.options[6].removeAttribute('disabled');
                rankState(desiredTiers, 'action4');
            }
            if(optionVal != 2 && resultState.option3){
                document.querySelector('.extras').classList.remove('hidden');
            }

            //update price to reflect duo option
            resultState.priceListener();
        },
        action2: (e) => {
            e.preventDefault();
            if(!e.target.dataset.option) { return false; }

            //remove seelected class from nearby buttons
            let optionButtons = document.querySelectorAll('#action2 .option');
            optionButtons.forEach((element) => {
                element.classList.remove('selected');
            });

            //mark this button as selected
            e.target.classList.add('selected');

            let optionVal = e.target.dataset.option;
            let container = document.getElementById('action3');
            container.classList.add('calculator__row--visible');

            //add event for next step/action
            container.addEventListener('click', actions.action3);

            //update result state and text
            resultState.setOption(2, optionVal);
        },
        action3: (e) => {
            e.preventDefault();
            if(!e.target.dataset.option) { return false; }

            //remove seelected class from nearby buttons
            let optionButtons = document.querySelectorAll('#action3 .option');
            optionButtons.forEach((element) => {
                element.classList.remove('selected');
            });

            //mark this button as selected
            e.target.classList.add('selected');

            let optionActions = ['action4', 'action4-placements', 'action4-netwins'];
            let optionVal = e.target.dataset.option;
            let container = document.getElementById(optionActions[optionVal-1]);
            container.style.display = "block";
            for(val of optionActions) {
                    if(val != optionActions[optionVal-1]) {
                    document.getElementById(val).style.display = "none";
                    document.getElementById(val).classList.remove('calculator__row--visible');
                }
            }
            container.classList.add('calculator__row--visible');

            //update result state and text
            resultState.setOption(3, optionVal);
            resultState.rankListener('current');
            if(resultState.option3 == 1) {
                resultState.rankListener('desired');
                let netWinsContainer = document.querySelector('#netWins');
                netWinsContainer.parentElement.classList.add('hidden');
                resultState.priceListener();

            }else {
                let currentNetWins = document.querySelector('#'+optionActions[optionVal-1]+' .winsForm');
                updateNetWins(currentNetWins.value);
            }

            rankState(document.querySelector('#'+optionActions[optionVal-1]+' select[name=current_tier]'), optionActions[optionVal-1]);

            if(resultState.option1 == 1) {
                document.querySelector('.extras').classList.remove('hidden');
            }
            
           
        }
    }
    

    intro.addEventListener('click', actions.intro);

    function discountLP(value) {
        let discountElement = document.querySelector("#discountDiv");
        if(value > 19) {
            discountElement.style.display = "block";
        }else {
            discountElement.style.display = "none";
        }
        resultState.lp = value ? value : 0;
    }
    function changeRankImage(e, tierName) {
        let splitUrl = e.src.split('tiers2/');
        e.src = splitUrl[0]+'tiers2/'+tierName+'.png';
    }

    function updateNetWins(val) {
        resultState.setNetWins(val);
        resultState.priceListener();
    }

    function rankState(e, action4Container) {
        let name = e.name.split('_');

        let currentTier = name[1] == 'tier' ? e : document.querySelector('#'+action4Container+' select[name=current_tier]');
        let currentDivision = name[1] == 'division' ? e : document.querySelector('#'+action4Container+' select[name=current_division]');

        if(resultState.option3 == 1) {
            let desiredTier = document.querySelector('select[name=desired_tier]');
            let desiredDivision = document.querySelector('select[name=desired_division]');

            if(name[0] == 'current') {

                if(currentTier.selectedIndex >= desiredTier.selectedIndex && currentDivision.selectedIndex == 3) {
                    desiredTier.selectedIndex = currentTier.selectedIndex+1;
                    desiredDivision.selectedIndex = 0;
                    changeRankImage(desiredTier.previousElementSibling, desiredTier.options[desiredTier.selectedIndex].innerText);
                }
                else if(currentTier.selectedIndex == desiredTier.selectedIndex && currentDivision.selectedIndex >= desiredDivision.selectedIndex) {
                    desiredDivision.selectedIndex = currentDivision.selectedIndex + 1;
                }
                else if(currentTier.selectedIndex > desiredTier.selectedIndex) {
                    desiredTier.selectedIndex = currentTier.selectedIndex;
                    changeRankImage(desiredTier.previousElementSibling, desiredTier.options[desiredTier.selectedIndex].innerText);
                }
            }

            if(desiredTier.selectedIndex == 5 && resultState.option1 == 2) {
                    desiredDivision.options[1].setAttribute('disabled', 'disabled');
                    desiredDivision.options[2].setAttribute('disabled', 'disabled');
                    desiredDivision.options[3].setAttribute('disabled', 'disabled');
                    desiredDivision.selectedIndex = 0;
            }else {
                    desiredDivision.options[1].removeAttribute('disabled');
                    desiredDivision.options[2].removeAttribute('disabled');
                    desiredDivision.options[3].removeAttribute('disabled');
            }
            
            desiredDivision.options[0].removeAttribute("disabled");
            resultState.rankListener('desired');
            if(desiredTier.options[desiredTier.selectedIndex].value >= 7) {
                desiredDivision.selectedIndex = 0;
                desiredDivision.style.display = 'none';
            }else {
                desiredDivision.style.display = 'block';
            }

            //update the object states desired rank info
            resultState.desiredTier = desiredTier.options[desiredTier.selectedIndex].value;
            if(desiredDivision.style.display != "none") {
                resultState.desiredDivision = desiredDivision.options[desiredDivision.selectedIndex].value;
            }else {
                resultState.desiredDivision = null;
            }

        }else if(resultState.option3 == 3) {
            if(currentTier.options[currentTier.selectedIndex].value >= 7) {
                currentDivision.selectedIndex = 0;
                currentDivision.style.display = 'none';
            }else {
                currentDivision.style.display = 'block';
            }
        }


        //update result state current rank info
        let currTier = document.querySelector('#'+action4Container+' select[name=current_tier]');
        let currDivision = document.querySelector('#'+action4Container+' select[name=current_division]');

        resultState.currentTier = currTier.options[currTier.selectedIndex].value;

        if(currDivision) {
            if(currDivision.style.display != "none") {
                resultState.currentDivision = currDivision.options[currDivision.selectedIndex].value;
            }else {
                resultState.currentDivision = null;
            }
        }else {
            resultState.currentDivision = null;
        }

        if(name[1] == 'tier') changeRankImage(e.previousElementSibling, e.options[e.selectedIndex].innerText);

        resultState.rankListener('current');
        resultState.priceListener();

    }

    </script>
</x-layout_boost>
