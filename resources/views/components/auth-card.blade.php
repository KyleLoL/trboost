<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0" style="background: url(https://cdna.artstation.com/p/assets/images/images/015/608/426/large/artur-sadlos-leg-more-sh280-background-as-v005.jpg?1548951828)">
    <div>
        {{ $logo }}
    </div>

    <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg">
        {{ $slot }}
    </div>
</div>
