<!DOCTYPE html>
<html lang="tr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords"
        content="eloboost, lol boost, tr serveri boost, west boost, lol koçluk, hesap satışı, lol elo boost, lol boost, lol hesap, smurf hesap, lol boost, smurf, ucuz, kaliteli, profesyonel , hızlı, teslim, eloboost, elo, boost, boss, poro, league of legends, hızlı, boost"/>
    <meta name="description" content="Türkiye'nin en kaliteli ELO Boost hizmetleri! İşlem sürecindeki her tarafın haklarını eşit şekilde korunurken, kaliteden ödün verilmediği adres.">
    <title>{{ $title ?? "EloBoostTR | Türkiye'nin En İyi ELO Boosting Hizmetleri" }}</title>
    <link rel="shortcut icon" type="image/x-icon" href="images/eloboosttr-logo.png" />
	<script>
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            let headerEl = document.getElementById("header__navbar");
            let navbarListEl = document.getElementById("navbar__list");
            let headerHamburger = document.getElementById("header__hamburger");
            if (window.pageYOffset > 0 || document.documentElement.scrollTop > 0 || document.body.scrollTop > 0) {
                headerEl.classList.add('header__navbar--fixed');
                navbarListEl.classList.add('navbar__list--fixed');
                headerHamburger.classList.add('hamburger--fixed');
            } else {
                headerEl.classList.remove('header__navbar--fixed');
                navbarListEl.classList.remove('navbar__list--fixed');
                headerHamburger.classList.remove('hamburger--fixed');
            }
        }

        window.onload = function() {
            // Look for .hamburger
            let hamburger = document.getElementById("header__hamburger");
            let navbarListEl = document.getElementById("navbar__list");
            let reviewListEl = document.getElementById("feedback__list");
            let feedbackBoxEl = document.getElementById("feedback__box");
            let reviewArrow = document.getElementById("feedback__box__arrow");
            let reviewBoxTextEl = document.getElementById("feedback__box__text");
            let reviews = [
    "Hesabımı Gümüş IV'ten Altın IV'e kadar oldukça hızlı ve profesyonel bir şekilde yükselttiler! Booster oldukça başarılıydı, kesinlikle onu tekrar tercih edeceğim.", 
    "Platin IV'ten Platin II'ye bir duo boost hizmeti satın aldım. Booster elbette başarılı bir şekilde hizmeti tamamladı ama beni onun hakkında gerçekten etkileyen en temel etmen bana karşı kibar ve bir o kadar da profesyonel olan davranışlarıydı.", 
    "Elmas I'den Ustalık aşamasına bir solo boost hizmeti satın aldım, eninde sonunda tamamlamalarını zaten bekliyordum ama bu kadar hızlı tamamlayabilecek kalitede oyuncularla çalıştıklarını tahmin dahi edememiştim!", 
    "Gümüş ELO'da birkaç maç Win Boost hizmeti satın aldım, hizmetime beklediğim kadar kısa sürede bir Booster atanmadı. Yönetim ile iletişime geçtim, paramı direk iade ettiler. Bu yanlarını takdir ediyorum."    ];

            // On click
            hamburger.addEventListener("click", function() {
                // Toggle class "is-active"
                hamburger.classList.toggle("is-active");
                navbarListEl.classList.toggle("navbar__visible");
            });

            reviewListEl.addEventListener("click", function(e) {
                var index = e.target.closest(".feedback__list__item").dataset.index;
                var reviewText = reviews[index-1];
                reviewBoxTextEl.innerText = reviewText;
                reviewArrow.style.left = ((feedbackBoxEl.getBoundingClientRect().width - ((e.target.getBoundingClientRect().width+20)*4)) / 2) + ((e.target.getBoundingClientRect().width+20)*index) - ((e.target.getBoundingClientRect().width+48) / 2)+"px";
            });


        };

    </script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">
    <link href="{{ asset('css/icon-font/lineicons.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/hamburger.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    @stack('sheets')
    
    <style> 
    .footer_widget_contact_list_element
    {
     border: 2px solid white;
     font-size: 0.82rem;
    }

    .btn--primary:hover {
      color: black;
      border-color: black;
    }
    .ssl-secure-section {
	width: 100%;
	padding-bottom: 20px;
	padding-top: 10px;
}

    #paytr-logos {
    	margin-right: 50px;
    }
    body{background-color:#f8f8f8}
  .guarantee {
  border-top-left-radius: 37px 140px;
  border-top-right-radius: 23px 130px;
  border-bottom-left-radius: 110px 19px;
  border-bottom-right-radius: 120px 24px;

  display: block;
  position: relative;
  border: solid 3px #6e7491;
  padding: 40px 60px;
  max-width: 800px;
  width: 70%;
  margin: 100px auto 0;
  font-size: 17px;
  line-height: 28px;
  transform: rotate(-1deg);
  box-shadow: 3px 15px 8px -10px rgba(0, 0, 0, 0.3);
  transition: all 0.13s ease-in;
}

.guarantee:hover {
  transform: translateY(-10px) rotate(1deg);
  box-shadow: 3px 15px 8px -10px rgba(0, 0, 0, 0.3);
}

.guarantee:hover .border {
  transform: translateY(4px) rotate(-5deg);
} 
   </style>
    <script src="https://use.fontawesome.com/d983741785.js"></script>
    <script src="https://kit.fontawesome.com/870b7dbf87.js" crossorigin="anonymous"></script>
    @stack('header-scripts')
</head>
<body>
<header class="header {{ $heroClass ?? '' }}">
    <div class="header__navbar {{ $transparentMenu ?? '' }}" id="header__navbar">
        <nav class="navbar flex">
            <a href="/" class="navbar__brand">EloBoostTR</a>

            <ul class="navbar__list flex" id="navbar__list">
                <li><a href="/" class="navbar__list__item navbar__list__item--active">Ana Sayfa</a></li>
                <li><a href="{{ route('boost') }}" class="navbar__list__item">Boost Hizmetleri</a></li>
                <li><a href="{{ route('demopage') }}" class="navbar__list__item">Demo</a></li>
                <li><a href="{{ route('contact_us') }}" class="navbar__list__item">İletişim</a></li>
                <li><a href="{{ route('cart.index') }}" class="navbar__list__item"><i class="lni lni-cart"></i> Cart</a></li>
                <li><a href="{{ route('login') }}" class="navbar__list__button btn btn--primary">Giriş/Kayıt</a></li>
            </ul>
            <button class="hamburger hamburger--collapse" id="header__hamburger" type="button">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
            </button>
        </nav>
    </div>
    {{ $hero ?? '' }}
</header>

<main>
    {{ $slot }}
</main>

<footer class="footer">
    <div class="subscribe flex">
        <h2><span>Bülten</span>imize üye ol.</h2>
        <div class="form-email">
            <input type="text" class="enteremail" placeholder="E-posta adresini gir.." />
            <span class="highlight"></span>
        </div>
        <input type="button" class="btn btn--primary subbutton" value="Üye Ol" />
    </div>
    <div class="footer__wrapper">
        <img src="./images/garen.png" class="footer__mascot" />
        <div class="footer__widgets flex">
            <div class="footer__widget footer__widget--about">
                <h4 class="footer__widget__header">Hakkımızda</h4>
                <p class="footer__about__text">EloBoostTR.com kaliteden ödün verilmeden, söz konusu platformlarda uzman Booster'ların; hızlı, kaliteli ve güvenilir hizmeti arayan ve hakeden müşterilerimiz ile bir araya geldikleri; sözü verilen hizmetlerin uygunca sunulumu garanti altına alınırken, her iki tarafın da çıkarlarının uygun ve adil şekilde korunduğu adrestir. EloBoostTR.com Lig(Aşama) Yükseltme, Win Boost(Net Wins) ve Yerleştirme Maçları hizmetleri sunmaktadır. Eğer maksimum kalite, profesyonel hizmet ve güvenilirliği olabilecek en uygun fiyatlara arıyorsanız, doğru yerdesiniz!</p>
                <div class="footer__about__info">
                    EloBoostTR, Riot Games Inc veya League of Legends TM ile hiçbir şekilde bağlantılı değildir, ilişkilendirilmez veya onlar tarafından desteklenmez.
                </div>
            </div>

            <div class="footer__widget footer__widget--list">
                <h4 class="footer__widget__header">Bilgilendirme</h4>
                <ul class="footer__widget__list">
                    <li><a href="{{ route('about') }}" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> Hakkımızda</a></li>
                    <li><a href="{{ route('join_us') }}" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> Bizimle Çalış</a></li>
                    <li><a href="{{ route('terms_of_service') }}" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> Kullanım Sözleşmesi</a></li>
                    <li><a href="{{ route('privacy_policy') }}" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> Gizlilik Sözleşmesi</a></li>
                </ul>
            </div>

            <div class="footer__widget footer__widget--contact">
                <h4 class="footer__widget__header">İletişim</h4>
              <ul class=" mb-20" style="margin-top:50px;">
                <li class="btn btn--primary btn--block footer_widget_contact_list_element"><i class="fa fa-map-marker"></i>  Çankaya, Ankara</li>
                <li class="btn btn--primary btn--block footer_widget_contact_list_element"><i class="fa fa-envelope-o"></i>  support@eloboosttr.com</li>
                <li class="btn btn--primary btn--block footer_widget_contact_list_element" style="font-size: 0.735rem;"><i class="fas fa-balance-scale-right"></i> Vergi No: 1180343860 / Çankaya</li>
              </ul>
            </div>
        </div>
    </div>
    <div class="footer__copyright">Copyright © 2021 <span class="text__highlight"><b>EloBoostTR</b></span>. Bütün hakları saklıdır.</div>
</footer>
@stack('footer-scripts')
<!--Start of Tawk.to Script-->
        <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/60ea64fb649e0a0a5ccb95c1/1fa9pktrl';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
        })();
        </script>
        <!--End of Tawk.to Script-->
</body>
</html>