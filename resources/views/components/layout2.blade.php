<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script>
        window.onscroll = function() {scrollFunction()};

        function scrollFunction() {
            let headerEl = document.getElementById("header__navbar");
            let navbarListEl = document.getElementById("navbar__list");
            let headerHamburger = document.getElementById("header__hamburger");
            if (window.pageYOffset > 0 || document.documentElement.scrollTop > 0 || document.body.scrollTop > 0) {
                headerEl.classList.add('header__navbar--fixed');
                navbarListEl.classList.add('navbar__list--fixed');
                headerHamburger.classList.add('hamburger--fixed');
            } else {
                headerEl.classList.remove('header__navbar--fixed');
                navbarListEl.classList.remove('navbar__list--fixed');
                headerHamburger.classList.remove('hamburger--fixed');
            }
        }

        window.onload = function() {
            // Look for .hamburger
            let hamburger = document.getElementById("header__hamburger");
            let navbarListEl = document.getElementById("navbar__list");
            let reviewListEl = document.getElementById("feedback__list");
            let feedbackBoxEl = document.getElementById("feedback__box");
            let reviewArrow = document.getElementById("feedback__box__arrow");
            let reviewBoxTextEl = document.getElementById("feedback__box__text");
            let reviews = [
    'Boosted my account from silver IV to gold IV very quick and professionally! The booster was pretty good, I will definitely prefer him again!', 
    'I bought a plat IV to plat II duo boosting service. The booster was surely good enought to complete the order but what I really like about him was how gentle and kind he treated me.', 
    'test3', 
    'test4'    ];

            // On click
            hamburger.addEventListener("click", function() {
                // Toggle class "is-active"
                hamburger.classList.toggle("is-active");
                navbarListEl.classList.toggle("navbar__visible");
            });

            reviewListEl.addEventListener("click", function(e) {
                var index = e.target.closest(".feedback__list__item").dataset.index;
                var reviewText = reviews[index-1];
                reviewBoxTextEl.innerText = reviewText;
                reviewArrow.style.left = ((feedbackBoxEl.getBoundingClientRect().width - ((e.target.getBoundingClientRect().width+20)*4)) / 2) + ((e.target.getBoundingClientRect().width+20)*index) - ((e.target.getBoundingClientRect().width+48) / 2)+"px";
            });


        };

    </script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">
    <link href="./css/icon-font/lineicons.css" rel="stylesheet">
    <link rel="stylesheet" href="./css/hamburger.css">
    <link rel="stylesheet" href="./css/style.css">
    <link rel="stylesheet" href="./css/calculator.css">
    <style> 
    .footer_widget_contact_list_element
    {
     border: 2px solid white;
     font-size: 0.82rem;
    }

    .btn--primary:hover {
      color: black;
      border-color: black;
    }
    .ssl-secure-section {
	width: 100%;
	padding-bottom: 20px;
	padding-top: 10px;
}

    #paytr-logos {
    	margin-right: 50px;
    }
    body{background-color:#f8f8f8}
  .guarantee {
  border-top-left-radius: 37px 140px;
  border-top-right-radius: 23px 130px;
  border-bottom-left-radius: 110px 19px;
  border-bottom-right-radius: 120px 24px;

  display: block;
  position: relative;
  border: solid 3px #6e7491;
  padding: 40px 60px;
  max-width: 800px;
  width: 70%;
  margin: 100px auto 0;
  font-size: 17px;
  line-height: 28px;
  transform: rotate(-1deg);
  box-shadow: 3px 15px 8px -10px rgba(0, 0, 0, 0.3);
  transition: all 0.13s ease-in;
}

.guarantee:hover {
  transform: translateY(-10px) rotate(1deg);
  box-shadow: 3px 15px 8px -10px rgba(0, 0, 0, 0.3);
}

.guarantee:hover .border {
  transform: translateY(4px) rotate(-5deg);
} 
   </style>
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>
    <script src="https://use.fontawesome.com/d983741785.js"></script>
    <script src="https://kit.fontawesome.com/870b7dbf87.js" crossorigin="anonymous"></script>
    <script>
        document.addEventListener( 'DOMContentLoaded', function () {
            new Splide( '.splide', {
                type: 'loop',
                direction: 'ttb',
                fixedHeight: '420px',
                height: '420px',
                autoplay: true,
                interval: 2000
            }).mount();
        } );
    </script>
</head>
<body>
<header class="header {{ $heroClass ?? '' }}">
    <div class="header__navbar {{ $transparentMenu ?? '' }}" id="header__navbar">
        <nav class="navbar flex">
            <a href="#" class="navbar__brand">EloBoostTR</a>

            <ul class="navbar__list flex" id="navbar__list">
                <li><a href="/" class="navbar__list__item navbar__list__item--active">Home</a></li>
                <li><a href="{{ route('boost') }}" class="navbar__list__item">LoL Boosting</a></li>
                <li><a href="#" class="navbar__list__item">Blog</a></li>
                <li><a href="#" class="navbar__list__item">Contact Us</a></li>
                <li><a href="{{ route('login') }}" class="navbar__list__button btn btn--primary">Boost Area</a></li>
            </ul>
            <button class="hamburger hamburger--collapse" id="header__hamburger" type="button">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
            </button>
        </nav>
    </div>
    {{ $hero ?? '' }}
</header>

<main>
    {{ $slot }}
</main>

<footer class="footer">
    <div class="subscribe flex">
        <h2>Subscribe To Our <span>Newsletter</span></h2>
        <div class="form-email">
            <input type="text" class="enteremail" placeholder="Enter your email address" />
            <span class="highlight"></span>
        </div>
        <input type="button" class="btn btn--primary subbutton" value="Subscribe" />
    </div>
    <div class="footer__wrapper">
        <img src="./images/garen.png" class="footer__mascot" />
        <div class="footer__widgets flex">
            <div class="footer__widget footer__widget--about">
                <h4 class="footer__widget__header">About Us</h4>
                <p class="footer__about__text">EloBoostTR is a veteran League of Legends elo boosting website. We require each booster to undergo verification so we know your account is in good hands.
                    We offer an array of services,
                    including smurf accounts and coaching. If you’re looking for the cheapest boosting available, you’ve come to the right place!</p>
                <div class="footer__about__info">
                    EloBoostTR is in no way affiliated with, associated with or endorsed by Riot Games, Inc or League of Legends, TM
                </div>
            </div>

            <div class="footer__widget footer__widget--list">
                <h4 class="footer__widget__header">Useful Links</h4>
                <ul class="footer__widget__list">
                    <li><a href="{{ route('about') }}" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> About Us</a></li>
                    <li><a href="{{ route('join_us') }}" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> Join Us</a></li>
                    <li><a href="#" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> Refund Policy</a></li>
                    <li><a href="#" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> Terms of Service</a></li>
                    <li><a href="#" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> Privacy Policy</a></li>
                </ul>
            </div>

            <div class="footer__widget footer__widget--contact">
                <h4 class="footer__widget__header">Contact</h4>
                <ul class="list-inline mb-20">
              <ul class="list-inline mb-20" style="margin-top:50px;">
              <li class="btn btn--primary btn--block footer_widget_contact_list_element"><i class="fa fa-map-marker"></i>  Çankaya, Ankara</li>
              <li class="btn btn--primary btn--block footer_widget_contact_list_element"><i class="fa fa-envelope-o"></i>  support@eloboosttr.com</li>
              <li class="btn btn--primary btn--block footer_widget_contact_list_element" style="font-size: 0.735rem;"><i class="fas fa-balance-scale-right"></i> Vergi No: 1180343860 / Çankaya</li>
            </ul>
            </ul>
            </div>
        </div>
    </div>
    <div class="footer__copyright">Copyright © 2021 <span class="text__highlight"><b>EloBoostTR</b></span>. All Rights Reserved.</div>
</footer>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/60ea64fb649e0a0a5ccb95c1/1fa9pktrl';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
