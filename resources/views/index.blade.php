<x-layout>
    @push('header-scripts')
        <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>
        <script>
        document.addEventListener( 'DOMContentLoaded', function () {
            new Splide( '.splide', {
                type: 'loop',
                direction: 'ttb',
                fixedHeight: '420px',
                height: '420px',
                autoplay: true,
                interval: 2000
            }).mount();
        } );
    </script>
    @endpush

    <x-slot name="heroClass">header--hero</x-slot>
    <x-slot name="transparentMenu">header__navbar--transparent</x-slot>
    <x-slot name="hero">
        <div class="header__hero">
            <section class="hero">
                <div class="splide">
                    <div class="splide__track">
                        <div class="splide__list">
                            <div class="splide__slide">
                                <div class="hero__slide">
                                    <h3>League of Legends</h3>
                                    <h1>Lig Yükseltme</h1>

                                    <p class="hero__slide__text">
                                        Düşük elo takım arkadaşlarıyla oynamaktan sıkıldın mı? Seni kayda değer rakiplerle eşleşebileceğin bir lige yükseltmemize izin ver!
                                        Ne için bekliyorsun?
                                    </p>
                                    <ul class="hero__slide__list flex">
                                        <li class="hero__slide__list__item"><i class="lni lni-checkmark-circle"></i> Profesyonel Booster Kadrosu</li>
                                        <li class="hero__slide__list__item"><i class="lni lni-checkmark-circle"></i> Tartışmasız Uygun/Rekabetçi Fiyatlar</li>
                                        <li class="hero__slide__list__item"><i class="lni lni-checkmark-circle"></i> Hızlı Hizmet</li>
                                        <li class="hero__slide__list__item"><i class="lni lni-checkmark-circle"></i> 7/24 Canlı Destek</li>
                                    </ul>

                                    <a href="#" class="btn btn--primary btn--ripple">Daha Fazla</a>
                                </div>
                            </div>
                            <div class="splide__slide">
                                <div class="hero__slide">
                                    <h3>League of Legends</h3>
                                    <h1>Yerleştirme Maçları</h1>

                                    <p class="hero__slide__text">
                                        Bu sezona güzel bir başlangıç mı yapmak istiyorsun?
                                        İzin ver uzman Booster kadromuz seni sıralamada efektif bir şekilde tırmanabileceğin muhteşem bir noktaya getirsinler!
                                    </p>
                                    <ul class="hero__slide__list flex">
                                        <li class="hero__slide__list__item"><i class="lni lni-checkmark-circle"></i> Profesyonel Booster Kadrosu</li>
                                        <li class="hero__slide__list__item"><i class="lni lni-checkmark-circle"></i> Tartışmasız Uygun/Rekabetçi Fiyatlar</li>
                                        <li class="hero__slide__list__item"><i class="lni lni-checkmark-circle"></i> Hızlı Hizmet</li>
                                        <li class="hero__slide__list__item"><i class="lni lni-checkmark-circle"></i> 7/24 Canlı Destek</li>
                                    </ul>

                                    <a href="#" class="btn btn--primary btn--ripple">Daha Fazla</a>
                                </div>
                            </div>
                            <div class="splide__slide">
                                <div class="hero__slide">
                                    <h3>League of Legends</h3>
                                    <h1>Net Wins</h1>

                                    <p class="hero__slide__text">
                                        Yalnızca birkaç oyunluk mu yardıma ihtiyacın var? Galibiyet/Mağlubiyet oranını yükseltmek için bizim Win Boost(Net Wins) hizmetimizi kullan!
                                        Eğer maç başına aldığın LP miktarı ortalamanın üzerinde ise, en yüksek verimliliği Win Boost hizmetimizden alabilirsin.
                                    </p>
                                    <ul class="hero__slide__list flex">
                                        <li class="hero__slide__list__item"><i class="lni lni-checkmark-circle"></i> Profesyonel Booster Kadrosu</li>
                                        <li class="hero__slide__list__item"><i class="lni lni-checkmark-circle"></i> Tartışmasız Uygun/Rekabetçi Fiyatlar</li>
                                        <li class="hero__slide__list__item"><i class="lni lni-checkmark-circle"></i> Hızlı Hizmet</li>
                                        <li class="hero__slide__list__item"><i class="lni lni-checkmark-circle"></i> 7/24 Canlı Destek</li>
                                    </ul>

                                    <a href="#" class="btn btn--primary btn--ripple">Daha Fazla</a>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
        </div>
    </x-slot>

    <section class="section" style="background-color:#ffffff;">
        <div class="section__wrapper">
            <header class="section__header flex" style="background-color:#f8f8f8; padding-left:50px;">
                <h2 class="section__header__title">Güncel <span>Hizmetlerimiz</span></h2>
                <p class="section__header__sub">Burada hizmet ve ürünlerimiz hakkında bilgi edinebilirsiniz!</p>
            </header>
            <section class="services flex">
                <div class="services__widget flex">
                    <img class="services__widget__image" src="./images/challanger_icon.png" />
                    <div class="services__widget__text">
                        <h4><span>Lig</span> Yükseltme</h4>
                        <p>Lig(Aşama) Yükseltme, Win Boost(Net Win) ve Yerleştirme Maçları hizmetleri sunuyoruz!</p>
                    </div>
                </div>
                <div class="services__widget flex">
                    <img class="services__widget__image"src="./images/icon-02.png" />
                    <div class="services__widget__text">
                        <h4><span>Kaliteli</span> Destek!</h4>
                        <p>Aklına takılan herhangi bir şeyi canlı destek üzerinden bize sorabilirsin!</p>
                    </div>
                </div>
                <div class="services__widget flex">
                    <img class="services__widget__image"src="./images/icon-03.png" />
                    <div class="services__widget__text">
                        <h4><span>Güvenli</span> Hizmetler</h4>
                        <p>En güvenli elo boost hizmetlerini sunmaktayız!</p>
                    </div>
                </div>
            </section>
        </div>
    </section>

    <section class="section">
        <div class="section__wrapper">
            <header class="section__header" style="margin-top:30px;padding-top:15px; padding-bottom:15px;background-color:#ffffff;">
                <h2 class="section__header__title section__header__title--single">Our <span>Vision</span></h2>
            </header>
            <section class="guarantee" style="background-color:#ffffff;padding:30px;">
                <p class="guarantee__text">EloBoostTR yalnızca sıralamanın en üstlerinden son derece özenli ve titiz bir şekilde işe alım görevlilerimiz
                    tarafından uygulanan işe alım prosedürlerimizi geçebilen Booster'lar ile birlikte çalışır.
                    Onların işlerini doğru şekilde yaptığından emin olmak, müşterilerimizin mağdur edilmediğinden emin olmak ve yine müşterilerimize ihtiyaç
                    durumunda her türlü destek ve yardımın sağlanması adına profesyonel bir yönetim ekibimiz de bulunmaktadır. Aklınıza takılan herhangi bir
                    soru için bize gerek Discord sunucumuz üzerinden, gerekse web-sitemiz üzerindeki canlı destek, e-posta adresi veya iletişim formları
                    aracılığı ulaşabilirsiniz!
                </p>

                <p class="guarantee__text">
                   Bizden Lig(Aşama) Yükseltme, Win Boost(Net Wins) veye Yerleştirme Maçları hizmetlerinden herhangi birisini
                   satın almanıza sebep olarak gösterilebilecek bir diğer etmen, rekabetçi fiyat tablomuzdur.
                   Eğer genel olarak internette benzer hizmetleri veren diğer siteleri ziyaret etmişseniz, zaten fiyatlarımızın
                   kaliteli ve güvenilir hizmet sunanlarına kıyasla oldukça uygun olduğunu fark edeceksiniz. Gerek fiyat, gerek hizmetler,
                   gerekse aklınıza takılan herhangi bir diğer konuda bizimle iletişime geçmekten çekinmeyin, uygun çalışma saatlerinde
                   seve seve hızlı bir şekilde desteği sunar, çalışma saatleri dışındaki destek taleplerinizi de en yakın çalışma saatleri
                   içerisinde cevaplarız. Sitemizden herhangi bir hizmeti satın aldıktan sonra sitenin arkaplanındaki kullanıcı arayüzünüzden
                   erişebileceğiniz, siparişinize özel, bir çok muhteşem özelliğin aktif hale geldiğini göreceksiniz! Bu özelliklerin genel
                   olarak nasıl olacağına dair bir demo görmek isterseniz, web sitemiz ana sayfasının navigasyon barından,
                   "DEMO" sayfasını ziyaret ederek bunu yapabilirsiniz.
                </p>
            </section>
        </div>
    </section>

    @include('includes/statistics');
    @include('includes/user-feedback');
    @include('includes/global-features');
    @include('includes/ssl-secure-paytr');
</x-layout>