<x-layout>
    <section class="section">
        <div class="section__wrapper">
            <header class="section__header flex">
                <h2 class="section__header__title">Checkout <span>Page</span></h2>
                <p class="section__header__sub"><a href="/">Home</a> / <span style="color: #ff4719;">Checkout</span></p>
            </header>
            <section>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br />
                @endif
                @if($serializedState)
                    <form method="post" action="{{ route('checkout.submit') }}">
                        @csrf
                        @if($serializedState)
                        <input type="hidden" name="serializedState" value="{{ $serializedState }}" />
                        @endif
                        Full Name: <input type="text" name="fullname" minlength="3" placeholder="John Smith" required /><br />
                        Phone Number: <input type="tel" name="phone" minlength="10" pattern="\+{1,1}[0-9]{2,2}[0-9]{10,}" placeholder="+902995000000" required /><br />
                        Email Address: <input type="email" name="email" required /><br />
                        Summoner Name: <input type="text" name="summonerName" placeholder="Summoner Name.." minlength="3" autocomplete="off" required /><br />
                        
                        @if(!$duoSelected)
                        LoL Username: <input type="text" name="username" placeholder="Username.." minlength="3" autocomplete="off" required /><br />
                        LoL Password: <input type="password" name="password" minlength="3" required /><br />
                        @endif
                        <input type="submit" value="Checkout" class="btn btn--primary" />
                    </form>
                @else
                    <div style="text-align: center;">
                        <h2 style="margin-bottom:2rem;">The boosting checkout has expired, please resubmit your boost request</h2>
                        <a href="{{ route('boost') }}" class="btn btn--primary">Back to boosting page</a>
                    </div>
                @endif
            </section>
        </div>
    </section>
</x-layout>