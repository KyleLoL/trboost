<section class="section section--dark">
    <div class="section__wrapper">
        <section class="aboutstats flex">
            <div class="aboutstats__widget flex">
                <img class="aboutstats__widget__image" src="img/counter_img_01.png" />
                <div class="aboutstats__widget__text">
                    <p class="aboutstats__counter">178</p>
                    <p class="aboutstats__info">LoL Boosts Sold</p>
                </div>
            </div>

            <div class="aboutstats__widget flex">
                <img class="aboutstats__widget__image" src="img/counter_img_02.png" />
                <div class="aboutstats__widget__text">
                    <p class="aboutstats__counter">223</p>
                    <p class="aboutstats__info">Positive Feedbacks</p>
                </div>
            </div>

            <div class="aboutstats__widget flex">
                <img class="aboutstats__widget__image" src="img/counter_img_03.png" />
                <div class="aboutstats__widget__text">
                    <p class="aboutstats__counter">428</p>
                    <p class="aboutstats__info">Happy Customers</p>
                </div>
            </div>

            <div class="aboutstats__widget flex">
                <img class="aboutstats__widget__image" src="img/counter_img_04.png" />
                <div class="aboutstats__widget__text">
                    <p class="aboutstats__counter">27</p>
                    <p class="aboutstats__info">Professional Boosters</p>
                </div>
            </div>

        </section>
    </div>
</section>