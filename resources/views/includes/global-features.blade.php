<section class="section">
    <div class="section__wrapper">
        <header class="section__header flex" style="background-color:#ffffff;">
            <h2 class="section__header__title" style="margin-left:50px;">Quality <span>Services</span></h2>
            <p class="section__header__sub">Below you can read about our Services and Products!</p>
        </header>
        <section class="globalfeatures flex">
            <div class="globalfeatures__widget">
                <img class="globalfeatures__widget__image" src="https://lolboost.novundev.com/img/feature-img/icon-01.png" />
                <div class="globalfeatures__widget__text">
                    <h4><span>24/7/365</span> Support</h4>
                    <p>You can ask us anything via our live chat anytime! Our live-chat speacilists work 7/24 to provide you with the best service possible ^__^!</p>
                </div>
            </div>

            <div class="globalfeatures__widget">
                <img class="globalfeatures__widget__image" src="https://lolboost.novundev.com/img/feature-img/icon-02.png" />
                <div class="globalfeatures__widget__text">
                    <h4><span>Order</span> Insurance</h4>
                    <p>Your order is at safe hands! We create policies to protect both your and our rights, for more information, please check out our Terms and Conditions!</p>
                </div>
            </div>

            <div class="globalfeatures__widget">
                <img class="globalfeatures__widget__image" src="https://lolboost.novundev.com/img/feature-img/icon-03.png" />
                <div class="globalfeatures__widget__text">
                    <h4><span>99.9%</span> Uptime</h4>
                    <p>We usually process your order within the next 10 minutes from the moment you order it! We care about your time and we will provide you with your desired service the fastest way possible!</p>
                </div>
            </div>
        </section>
    </div>
</section>
