<section class="section section--alt" style="background-color:#ffffff;">
    <div class="section__wrapper">
        <header class="section__header flex" style="background-color:#f8f8f8; margin-top:20px; padding-left: 50px;">
            <h2 class="section__header__title">Client <span>Feedback</span></h2>
            <p class="section__header__sub">Let’s be honest, a lot of websites have testimonials that don’t seem to be real.
                You can check out our client feedback threads on third party sites to ensure that your account is in good hands. Click here to see one of our threads!</p>
        </header>
        <section class="feedback" >
            <ul class="feedback__list inline-flex" id="feedback__list">
                <li class="feedback__list__item" data-index="1"><img src="https://lolboost.novundev.com/img/feedback-img/icon-01.png" /></li>
                <li class="feedback__list__item" data-index="2"><img src="https://lolboost.novundev.com/img/feedback-img/icon-02.png" /></li>
                <li class="feedback__list__item" data-index="3"><img src="https://lolboost.novundev.com/img/feedback-img/icon-03.png" /></li>
                <li class="feedback__list__item" data-index="4"><img src="https://lolboost.novundev.com/img/feedback-img/icon-04.png" /></li>
            </ul>
            <div class="feedback__box" id="feedback__box">
                <div class="feedback__box__arrow" id="feedback__box__arrow"></div>
                <div class="feedback__box__text" id="feedback__box__text">"Boosted my account from Bronze 3 to Silver 5, did a great job!" - Find vouch on Forum!</div>
            </div>
        </section>
    </div>
</section>
