<x-app-layout>
    @push('styles')
        <style>
            .tableHiddenRow, .tableHiddenRow + tr {
                display: none;
            }
        </style>
    @endpush
    <x-slot name="header">
            Orders
    </x-slot>


    @if (session('status'))
        <div class="row">
            <div class="col-xl-12 col-md-12 mb-4">
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            </div>
        </div>
    @endif
    <div class="row">

        <!-- Earnings (Monthly) Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <a href="{{ route('admin.orders.index') }}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Open Orders</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $openOrderCount }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
                </a>
            </div>
        </div>

        <!-- Earnings (Annual) Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <a href="{{ route('admin.boosts.index') }}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                Orders Processesing</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $boostsCount }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
                </a>
            </div>
        </div>

        <!-- Pending Requests Card Example -->
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <a href="{{ route('admin.orders.finished') }}">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">
                                Finished Orders</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $finishedBoostsCount }}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
                </a>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- DataTales Example -->
        <div class="card shadow col-xl-10 col-md-6 mb-4 px-0 mx-3">
            <div class="card-header py-3 d-flex justify-content-between align-items-baseline">
                <h6 class="m-0 font-weight-bold text-primary">Open Orders</h6>
                <a href="{{ route('admin.orders.create') }}" class="btn btn-primary">Create Order</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Booster</th>
                            <th>Date</th>
                            <th>Queue</th>
                            <th>Boost</th>
                            <th>Email</th>
                            <th>Summoner Name</th>
                            <th>Price</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Booster</th>
                            <th>Date</th>
                            <th>Queue</th>
                            <th>Boost</th>
                            <th>Email</th>
                            <th>Summoner Name</th>
                            <th>Price</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($orders as $order)
                        <tr class="tableVisibleRow">
                            <td>#{{ $order->id }}</td>
                            <td>{{ $order->boosts()->where('status', 2)->first()->user->name }}</td>
                            <td>{{ $order->created_at->format('d/m/y') }}</td>
                            <td>
                                @switch($order->queue_type)
                                    @case(1)
                                    Solo
                                    @break
                                    @case(2)
                                    Duo
                                    @break
                                    @case(3)
                                    Solo (flex)
                                    @break
                                    @case(4)
                                    Duo (flex)
                                    @break
                                @endswitch
                            </td>
                            @if($order->current_rank && $order->desired_rank)
                                <td>
                                    {{ $order->currentRank->rankTier->name }}
                                    {{ $order->currentRank->rankDivision->number }}
                                    ->
                                    {{ $order->desiredRank->rankTier->name }}
                                    {{ $order->desiredRank->rankDivision->number }}
                                </td>
                            @elseif($order->boost_type == 2)
                                <td>{{ $order->wins }} Placements ({{ $order->currentRank->rankTier->name }} {{ $order->currentRank->rankDivision->number }})</td>
                            @else
                                <td>{{ $order->wins }} Net Wins ({{ $order->currentRank->rankTier->name }} {{ $order->currentRank->rankDivision->number }})</td>
                            @endif
                            <td>{{ $order->email }}</td>
                            <td>{{ $order->summoner_name }}</td>
                            <td>${{ $order->price }}</td>
                            <td>
                                <a href="{{ route('admin.orders.delete', ['order' => $order->id]) }}" class="btn btn-outline-danger btn-sm mr-2">Delete</a>
                                -
                                <a href="#" class="btn btn-outline-info btn-sm dropdown-toggle ml-2 orderExtras">Extras</a>
                            </td>
                        </tr>
                        <tr class="tableHiddenRow" style="background-color: #f5f5f5;">
                            <th colspan="4">
                                Login:
                            </th>
                            <th colspan="2">
                                Roles:
                            </th>
                            <th colspan="1">
                                Spells:
                            </th>
                            <th colspan="2">
                                Addons:
                            </th>
                        </tr>
                        <tr>
                            <td colspan="4">
                                User: {{ $order->username }}<br />
                                Pass: {{ $order->password }}
                            </td>
                            <td colspan="2">
                                @if($order->orderExtra->role1)
                                    {{ $order->orderExtra->role1 }} | {{ $order->orderExtra->role2 }}
                                @endif
                            </td>
                            <td colspan="1">
                                @if($order->orderExtra->spell1)
                                    [D] {{ $order->orderExtra->spell1 }} | [F] {{ $order->orderExtra->spell2 }}
                                @endif
                            </td>
                            <td colspan="2">
                                <ul>
                                    @if($order->orderExtra->premium)<li>Premium</li>@endif
                                    @if($order->orderExtra->streaming)<li>Streaming</li>@endif
                                    @if($order->orderExtra->offline)<li>Offline Mode</li>@endif
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

    <script>
        (function () {
            let orderExtraButtons = document.querySelectorAll('.orderExtras');
            orderExtraButtons.forEach(function(element) {
                element.addEventListener('click', function(e) {
                    e.preventDefault();
                    let hiddenRow = element.parentElement.parentElement.nextElementSibling;

                    if(!hiddenRow.classList.contains('tableHiddenRow')) {
                        hiddenRow.classList.add('tableHiddenRow');
                    }else {
                        hiddenRow.classList.remove('tableHiddenRow');
                    }
                });
            });
        })();
    </script>

</x-app-layout>
