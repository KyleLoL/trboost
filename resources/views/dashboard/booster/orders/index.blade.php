<x-app-layout>
    @push('styles')
        <style>
            .tableHiddenRow, .tableHiddenRow + tr {
                display: none;
            }
        </style>
    @endpush
    <x-slot name="header">
        Gösterge Paneli - Booster - Siparişler Dizini
    </x-slot>

    @if (session('status'))
        <div class="row">
            <div class="col-xl-12 col-md-12 mb-4">
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            </div>
        </div>
    @endif
    <div class="row">
    <!-- DataTales Example -->
    <div class="card shadow col-xl-10 col-md-6 mb-4 px-0 mx-3">
        <div class="card-header py-3 d-flex justify-content-between align-items-baseline">
            <h6 class="m-0 font-weight-bold text-primary">Bekleyen Siparişler</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Tarih</th>
                        <th>Dereceli Sıra Türü</th>
                        <th>Boost Türü</th>
                        <th>E-posta</th>
                        <th>Sihirdar Adı</th>
                        <th>Fiyat</th>
                        <th>Sunucu</th>
                        <th>İşlemler</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Tarih</th>
                        <th>Dereceli Sıra Türü</th>
                        <th>Boost Türü</th>
                        <th>E-posta</th>
                        <th>Sihirdar Adı</th>
                        <th>Fiyat</th>
                        <th>Sunucu</th>
                        <th>İşlemler</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($orders as $order)
                        <tr class="tableVisibleRow">
                            <td>#{{ $order->id }}</td>
                            <td>{{ $order->created_at->format('d/m/y') }}</td>
                            <td>
                                @switch($order->queue_type)
                                    @case(1)
                                    Solo
                                    @break
                                    @case(2)
                                    Duo
                                    @break
                                    @case(3)
                                    Solo (flex)
                                    @break
                                    @case(4)
                                    Duo (flex)
                                    @break
                                @endswitch
                            </td>
                            @if($order->current_rank && $order->desired_rank)
                                <td>
                                    {{ $order->currentRank->rankTier->name }}
                                    {{ $order->currentRank->rankDivision->number }}
                                    ->
                                    {{ $order->desiredRank->rankTier->name }}
                                    {{ $order->desiredRank->rankDivision->number }}
                                </td>
                            @elseif($order->boost_type == 2)
                                <td>{{ $order->wins }} Placements ({{ (isset($order->currentRank)) ? $order->currentRank->rankTier->name : "Unranked" }})</td>
                            @else
                                <td>{{ $order->wins }} Net Wins ({{ $order->currentRank->rankTier->name }} {{ $order->currentRank->rankDivision->number }})</td>
                            @endif
                            <td>{{ $order->email }}</td>
                            <td>{{ $order->summoner_name }}</td>
                            <td>${{ $order->booster_price }}</td>
                            <td>{{ $order->server }}</td>
                            <td>
                                @if($boostCount < 2)
                                    <form method="post" action="{{ route('booster.orders.store') }}" style="display:inline;">
                                        @csrf
                                        <input type="hidden" name="order_id" value="{{ $order->id }}" />
                                        <input type="submit" class="btn btn-outline-success btn-sm mr-2" value="Pickup Order" />
                                    </form>
                                @endif
                                @if($order->orderExtra()->exists())
                                -
                                    <a href="#" class="btn btn-outline-info btn-sm dropdown-toggle ml-2 orderExtras">Ekstralar</a>
                                @endif
                            </td>
                        </tr>
                        @if($order->orderExtra()->exists())
                        <tr class="tableHiddenRow" style="background-color: #f5f5f5;">
                            <th colspan="3">
                                Roller:
                            </th>
                            <th colspan="3">
                                Sihirdar Büyüleri:
                            </th>
                            <th colspan="3">
                                Şampiyonlar:
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                @if($order->orderExtra->role1)
                                    {{ $order->orderExtra->role1 }} | {{ $order->orderExtra->role2 }}
                                @endif
                            </td>
                            <td colspan="3">
                                @if($order->orderExtra->spell1)
                                    [D] {{ $order->orderExtra->spell1 }} | [F] {{ $order->orderExtra->spell2 }}
                                @endif
                            </td>
                            <td colspan="3">
                                <ul>
                                    @foreach(explode(',',$order->orderExtra->champions) as $champ)
                                    @if($champ != '')<li>{{ $champ }}</li>@endif
                                    @endforeach
                                </ul>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>

        <script>
            (function () {
                let orderExtraButtons = document.querySelectorAll('.orderExtras');
                orderExtraButtons.forEach(function(element) {
                    element.addEventListener('click', function(e) {
                        e.preventDefault();
                        let hiddenRow = element.parentElement.parentElement.nextElementSibling;

                        if(!hiddenRow.classList.contains('tableHiddenRow')) {
                            hiddenRow.classList.add('tableHiddenRow');
                        }else {
                            hiddenRow.classList.remove('tableHiddenRow');
                        }
                    });
                });
            })();
        </script>


</x-app-layout>
