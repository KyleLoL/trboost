<x-app-layout>
    @push('styles')
        <style>
            .tableHiddenRow, .tableHiddenRow + tr {
                display: none;
            }
        </style>
    @endpush
    <x-slot name="header">
        Gösterge Paneli - Booster - Siparişler Dizini
    </x-slot>
    <div class="row">
    <!-- DataTales Example -->
    <div class="card shadow col-xl-10 col-md-6 mb-4 px-0 mx-3">
        <div class="card-header py-3 d-flex justify-content-between align-items-baseline">
            <h6 class="m-0 font-weight-bold text-primary">Tamamlanmış Siparişler</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Tarih</th>
                        <th>Dereceli Sıra</th>
                        <th>Boost Türü</th>
                        <th>E-posta</th>
                        <th>Sihirdar Adı</th>
                        <th>Fiyat</th>
                        <th>Sunucu</th>
                        <th>İşlemler</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Tarih</th>
                        <th>Dereceli Sıra</th>
                        <th>Boost Türü</th>
                        <th>E-posta</th>
                        <th>Sihirdar Adı</th>
                        <th>Fiyat</th>
                        <th>Sunucu</th>
                        <th>İşlemler</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($orders as $boost)
                        <tr class="tableVisibleRow">
                            <td>#{{ $boost->order->id }}</td>
                            <td>{{ $boost->order->created_at->format('d/m/y') }}</td>
                            <td>
                                @switch($boost->order->queue_type)
                                    @case(1)
                                    Solo
                                    @break
                                    @case(2)
                                    Duo
                                    @break
                                    @case(3)
                                    Solo (flex)
                                    @break
                                    @case(4)
                                    Duo (flex)
                                    @break
                                @endswitch
                            </td>
                            @if($boost->order->current_rank && $boost->order->desired_rank)
                                <td>
                                    {{ $boost->order->currentRank->rankTier->name }}
                                    {{ $boost->order->currentRank->rankDivision->number }}
                                    ->
                                    {{ $boost->order->desiredRank->rankTier->name }}
                                    {{ $boost->order->desiredRank->rankDivision->number }}
                                </td>
                            @elseif($boost->order->boost_type == 2)
                                <td>{{ $boost->order->wins }} Placements ({{ (isset($boost->order->currentRank)) ? $boost->order->currentRank->rankTier->name : "Unranked" }})</td>
                            @else
                                <td>{{ $boost->order->wins }} Net Wins ({{ $boost->order->currentRank->rankTier->name }} {{ $boost->order->currentRank->rankDivision->number }})</td>
                            @endif
                            <td>{{ $boost->order->email }}</td>
                            <td>{{ $boost->order->summoner_name }}</td>
                            <td>${{ $boost->order->booster_price }}</td>
                            <td>${{ $boost->order->server }}</td>
                            <td>
                                @if($boost->order->orderExtra()->exists())
                                <a href="#" class="btn btn-outline-info btn-sm dropdown-toggle orderExtras">Ekstralar</a>
                                @endif
                            </td>
                        </tr>
                        @if($boost->order->orderExtra()->exists())
                        <tr class="tableHiddenRow" style="background-color: #f5f5f5;">
                            <th colspan="3">
                                Roller:
                            </th>
                            <th colspan="3">
                                Sihirdar Büyüleri:
                            </th>
                            <th colspan="3">
                                Şampiyonlar:
                            </th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                @if($boost->order->orderExtra->role1)
                                    {{ $boost->order->orderExtra->role1 }} | {{ $boost->order->orderExtra->role2 }}
                                @endif
                            </td>
                            <td colspan="3">
                                @if($boost->order->orderExtra->spell1)
                                    [D] {{ $boost->order->orderExtra->spell1 }} | [F] {{ $boost->order->orderExtra->spell2 }}
                                @endif
                            </td>
                            <td colspan="3">
                                <ul>
                                    @foreach(explode(',',$boost->order->orderExtra->champions) as $champ)
                                    @if($champ != '')<li>{{ $champ }}</li>@endif
                                    @endforeach
                                </ul>
                            </td>
                        </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>

        <script>
            (function () {
                let orderExtraButtons = document.querySelectorAll('.orderExtras');
                orderExtraButtons.forEach(function(element) {
                    element.addEventListener('click', function(e) {
                        e.preventDefault();
                        let hiddenRow = element.parentElement.parentElement.nextElementSibling;

                        if(!hiddenRow.classList.contains('tableHiddenRow')) {
                            hiddenRow.classList.add('tableHiddenRow');
                        }else {
                            hiddenRow.classList.remove('tableHiddenRow');
                        }
                    });
                });
            })();
        </script>

</x-app-layout>
