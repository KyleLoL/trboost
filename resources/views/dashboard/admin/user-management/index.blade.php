<x-app-layout>
    <x-slot name="header">
        Admin - User Management
    </x-slot>


    @if (session('status'))
        <div class="row">
            <div class="col-xl-12 col-md-12 mb-4">
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-3">
            <div style="width: 100%; border: 1px solid #ccc; border-radius: 15px; padding: 2rem;">
                <h3>Users</h3>
                <p class="mb-5">Features related to managing users.</p>

                <a href="{{ route('admin.user-roles.index') }}" class="btn btn-primary mb-2">User Roles</a><br />
                <a href="{{ route('admin.financial.index') }}" class="btn btn-primary mb-2">Financials</a><br />
            </div>
        </div>

        <div class="col-3">
            <div style="width: 100%; border: 1px solid #ccc; border-radius: 15px; padding: 2rem;">
                <h3>Users</h3>
                <p class="mb-5">Features related to managing users.</p>

                <a href="#" class="btn btn-primary mb-2">User Roles</a><br />
                <a href="#" class="btn btn-primary mb-2">User Roles</a><br />
            </div>
        </div>

        <div class="col-3">
            <div style="width: 100%; border: 1px solid #ccc; border-radius: 15px; padding: 2rem;">
                <h3>Users</h3>
                <p class="mb-5">Features related to managing users.</p>

                <a href="#" class="btn btn-primary mb-2">User Roles</a><br />
                <a href="#" class="btn btn-primary mb-2">User Roles</a><br />
            </div>
        </div>
    </div>

</x-app-layout>
