<x-app-layout>
    <x-slot name="header">
            Edit Order
    </x-slot>


    <div class="row">
        <div class="col-6">
            <form method="post" action="{{ route('admin.orders.update', ['order' => $order->id]) }}">
                <div class="form-group">
                    <label for="price">Email:</label>
                    <input type="text" class="form-control" value="{{ $order->email }}" disabled>
                </div>
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="queueType">Queue Type:</label>
                    <select name="queue" id="queueType" class="form-control">
                        <option value="1" {{ $order->queue_type == 1 ? "selected" : ""}}>Solo</option>
                        <option value="2" {{ $order->queue_type == 2 ? "selected" : ""}}>Duo</option>
                        <option value="3" {{ $order->queue_type == 3 ? "selected" : ""}}>Solo (flex)</option>
                        <option value="4" {{ $order->queue_type == 4 ? "selected" : ""}}>Duo (flex)</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="boostType">Boost Type:</label>
                    <select name="boost" id="boostType" class="form-control">
                        <option value="0">Select a Type:</option>
                        <option value="1" {{ $order->boost_type == 1 ? "selected" : ""}}>Division</option>
                        <option value="2" {{ $order->boost_type == 2 ? "selected" : ""}}>Placements</option>
                        <option value="3" {{ $order->boost_type == 3 ? "selected" : ""}}>Net Wins</option>
                    </select>
                </div>

                <div class="form-group row" id="currentRankContainer">
                    <div class="col">
                        <label for="currentTier">Current Tier:</label>
                        <select id="currentTier" name="currentTier" class="form-control">
                            @foreach($rankTiers as $key => $val)
                                <option value="{{ $val->id }}" {{ $order->currentRank->rankTier->id == $val->id ? "selected" : ""}}>{{ $val->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col">
                        <label for="currentDivision">Current Division</label>
                        <select id="currentDivision" name="currentDivision" class="form-control">
                            @foreach($rankDivisions as $key => $val)
                                <option value="{{ $val->id }}" {{ $order->currentRank->rankDivision->id == $val->id ? "selected" : ""}}>{{ $val->number }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @if($order->boost_type == 1)
                <div class="form-group row" id="desiredRankContainer">
                    <div class="col">
                        <label for="desiredTier">Desired Tier:</label>
                        <select id="desiredTier" name="desiredTier" class="form-control">
                            @foreach($rankTiers as $key => $val)
                                <option value="{{ $val->id }}" {{ $order->desiredRank->rankTier->id == $val->id ? "selected" : ""}}>{{ $val->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col">
                        <label for="desiredDivision">Desired Division</label>
                        <select id="desiredDivision" name="desiredDivision" class="form-control">
                            @foreach($rankDivisions as $key => $val)
                                <option value="{{ $val->id }}" {{ $order->desiredRank->rankDivision->id == $val->id ? "selected" : ""}}>{{ $val->number }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @else
                    <label for="amountWins">Amount of Wins:</label>
                    <input type="text" class="form-control" name="amountWins" id="amountWins" value="{{ $order->wins }}" />
                @endif
                <div class="form-group">
                    <label for="summonerName">Summoner Name:</label>
                    <input type="text" class="form-control" name="summonerName" id="summonerName" value="{{ $order->summoner_name }}">
                </div>
                <div class="form-group">
                    <label for="price">Price:</label>
                    <input type="text" class="form-control" name="price" id="price" value="{{ $order->price }}">
                </div>

                <div class="form-group">
                    <label for="username">LoL Username:</label>
                    <input type="text" class="form-control" name="username" id="username" value="{{ $order->username }}">
                </div>

                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="text" class="form-control" name="password" id="password" value="{{ $order->password }}">
                </div>

                <h4 class="my-4">Extras:</h4>

                <div class="form-group row">
                    <div class="col">
                        <label for="roles1">Role 1:</label>
                        <select name="roles1" id="roles1" class="form-control">
                            <option value="0">Select a Type:</option>
                            <option value="1" {{ $order->orderExtra->role1 == "Top" ? "selected" : ""}}>Top</option>
                            <option value="2" {{ $order->orderExtra->role1 == "Mid" ? "selected" : ""}}>Mid</option>
                            <option value="3" {{ $order->orderExtra->role1 == "Jungle" ? "selected" : ""}}>Jungle</option>
                            <option value="4" {{ $order->orderExtra->role1 == "AD Carry" ? "selected" : ""}}>AD Carry</option>
                            <option value="5" {{ $order->orderExtra->role1 == "Support" ? "selected" : ""}}>Support</option>
                        </select>
                    </div>
                    <div class="col">
                        <label for="roles2">Role 2:</label>
                        <select name="roles2" id="roles2" class="form-control">
                            <option value="0">Select a Type:</option>
                            <option value="1" {{ $order->orderExtra->role2 == "Top" ? "selected" : ""}}>Top</option>
                            <option value="2" {{ $order->orderExtra->role2 == "Mid" ? "selected" : ""}}>Mid</option>
                            <option value="3" {{ $order->orderExtra->role2 == "Jungle" ? "selected" : ""}}>Jungle</option>
                            <option value="4" {{ $order->orderExtra->role2 == "AD Carry" ? "selected" : ""}}>AD Carry</option>
                            <option value="5" {{ $order->orderExtra->role2 == "Support" ? "selected" : ""}}>Support</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col">
                        <label for="spell1">Spell 1:</label>
                        <select name="spell1" id="spell1" class="form-control">
                            <option value="0">Select a Type:</option>
                            <option value="1" {{ $order->orderExtra->spell1 == "Barrier" ? "selected" : ""}}>Barrier</option>
                            <option value="2" {{ $order->orderExtra->spell1 == "Cleanse" ? "selected" : ""}}>Cleanse</option>
                            <option value="3" {{ $order->orderExtra->spell1 == "Exhaust" ? "selected" : ""}}>Exhaust</option>
                            <option value="4" {{ $order->orderExtra->spell1 == "Flash" ? "selected" : ""}}>Flash</option>
                            <option value="5" {{ $order->orderExtra->spell1 == "Ghost" ? "selected" : ""}}>Ghost</option>
                            <option value="6" {{ $order->orderExtra->spell1 == "Heal" ? "selected" : ""}}>Heal</option>
                            <option value="7" {{ $order->orderExtra->spell1 == "Ignite" ? "selected" : ""}}>Ignite</option>
                            <option value="8" {{ $order->orderExtra->spell1 == "Smite" ? "selected" : ""}}>Smite</option>
                            <option value="9" {{ $order->orderExtra->spell1 == "Teleport" ? "selected" : ""}}>Teleport</option>
                        </select>
                    </div>
                    <div class="col">
                        <label for="spell2">Spell 2:</label>
                        <select name="spell2" id="spell2" class="form-control">
                            <option value="0">Select a Type:</option>
                            <option value="1" {{ $order->orderExtra->spell2 == "Barrier" ? "selected" : ""}}>Barrier</option>
                            <option value="2" {{ $order->orderExtra->spell2 == "Cleanse" ? "selected" : ""}}>Cleanse</option>
                            <option value="3" {{ $order->orderExtra->spell2 == "Exhaust" ? "selected" : ""}}>Exhaust</option>
                            <option value="4" {{ $order->orderExtra->spell2 == "Flash" ? "selected" : ""}}>Flash</option>
                            <option value="5" {{ $order->orderExtra->spell2 == "Ghost" ? "selected" : ""}}>Ghost</option>
                            <option value="6" {{ $order->orderExtra->spell2 == "Heal" ? "selected" : ""}}>Heal</option>
                            <option value="7" {{ $order->orderExtra->spell2 == "Ignite" ? "selected" : ""}}>Ignite</option>
                            <option value="8" {{ $order->orderExtra->spell2 == "Smite" ? "selected" : ""}}>Smite</option>
                            <option value="9" {{ $order->orderExtra->spell2 == "Teleport" ? "selected" : ""}}>Teleport</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-2">
                        <label for="premium">Premium:</label>
                        <input type="checkbox" name="premium" id="premium" value="1" {{ $order->orderExtra->premium ? "checked" : ""}}/>
                    </div>

                    <div class="col-2">
                        <label for="streaming">Streaming:</label>
                        <input type="checkbox" name="streaming" id="streaming" value="1" {{ $order->orderExtra->streaming ? "checked" : ""}}/>
                    </div>

                    <div class="col-2">
                        <label for="offline">Offline:</label>
                        <input type="checkbox" name="offline" id="offline" value="1" {{ $order->orderExtra->offline ? "checked" : ""}}/>
                    </div>
                </div>


                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

    <script>
        const boostSelect = document.querySelector('#boostType');
        boostSelect.addEventListener('change', function(e) {
            const parentGroup = boostSelect.parentNode;
            if(e.target.value == 0) {
                document.querySelector('#currentRankContainer').remove();
                document.querySelector('#desiredRankContainer').remove();
                document.querySelector('#amountWinsContainer').remove();
            }
            else if(e.target.value == 1 && !document.querySelector('#ranksContainer')) {
                if(document.querySelector('#currentRankContainer')) {
                    document.querySelector('#currentRankContainer').remove();
                }
                if(document.querySelector('#amountWinsContainer')) {
                    document.querySelector('#amountWinsContainer').remove();
                }
                //add current/desired selects
                let newElement = document.createElement('div');
                newElement.classList.add('form-group');
                newElement.classList.add('row');
                newElement.setAttribute('id', 'currentRankContainer');
                newElement.innerHTML = '' +
                    '<div class="col">' +
                        '<label for="currentTier">Current Tier:</label>' +
                        '<select id="currentTier" name="currentTier" class="form-control">' +
                            @foreach($rankTiers as $key => $val)
                                '<option value="{{ $val->id }}">{{ $val->name }}</option>' +
                            @endforeach
                        '</select>' +
                        '</div>' +
                    '<div class="col">' +
                        '<label for="currentDivision">Current Division</label>' +
                        '<select id="currentDivision" name="currentDivision" class="form-control">' +
                            @foreach($rankDivisions as $key => $val)
                                '<option value="{{ $val->id }}">{{ $val->number }}</option>' +
                            @endforeach
                        '</select>' +
                    '</div>';
                boostSelect.parentNode.after(newElement);

                let newElement2 = document.createElement('div');
                newElement2.classList.add('form-group');
                newElement2.classList.add('row');
                newElement2.setAttribute('id', 'desiredRankContainer');
                newElement2.innerHTML = '' +
                    '<div class="col">' +
                    '<label for="desiredTier">Desired Tier:</label>' +
                    '<select id="desiredTier" name="desiredTier" class="form-control">' +
                        @foreach($rankTiers as $key => $val)
                            '<option value="{{ $val->id }}">{{ $val->name }}</option>' +
                        @endforeach
                    '</select>' +
                    '</div>' +
                    '<div class="col">' +
                    '<label for="desiredDivision">Desired Division</label>' +
                    '<select id="desiredDivision" name="desiredDivision" class="form-control">' +
                        @foreach($rankDivisions as $key => $val)
                            '<option value="{{ $val->id }}">{{ $val->number }}</option>' +
                        @endforeach
                    '</select>' +
                    '</div>';
                newElement.after(newElement2);
            }else if(!document.querySelector('#amountWins')){
                if(document.querySelector('#currentRankContainer')) {
                    document.querySelector('#currentRankContainer').remove();
                }
                if(document.querySelector('#desiredRankContainer')) {
                    document.querySelector('#desiredRankContainer').remove();
                }

                //add amount wins textfield
                let newElement = document.createElement('div');
                newElement.classList.add('form-group');
                newElement.setAttribute('id', 'amountWinsContainer');
                newElement.innerHTML = '<label for="amountWins">Amount of Wins:</label><input type="text" class="form-control" name="amountWins" id="amountWins" placeholder="5">';
                boostSelect.parentNode.after(newElement);

                let newElement2 = document.createElement('div');
                newElement2.classList.add('form-group');
                newElement2.classList.add('row');
                newElement2.setAttribute('id', 'currentRankContainer');
                newElement2.innerHTML = '' +
                    '<div class="col">' +
                    '<label for="currentTier">Current Tier:</label>' +
                    '<select id="currentTier" name="currentTier" class="form-control">' +
                    @foreach($rankTiers as $key => $val)
                        '<option value="{{ $val->id }}">{{ $val->name }}</option>' +
                    @endforeach
                        '</select>' +
                    '</div>' +
                    '<div class="col">' +
                    '<label for="currentDivision">Current Division</label>' +
                    '<select id="currentDivision" name="currentDivision" class="form-control">' +
                    @foreach($rankDivisions as $key => $val)
                        '<option value="{{ $val->id }}">{{ $val->number }}</option>' +
                    @endforeach
                        '</select>' +
                    '</div>';
                boostSelect.parentNode.after(newElement2);
            }
        });
    </script>

</x-app-layout>
