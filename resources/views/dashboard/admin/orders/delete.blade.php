<x-app-layout>
    @push('styles')
        <style>
            .tableHiddenRow, .tableHiddenRow + tr {
                display: none;
            }
        </style>
    @endpush
    <x-slot name="header">
            Delete Order
    </x-slot>


    <div class="row">
        <!-- DataTales Example -->
        <div class="card shadow col-xl-10 col-md-6 mb-4 px-0 mx-3">
            <div class="card-header py-3 d-flex justify-content-between align-items-baseline">
                <h6 class="m-0 font-weight-bold text-primary">Delete Order</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Queue</th>
                            <th>Boost</th>
                            <th>Email</th>
                            <th>Summoner Name</th>
                            <th>Price</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr class="tableVisibleRow">
                                <td>#{{ $order->id }}</td>
                                <td>{{ $order->created_at->format('d/m/y') }}</td>
                                <td>
                                    @switch($order->queue_type)
                                        @case(1)
                                        Solo
                                        @break
                                        @case(2)
                                        Duo
                                        @break
                                        @case(3)
                                        Solo (flex)
                                        @break
                                        @case(4)
                                        Duo (flex)
                                        @break
                                    @endswitch
                                </td>
                                @if($order->current_rank && $order->desired_rank)
                                    <td>
                                        {{ $order->currentRank->rankTier->name }}
                                        {{ $order->currentRank->rankDivision->number }}
                                        ->
                                        {{ $order->desiredRank->rankTier->name }}
                                        {{ $order->desiredRank->rankDivision->number }}
                                    </td>
                                @elseif($order->boost_type == 2)
                                    <td>{{ $order->wins }} Placements ({{ $order->currentRank->rankTier->name }} {{ $order->currentRank->rankDivision->number }})</td>
                                @else
                                    <td>{{ $order->wins }} Net Wins ({{ $order->currentRank->rankTier->name }} {{ $order->currentRank->rankDivision->number }})</td>
                                @endif
                                <td>{{ $order->summoner_name }}</td>
                                <td>${{ $order->price }}</td>
                                <td>N/A</td>
                                <td>
                                    <a href="#" class="btn btn-outline-info btn-sm dropdown-toggle orderExtras">Extras</a>
                                </td>
                            </tr>
                            <tr class="tableHiddenRow" style="background-color: #f5f5f5;">
                                <th colspan="3">
                                    Roles:
                                </th>
                                <th colspan="3">
                                    Spells:
                                </th>
                                <th colspan="3">
                                    Addons:
                                </th>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    @if($order->orderExtra->role1)
                                        {{ $order->orderExtra->role1 }} | {{ $order->orderExtra->role2 }}
                                    @endif
                                </td>
                                <td colspan="3">
                                    @if($order->orderExtra->spell1)
                                        [D] {{ $order->orderExtra->spell1 }} | [F] {{ $order->orderExtra->spell2 }}
                                    @endif
                                </td>
                                <td colspan="3">
                                    <ul>
                                        @if($order->orderExtra->premium)<li>Premium</li>@endif
                                        @if($order->orderExtra->streaming)<li>Streaming</li>@endif
                                        @if($order->orderExtra->offline)<li>Offline Mode</li>@endif
                                    </ul>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-6">
            <form method="post" action="{{ route('admin.orders.destroy', ['order' => $order->id]) }}">
                @csrf
                @method('DELETE')

                <p>Are you sure you wish to delete this order?</p>


                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </div>
    </div>

    <script>
        (function () {
            let orderExtraButtons = document.querySelectorAll('.orderExtras');
            orderExtraButtons.forEach(function(element) {
                element.addEventListener('click', function(e) {
                    e.preventDefault();
                    let hiddenRow = element.parentElement.parentElement.nextElementSibling;

                    if(!hiddenRow.classList.contains('tableHiddenRow')) {
                        hiddenRow.classList.add('tableHiddenRow');
                    }else {
                        hiddenRow.classList.remove('tableHiddenRow');
                    }
                });
            });
        })();
    </script>


</x-app-layout>
