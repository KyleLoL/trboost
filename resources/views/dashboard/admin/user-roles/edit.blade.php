<x-app-layout>
    <x-slot name="header">
        Admin - User Roles - Edit
    </x-slot>


    @if (session('status'))
        <div class="row">
            <div class="col-xl-12 col-md-12 mb-4">
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-6">
            <form method="post" action="{{ route('admin.user-roles.update', ['user' => $user->id]) }}">
                @csrf
                @method('PATCH')
                <h5 class="mb-5">Editting <b>{{ $user->name }}</b></h5>

                <div class="form-group">
                    <label for="role">Role:</label>
                    <select name="role" id="role" class="form-control">
                        <option value="0" {{ $user->role_id == 0 ? "selected" : "" }}>Normal</option>
                        @foreach($roles as $role)
                            <option value="{{ $role->id }}" {{ $user->role_id == $role->id ? "selected" : "" }}>
                                {{ $role->name }}
                            </option>
                        @endforeach
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Update Role</button>
            </form>
        </div>
    </div>

</x-app-layout>
