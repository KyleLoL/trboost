<x-app-layout>
    @push('styles')
        <style>
            .tableHiddenRow, .tableHiddenRow + tr {
                display: none;
            }
        </style>
    @endpush
    <x-slot name="header">
            Boosts
    </x-slot>


    @if (session('status'))
        <div class="row">
            <div class="col-xl-12 col-md-12 mb-4">
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <!-- DataTales Example -->
        <div class="card shadow col-xl-10 col-md-6 mb-4 px-0 mx-3">
            <div class="card-header py-3 d-flex justify-content-between align-items-baseline">
                <h6 class="m-0 font-weight-bold text-primary">Ongoing Boosts</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Queue</th>
                            <th>Boost</th>
                            <th>Booster</th>
                            <th>Email</th>
                            <th>Summoner Name</th>
                            <th>Price</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Date</th>
                            <th>Queue</th>
                            <th>Boost</th>
                            <th>Booster</th>
                            <th>Email</th>
                            <th>Summoner Name</th>
                            <th>Price</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($boosts as $boost)
                            <tr class="tableVisibleRow">
                                <td>#{{ $boost->order->id }}</td>
                                <td>{{ $boost->order->created_at->format('d/m/y') }}</td>
                                <td>
                                    @switch($boost->order->queue_type)
                                        @case(1)
                                        Solo
                                        @break
                                        @case(2)
                                        Duo
                                        @break
                                        @case(3)
                                        Solo (flex)
                                        @break
                                        @case(4)
                                        Duo (flex)
                                        @break
                                    @endswitch
                                </td>
                                @if($boost->order->current_rank && $boost->order->desired_rank)
                                    <td>
                                        {{ $boost->order->currentRank->rankTier->name }}
                                        {{ $boost->order->currentRank->rankDivision->number }}
                                        ->
                                        {{ $boost->order->desiredRank->rankTier->name }}
                                        {{ $boost->order->desiredRank->rankDivision->number }}
                                    </td>
                                @elseif($boost->order->boost_type == 2)
                                    <td>{{ $boost->order->wins }} Placements ({{ $boost->order->currentRank->rankTier->name }} {{ $boost->order->currentRank->rankDivision->number }})</td>
                                @else
                                    <td>{{ $boost->order->wins }} Net Wins ({{ $boost->order->currentRank->rankTier->name }} {{ $boost->order->currentRank->rankDivision->number }})</td>
                                @endif
                                <td>{{ $boost->user->name }}</td>
                                <td>{{ $boost->order->email }}</td>
                                <td>{{ $boost->order->summoner_name }}</td>
                                <td>${{ $boost->order->price }}</td>
                                <td>
                                    <form method="post" action="{{ route('admin.boosts.takeaway', ['boost' => $boost->id]) }}" style="display:inline;">
                                        @csrf
                                        @method('PATCH')
                                        <input type="submit" class="btn btn-outline-danger btn-sm mr-2" value="Takeaway Order" />
                                    </form>
                                    -
                                    <a href="#" class="btn btn-outline-info btn-sm dropdown-toggle ml-2 orderExtras">Extras</a>
                                </td>
                            </tr>

                            <tr class="tableHiddenRow" style="background-color: #f5f5f5;">
                                <th colspan="3">
                                    Roles:
                                </th>
                                <th colspan="3">
                                    Spells:
                                </th>
                                <th colspan="3">
                                    Addons:
                                </th>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    @if($boost->order->orderExtra->role1)
                                        {{ $boost->order->orderExtra->role1 }} | {{ $boost->order->orderExtra->role2 }}
                                    @endif
                                </td>
                                <td colspan="3">
                                    @if($boost->order->orderExtra->spell1)
                                        [D] {{ $boost->order->orderExtra->spell1 }} | [F] {{ $boost->order->orderExtra->spell2 }}
                                    @endif
                                </td>
                                <td colspan="3">
                                    <ul>
                                        @if($boost->order->orderExtra->premium)<li>Premium</li>@endif
                                        @if($boost->order->orderExtra->streaming)<li>Streaming</li>@endif
                                        @if($boost->order->orderExtra->offline)<li>Offline Mode</li>@endif
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

        <script>
            (function () {
                let orderExtraButtons = document.querySelectorAll('.orderExtras');
                orderExtraButtons.forEach(function(element) {
                    element.addEventListener('click', function(e) {
                        e.preventDefault();
                        let hiddenRow = element.parentElement.parentElement.nextElementSibling;

                        if(!hiddenRow.classList.contains('tableHiddenRow')) {
                            hiddenRow.classList.add('tableHiddenRow');
                        }else {
                            hiddenRow.classList.remove('tableHiddenRow');
                        }
                    });
                });
            })();
        </script>

</x-app-layout>
