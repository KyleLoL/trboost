<x-app-layout>
    <x-slot name="header">
        Admin - Financials
    </x-slot>


    @if (session('status'))
        <div class="row">
            <div class="col-xl-12 col-md-12 mb-4">
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <!-- DataTales Example -->
        <div class="card shadow col-xl-10 col-md-6 mb-4 px-0 mx-3">
            <div class="card-header py-3 d-flex justify-content-between align-items-baseline">
                <h6 class="m-0 font-weight-bold text-primary">Boosters</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Pay</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Pay</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($boosters as $user)
                            <tr>
                                <td>#{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    ${{ $user->pay }}
                                </td>
                                <td>
                                    <a href="{{ route('admin.financial.edit', ['user' => $user->id]) }}" class="btn btn-outline-warning btn-sm">Edit Pay</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

</x-app-layout>
