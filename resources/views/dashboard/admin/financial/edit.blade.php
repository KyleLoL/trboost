<x-app-layout>
    <x-slot name="header">
        Admin - Financials - Edit
    </x-slot>


    @if (session('status'))
        <div class="row">
            <div class="col-xl-12 col-md-12 mb-4">
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <div class="col-6">
            <form method="post" action="{{ route('admin.financial.update', ['user' => $user->id]) }}">
                @csrf
                @method('PATCH')
                <h5 class="mb-5">Editting <b>{{ $user->name }}</b></h5>

                <div class="form-group">
                    <label for="role">Pay:</label>
                    <input type="text" name="pay" id="pay" value="{{ $user->pay }}" />
                </div>

                <button type="submit" class="btn btn-primary">Update Pay</button>
            </form>
        </div>
    </div>

</x-app-layout>
