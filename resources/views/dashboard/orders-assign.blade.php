<x-app-layout>
    <x-slot name="header">
            Orders > Assign
    </x-slot>


    @if (session('status'))
        <div class="row">
            <div class="col-xl-12 col-md-12 mb-4">
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <!-- DataTales Example -->
        <div class="card shadow col-xl-10 col-md-6 mb-4 px-0 mx-3">
            <div class="card-header py-3 d-flex justify-content-between align-items-baseline">
                <h6 class="m-0 font-weight-bold text-primary">Open Orders</h6>
                <a href="{{ route('admin.orders.create') }}" class="btn btn-primary">Create Order</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Booster</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Booster</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @foreach($boosters as $booster)
                        <tr class="tableVisibleRow">
                            <td>{{ $booster->name }}</td>
                            <td>
                            <form method="post" action="{{ route('admin.orders.assign.submit', ['order' => $orderID]) }}">
                                @csrf
                                <input type="hidden" name="booster_id" value="{{ $booster->id }}" />
                                <input type="submit" class="btn btn-outline-success btn-sm" value="Give" />
                            </form>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>

</x-app-layout>
