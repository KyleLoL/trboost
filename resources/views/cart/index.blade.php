<x-layout>
    @push('sheets')
    <style>
        body {
            background-color: #fff !important;
        }
        .cart {
            display: flex;
            justify-content: space-between;
            font-family: 'Poppins', sans-serif;
        }
        .boost {
            flex: 1 1 45%;
        }
        .checkout {
            position: relative;
            flex: 1 1 55%;
            border: 1px solid #ccc;
            border-radius: 1rem;
            padding: 2rem;
        }
        .price {
            margin-top: 3rem;   
        }
        .price span {
            display: inline-block;
            margin-bottom: 1rem;
            font-weight: 600;
            font-size: 1.25rem;
            margin-left: 0.25rem;
        }
        .checkout-btn {
            padding: 0.5rem 1.5rem;
            font-size: 0.875rem;
            border-radius: 8px;
            line-height: 1.375rem;
            cursor: pointer;
        }
        .checkout__input__group {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 1rem;
        }
        .checkout__input__group label {
            width: 35%;
            padding-right: 2rem;
        }
        .checkout__input {
            box-sizing: border-box;
            height: 45px;
            font-family: Barlow,sans-serif;
            font-size: 16px;
            font-weight: 500;
            line-height: 25px;
            text-align: left;
            color: #9494a4;
            padding: 10px 10px 10px 15px;
            border: none;
            border-radius: 3px;
            box-shadow: none;
            background-color: #f1f1f3;
            outline: 0;

            flex-grow: 1;
        }
        .boost__row {
            margin: 0.5rem 0;
            display: flex;
            justify-content: space-between;
            align-items: baseline;
        }
        .boost__row--title {
            margin: 3rem 0 1rem 0;
        }
        .boost__row--title:first-child {
            margin-top: 0;
        }
        .boost__row--title h4 {
            margin: 0;
        }
        .boost__title {
            font-size: 1.125rem;
            font-weight: 600;
            margin: 0;
            width: 40%;
        }
        .boost__value {
            width: 60%;
        }
    </style>
    @endpush
    <section class="section">
        <div class="section__wrapper">
            <header class="section__header flex">
                <h2 class="section__header__title">Checkout <span>Page</span></h2>
                <p class="section__header__sub"><a href="/">Home</a> / <span style="color: #ff4719;">Cart</span></p>
            </header>
            <section>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    <br />
                @endif
                
                @if(isset($cart))
                <div class="cart">
                    <div class="boost">
                        <div class="boost__row boost__row--title">
                            <h4>Boost Settings:</h4>
                        </div>
                        <div class="boost__row">
                            <h4 class="boost__title">Server:</h4>
                            <span class="boost__value">{{ $cart['server'] }}</span>
                        </div>

                        <div class="boost__row">
                            <h4 class="boost__title">Queue:</h4>
                            <span class="boost__value">
                            @switch($cart['queue_type'])
                                @case(1)
                                    Solo | Solo/Duo Ladder
                                    @break

                                @case(2)
                                    Duo | Solo/Duo Ladder
                                    @break

                                @case(3)
                                    Solo | 5v5 Flex Ladder
                                    @break

                                @case(4)
                                    Duo | 5v5 Flex Ladder
                                    @break
                            @endswitch
                            </span>
                        </div>

                        <div class="boost__row">
                            <h4 class="boost__title">Boost:</h4>
                            <span class="boost__value">
                                @switch($cart['boost_type'])
                                @case(1)
                                    Division
                                    @break

                                @case(2)
                                    Placements
                                    @break

                                @case(3)
                                    Net Wins
                                    @break
                            @endswitch
                            </span>
                        </div>

                        <div class="boost__row">
                            <h4 class="boost__title">Rank:</h4>
                            <span class="boost__value">{{ $currentRank }}
                                @if($cart['current_lp'] > 0) {{ $cart['current_lp'] }} LP @endif
                                @if($desiredRank)
                                <i class="lni lni-arrow-right" style="margin: 0 0.25rem; font-size: 1.125rem;"></i> {{ $desiredRank }}
                                @endif
                                </span>
                        </div>

                        @if($cart['wins'] > 0)
                        <div class="boost__row">
                            <h4 class="boost__title">Net Wins:</h4>
                            <span class="boost__value">{{ $cart['wins'] }}</span>
                        </div>
                        @endif

                        @if(!is_null($cart['role1']) || !is_null($cart['spell1']))
                        <div class="boost__row boost__row--title">
                            <h4>Extras</h4>
                        </div>
                        @endif

                        @if(!is_null($cart['role1']) && !is_numeric($cart['role1']))
                        <div class="boost__row">
                            <h4 class="boost__title">Roles:</h4>
                            <span class="boost__value">{{ $cart['role1'] }} / {{ $cart['role2'] }}</span>
                        </div>
                        @elseif(!is_null($cart['role1']))
                        <div class="boost__row">
                            <h4 class="boost__title">Roles:</h4>
                            <span class="boost__value">{{ $roles[$cart['role1']-1] }} / {{ $roles[$cart['role2']-1] }}</span>
                        </div>
                        @endif

                        @if(!is_null($cart['spell1']) && !is_numeric($cart['spell1'])))
                        <div class="boost__row">
                            <h4 class="boost__title">Spells:</h4>
                            <span class="boost__value">[D] {{ $cart['spell1'] }} | [F] {{ $cart['spell2'] }}</span>
                        </div>
                        @elseif(!is_null($cart['spell1']))
                        <div class="boost__row">
                            <h4 class="boost__title">Spells:</h4>
                            <span class="boost__value">[D] {{ $spells[$cart['spell1']-1] }} | [F] {{ $spells[$cart['spell2']-1] }}</span>
                        </div>
                        @endif

                        @if(!is_null($cart['champions']))
                        <div class="boost__row">
                            <h4 class="boost__title">Champions:</h4>
                            <span class="boost__value">Aatrox</span>
                        </div>
                        @endif
                    </div>
                    <div class="checkout">
                        <form method="post" action="{{ route('cart.checkout.post') }}">
                            @csrf

                            @foreach($cart as $key => $val)
                                <input type="hidden" name="{{ $key }}" value="{{ $val }}" />
                            @endforeach

                            <div class="checkout__input__group">
                                <label for="fullname">Full Name:</label>
                                <input class="checkout__input" id="fullname" name="fullname" type="text" placeholder="John Doe" />
                            </div>

                            <div class="checkout__input__group">
                                <label for="email">Email:</label>
                                <input class="checkout__input" id="email" name="email" type="text" placeholder="johndoe@gmail.com" />
                            </div>

                            <div class="checkout__input__group">
                                <label for="phone">Phone Number:</label>
                                <input class="checkout__input" id="phone" name="phone" type="text" placeholder="+01-716-495-8197" />
                            </div>

                            <div class="checkout__input__group">
                                <label for="summoner_name">Summoner Name:</label>
                                <input class="checkout__input" id="summoner_name" name="summoner_name" type="text" placeholder="Faker" />
                            </div>

                            @if(in_array($cart['queue_type'], array(1,3)))
                                <div class="checkout__input__group">
                                    <label for="lol_user">LoL Username:</label>
                                    <input class="checkout__input" id="lol_user" name="lol_user" type="text" placeholder="FakerUser" />
                                </div>
                                <div class="checkout__input__group">
                                    <label for="lol_pass">LoL Password:</label>
                                    <input class="checkout__input" id="lol_pass" name="lol_pass" type="password" />
                                </div>
                            @endif

                            <div class="price">
                                <span>Price: {{ $price }}</span><br />
                                <input type="submit" class="btn btn--primary checkout-btn" value="Checkout" />
                            </div>
   
                        </form>
                    </div>
                </div>
                @else
                    <h4>Cart is empty</h4>
                    <a href="{{ route('boost') }}" class="btn btn--primary" style="margin-top: 1rem;">Get Boosting</a>
                @endif


            </section>
        </div>
    </section>
</x-layout>