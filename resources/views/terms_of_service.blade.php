<!DOCTYPE html>

<html lang="en">

    <head>

        <meta charset="UTF-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>EloBoostTR Best Elo Boosting Services</title>

            

        <script type="text/javascript" src="js/scrollFunction.js"></script>

        -------------------------------------------------------------------------------------------------------------------------->

        <link rel='stylesheet' href='css/ExternalCss/bootstrap.min.css' type='text/css' media='all' />

        <link rel='stylesheet' href='css/ExternalCss/hostwhmcs.css' type='text/css' media='all' />  

       

        <!-- External Style Sheets End ----------------------------------------------------------------------------------------------------------------------------------------------------------------->

        <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>

        <script src="https://use.fontawesome.com/d983741785.js"></script>

        <script src="https://kit.fontawesome.com/870b7dbf87.js" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">

        <link href="css/icon-font/lineicons.css" rel="stylesheet">

        <link rel="stylesheet" href="css/style.css">

        <link rel="stylesheet" href="css/calculator.css">

        <link rel="stylesheet" href="css/my_custom_inline_style.css">

    </head>

    <body>

        <header class="header ">

            <div class="header__navbar " id="header__navbar">

                <nav class="navbar flex">

                    <a href="/" class="navbar__brand" style="color:#ff4719;margin-right: 430px;">EloBoostTR</a>



                    <ul class="navbar__list flex" id="navbar__list">

                        <li><a href="/" class="navbar__list__item navbar__list__item--active" style="color:#ff4719;">Home</a></li>

                        <li><a href="boost" class="navbar__list__item">LoL Boosting</a></li>

                        <li><a href="demopage" class="navbar__list__item">Demo Page</a></li>

                        <li><a href="contact_us" class="navbar__list__item">Contact Us</a></li>

                        <li><a href="login" class="navbar__list__button btn btn--primary">Boost Area</a></li>

                    </ul>

                    <button class="hamburger hamburger--collapse" id="header__hamburger" type="button">

                  <span class="hamburger-box">

                    <span class="hamburger-inner"></span>

                  </span>

                    </button>

                </nav>

            </div>

            

        </header>



        <main>

            <!--Join Us Section Start---------------------------------------------------------------------------------------------------------------------------------------------------------------------->



            <div id="pageHeader" class="global ">

                <div class="container">

                    <div class="section--title">

                        <div class="row">

                            <div class="col-md-6">

                                <h2 class="join_us_h2">Terms of Service<span> Page</span></h2>                

                            </div>

                            

                            <div class="col-md-6 ">

                                <div class="page-header--breadcrumb woo-breadcrumbs">

                                    <ul class="breadcrumb">

                                        <li>

                                            <a href="index" class="home_a">Home</a>

                                        </li>

                                        <li>

                                            Terms of Service

                                        </li>

                                    </ul>

                                </div> 

                            </div>

                        </div>

                    </div>

                </div>

            </div>



            <section class="tosContainer">

                

                <div class="container">



                    <p>

                        <h2 class="title" style="font-size:25px;">

                            ELOBOOSTTR.COM WEB SİTESİNİN SATIŞ GENEL HÜKÜM VE KOŞULLARI

                        </h2>

                    </p>

                    <p>

                        <strong>I.</strong> Web sitesinin Genel Satış Hüküm ve Koşulları, https://www.eloboosttr.com (bundan böyle "EloBoostTR" olarak anılacaktır) web sitesinde, Kullanıcılar tarafından web sitesinin kullanım koşullarını belirtir.

                    </p>

                    <p>

                        Web sitesinin yöneticisi ("Hizmet sağlayıcı"):

                    </p>

                    <p>

                        Şirketimiz Ozan Aydoğan Şahıs Firması'dır,

                    </p>

                    <p>

                        

                    </p>

                    <p>

                        Yasal kayıtlı adresi;

                    </p>

                    <p>

                        - Kavaklıdere Mahallesi, Esat Caddesi,

                    </p>

                    <p>

                        - Bina NO: 12, İç Kapı NO: 1,

                    </p>

                    <p>

                        - ÇANKAYA/ ANKARA.

                    </p>

                    <p>

                        KAVAKLIDERE VERGİ DAİRESİ MÜDÜRLÜĞÜ, 1180343860 vergi NO'lu mükellefidir.

                    </p>

                    <p>

                        Servis Sağlayıcı'nın e-posta adresi: support@eloboosttr.com

                    </p>

                    <p>

                        Kullanıcılar ve Hizmet Sağlayıcı arasındaki herhangi bir niyet beyanı elektronik ortamda duyurulacaktır, yani kayıt formunda belirtilen e-posta adresine bir e-posta gönderilerek.

                    </p>

                    <p>

                        

                    </p>

                    <p>

                        <strong>II.</strong> EloBoostTR, League of Legends (bundan böyle "LOL" olarak anılacaktır) adlı bir oyunda Güçlendirme(Elo Boosting) gibi dijital hizmetleri satın almak için tasarlanmış web sitesini yönetir ve işletir.

                    </p>

                    <p>

                        <strong>III.</strong> Web sitesi, kullanıcıların kendi aralarındaki işlemleri gerçekleştirebilecekleri çevrimiçi bir platformdur.

                    </p>

                    <p>

                        <strong>IV.</strong> EloBoostTR, Boosting hizmetleri sağlamaz. Hizmet, Güçlendiriciler(Boosterlar) ve Kullanıcılar arasındaki işlemlerin gerçekleştirilebilmesini mümkün kılar. Hizmet, istenilen aşama/lig'e ulaşmaya dayalıdır. EloBoostTR, Avrupa Batı, Avrupa Kuzey ve Doğu,  Rusya ve Türkiye gibi sunucularda Elo Boosting hizmetinin gerçekleştirilmesine olanak sağlar.

                    </p>

                    <p>

                        <h2 class="title" style="font-size:25px;">

                            Tanımlar:

                        </h2>

                    </p>

                    <p>

                        <strong>Hesap</strong> - web sitesindeki kullanıcı için bir hesap, etkinleştirildiğinde kullanıcı sitede sunulan hizmetin tam kapasitesine ve işlevselliğine sahiptir.

                    </p>

                    <p>

                        <strong>Dijital Hizmetler</strong> - Kullanıcının bireysel olarak bazı opsiyonlardan yaptıkları seçimler temelinde oluşturulan fiziksel olmayan hizmetler. eloboosttr.com tek bir türde dijital hizmet sağlar: Güçlendirme veya daha sık kullanılan tabiri ile, Elo Boosting.

                    </p>

                    <p>

                        <strong>Solo Boost hizmetleri</strong> - bu, bize ilgili bilgiler verdildikten sonra güçlendiricinin(Booster'ın) hesabınıza giriş yapacağı ve istediğiniz aşamaya bir Elo Güçlendirmesi(Elo Boosting) işlemi gerçekleştirmeye başlayacağı bir tür dijital hizmettir.

                    </p>

                    <p>

                        <strong>Duo Boost hizmetleri</strong> - Müşterinin kendi hesabında oynayacağı ve booster'ın istediği aşamaya ulaşana kadar onunla oynayacağı dijital hizmetler türüdür.

                    </p>

                    <p>

                        <strong>Kullanıcılar</strong> - web sitesinin gerçekleştirilmesine olanak tanıdığı işlemlerden  herhangi birini gerçekleştirmeye uygun olan bir kişi.

                    </p>

                    <p>

                        <strong>Misafir Hesabı</strong> - Hizmet Sağlayıcı tarafından web sitesinde bir hesap oluşturmak için zımni rızasını ifade eden müşteriye ait olan Hizmet Sağlayıcı tarafından oluşturulan bir hesap.

                    </p>

                    <p>

                        <strong>Abone</strong> - EloBoostTR bültenine kayıt olabilen herhangi bir kişi.

                    </p>

                    <p>

                        <strong>Hizmet Sağlayıcı</strong> - EloBoostTR - Ozan Aydoğan Şahıs Firması,

                    </p>

                    <p>

                        Yasal kayıtlı adresi;

                    </p>

                    <p>

                        - Kavaklıdere Mahallesi, Esat Caddesi,

                    </p>

                    <p>

                        - Bina NO: 12, İç Kapı NO: 1,

                    </p>

                    <p>

                        - ÇANKAYA/ ANKARA.

                    </p>

                    <p>

                        KAVAKLIDERE VERGİ DAİRESİ MÜDÜRLÜĞÜ, 1180343860 vergi NO'lu mükellefi.

                    </p>

                    <p>

                        <strong>Web Sitesi</strong> - https://www.eloboosttr.com bağlantısı altında bulunan site. Başka hiçbir web sitesinin EloBoostTR ile doğrudan bağlantısı yoktur.

                    </p>

                    <p>

                        <strong>Elo Boost / MMR Boosting</strong> - çevrimiçi League of Legends oyununda müşterinin siparişi gerçekleştirdiği özellikler temelinde hesabının sanal olarak yükseltilmesi(güçlendirilmesi) olayına konu edinilen sanal bir hizmettir. Hizmetin esası, müşterinin parasını ödediği istenen aşamaya/lige(dereceye) hesabını ulaştırmaktır. Hizmet, oyunda çok fazla bilgi ve deneyime sahip kişiler tarafından gerçekleştirilir. Bu insanlara güçlendirici(Booster) denir. EloBoostTR, bu hizmetin Avrupa Batı, Avrupa Kuzey  ve Doğu, Rusya ve Türkiye olan tüm sunucularda sağlanmasına olanak sunar.

                    </p>

                    <p>

                        <strong>Booster</strong> - çevrimiçi League of Legends oyununda müşterinin siparişi gerçekleştirdiği özellikler temelinde hesabının sanal olarak yükseltilmesi(güçlendirilmesi) olayını gerçekleştiren kişi.

                    </p>

                    <p>

                        <h2 class="title" style="font-size:25px;">

                            1. Web Sitesi Şart ve Koşullarının Uygulanabilirliği ve Kabulü

                        </h2>

                    </p>

                    <p>

                        <strong>1.1</strong> Web sitesinin bütün kullanıcıları, bu Hüküm ve Koşulları ve Gizlilik Politikasını kabul eder.

                    </p>

                    <p>

                        <strong>1.2</strong> EloBoostTR ile yasal olarak bağlayıcı bir sözleşme yapamayan ve ülke düzenlemeleri veya bölgesel düzenlemeler nedeniyle Hizmetleri kullanması yasak olan kullanıcılardan hizmeti kullanmaktan kaçınmaları istenir. Web Sitesi'nde alışveriş yapan kullanıcı, en az on sekiz (18) yaşında olduğunu veya yasal olarak bağlayıcı sözleşmeleri ifade etmesine izin verecek başka bir yaşa geldiğini onaylar. Kullanıcı, üçüncü bir kişi adına ve namına hareket ederse, her biri, bu kişi adına ve namına yasal olarak bağlayıcı sözleşmeler yapmaya yetkili olduğunu iddia eder.

                    </p>

                    <p>

                        <strong>1.3</strong> EloBoostTR, Genel Satış Hüküm ve Koşullar'ında, yeni düzenlemeler eklemek ve eskilerini silmek de dahil olmak üzere, web sitesinde Hüküm ve Koşulların yayınlanması yoluyla yapılabilecek her türlü değişiklik ve düzenlemeyi yapma hakkını saklı tutar. Genel Hüküm ve Koşullar'da yapılacak yeni düzenlemeler ve değişiklikler, Web Sitesi'nde yayımlanmasından bir gün sonra otomatik olarak yürürlüğe girecektir. Şartlar ve Koşullar'da yazılı olan madde ve kurallarda yapılacak her türlü değişiklik, değişiklik, çıkarma veya eklemelere dikkat etmek münhasıran yukarıda belirtilenlerin sorumluluğundadır. EloBoostTR'nin sağladığı herhangi bir hizmeti satın alarak, Hüküm ve Koşullar'ı bir bütün olarak otomatik bir şekilde kabul etmiş olursunuz.

                    </p>

                    <p>

                        <strong>1.4</strong> Bu arada, herhangi bir hizmeti kullanarak veya EloBoostTR ile iletişime geçerek, Kullanıcı, Güçlendirici(Booster), EloBoostTR ile elektronik olarak iletişim kurar. EloBoostTR, söz konusu taraflarla e-posta veya Web Sitesi üzerinden canlı sohbet yoluyla ve farklı distribütörler ile kendi iletişim kanalları aracılığıyla iletişim kurar.

                    </p>

                    <p>

                        <h2 class="title" style="font-size:25px;">

                            2. Fikri mülkiyet, Web Sitesinin Telif Hakkı 

                        </h2>

                    </p>

                    <p>

                        <strong>2.1</strong> İçerik, metinler, fotoğraflar, tasarımlar, logolar, resimler, yazılım, kaynak kodu ve genel olarak sitenin mevcut herhangi bir fikri yaratımı ve bir multimedya sanat eseri olarak tüm site, fikri mülkiyetle ilgili telif hakkı mevzuatı tarafından tamamen korunmaktadır ve mülkiyet hakları Servis Sağlayıcı'ya aittir.

                    </p>

                    <p>

                        <strong>2.2</strong> Söz konusu Web Sitesi'nde yer alan içerikler, Kurum tarafından önceden yazılı olarak izin verilmedikçe, herhangi bir destek veya medya altında tamamen veya kısmen çoğaltılamaz, herhangi bir bilgi erişim sistemi tarafından aktarılamaz veya kaydedilemez.

                    </p>

                    <p>

                        <strong>2.3</strong> Web Sitesi, Kullanıcı'ya yalnızca https://eloboosttr.com Web Sitesi aracılığıyla kullanılabilir olmasıyla sağlanır.

                    </p>

                    <p>

                        <strong>2.4</strong> EloBoostTR iddiaları, Riot Games Inc. ile hiçbir şekilde bağlantılı değildir ve Riot Games Inc. tarafından desteklenmemektedir.

                    </p>

                    <p>

                        <strong>2.5</strong> EloBoostTR, Riot Games'in veya herhangi bir bağlı kuruluşun fikri mülkiyetinde hiçbir mülkiyet iddiasında bulunmaz. Tüm telif hakları ve ticari markalar ilgili sahiplerinin mülkiyetindedir.

                    </p>

                    <p>

                        <h2 class="title" style="font-size:25px;">

                            3. Kayıt Formu, Kullanıcı Hesabı, Misafir Hesabı

                        </h2>

                    </p>

                    <p>

                        <strong>3.1</strong> Kullanıcı, hesap oluşturmak için sitede bulunan kayıt formunu doldurarak kayıt yaptırmakla yükümlüdür. Bu işlem sırasında Kullanıcı, kullanıcı adı ve e-posta adresi gibi bilgileri sağlamakla yükümlüdür. Kayıttan önce, Kullanıcı hizmet açıklamalarına, Genel Satış Hüküm ve Koşulları'na ve Gizlilik Politikası'na aşina olma becerisine ve müsaitliğine sahiptir.

                    </p>

                    <p>

                        <strong>3.2</strong> Herhangi bir Kullanıcı, kayıt olmadan Misafir olarak bir Hizmet satın alma seçeneğini seçerse, Hizmet Sağlayıcı tarafından Web Sitesi'nde bir hesap oluşturmak için zımni onayını beyan eder.

                    </p>

                    <p>

                        <strong>3.3</strong> Bu hesap, Hizmet Sağlayıcı tarafından otomatik olarak oluşturulacak ve daha sonra Müşteri'nin satın alma sürecinde verdiği e-posta adresine elektronik ortamda teslim edilecektir.

                    </p>

                    <p>

                        <strong>3.4</strong> EloBoostTR, Kullanıcı'nın hesabını sonlandırma veya askıya alma hakkını saklı tutar. Bir Kullanıcı'nın hesabı askıya alınır veya kaldırılırsa ve hesapta kalan bakiye varsa, EloBoostTR, Hizmet Sağlayıcı'nın Genel Satış Hüküm ve Koşulları'nın ihlal edilmesi nedeniyle bu bakiyeyi dondurabilir veya kaldırabilir. Genel Satış Hüküm ve Koşulları'nın ihlali hakkında daha fazla bilgi aşağıdaki düzenlemelerde 12, 12.1 ve 12.2 maddelerinde bulunabilir. EloBoostTR, Kullanıcı'nın şirket çıkarlarının ihlali olarak yansıtılabilecek durumlar yaratması halinde, Kullanıcı'nın hesabını sonlandırma veya askıya alma hakkını saklı tutar.

                    </p>

                    <p>

                        <h2 class="title" style="font-size:25px;">

                            4. Web Sitesini Kullanma

                        </h2>

                    </p>

                    <p>

                        <strong>4.1</strong> Web Sitesi'ni kullanmak için kayıt olmanız gerekmez. Ayrıca, Web Sitesi'ni kullanarak şu bölümlere ulaşabilir ve kontrol edebilirsiniz: Ana Sayfa, LoL Boosting, Demo, Blog, Hizmet Şartları, Gizlilik Politikası, Sıkça Sorulan Sorular. Kayıttan önce, ürün açıklaması (dijital hizmetler), Hizmet ve Hizmet Sağlayıcı hakkında açıklama ve bilgiler gibi Web Sitesi'nin içeriğine aşina olma olanağına sahipsiniz. Hizmet Sağlayıcı, yeni açıklamalar eklemek, mevcut olanı değiştirmek ve eskisini silmek de dahil olmak üzere Satış Koşulları'nda Web Sitesi'nin belirli bölümlerinde ilan yoluyla yapılabilecek her türlü değişiklik ve düzenlemeyi yapma hakkını saklı tutar.

                    </p>

                    <p>

                        <strong>4.2</strong> Yeni açıklama ve bilgiler, Web Sitesi'nde yayınlandıktan sonra otomatik olarak yürürlüğe girecektir.

                    </p>

                    <p>

                        <strong>4.3</strong> Hizmet Sağlayıcı, promosyon ve indirimleri sürdürme hakkını saklı tutar. Bu tür eylemlerin kuralları her zaman Web Sitesi'nde mevcut olacaktır: https://eloboosttr.com. Bu kurallar her zaman değiştirilebilir ve bu tamamen Hizmet Sağlayıcı'nın takdirine bağlı olarak olacaktır, bu tür bir değişiklik Kullanım Koşulları'nda yapılan bir değişiklik değildir.

                    </p>

                    <p>

                        <strong>4.4</strong> Hizmet Sağlayıcı, kayıtlı Kullanıcı'ya, yukarıda belirtilen rıza beyanından bağımsız olarak, bu Kullanıcı tarafından Web Sitesi'nin kullanılması, kanun hükümlerinin veya Kullanım Koşulları'nın veya gerekliliğin uygulanması, kanun hükmünden veya Kullanım Koşulları'ndan kaynaklanan yükümlülüklerin yerine getirilmesinin yanı sıra Web Sitesi'nin yeni özellikleri hakkında bilgi vermek  için gerekli olan ticari olmayan bilgileri veya pazarlama materyallerini gönderebilir.

                    </p>

                    <p>

                        <strong>4.5</strong> Hizmet Sağlayıcı, belirli müşteriler için özel indirimli fiyatlandırma yapma hakkını saklı tutar. Özel fiyat, Web Sitesi'nde listelenmeyen yönetimimiz tarafından özel olarak oluşturulmuştur.

                    </p>

                    <p>

                        <strong>4.6</strong> Kullanıcı, Hizmet Sağlayıcı'dan ticari mesajlar (bundan böyle “bülten mesajları” olarak anılacaktır) almayı kabul edebilir. Onay, Kayıttan önce veya sonra herhangi bir zamanda beyan edilebilir. Onay, herhangi bir zamanda elektronik mesaj yoluyla veya ticari bilgilerin sonundaki “Abonelikten çık” bağlantısına tıklanarak iptal edilebilir.

                    </p>

                    <p>

                        <strong>4.7</strong> Kullanıcı, Web Sitesi'ni kendi adına kullanmakla ve Hesabının başka kişiler tarafından kullanılmasına izin vermemekle yükümlüdür. Kullanıcı adı ve parolanın başkaları tarafından erişime karşı yeterince korunması gerekir. Hesap devredilemez ve devralınamaz. Birden fazla Hesap sahibi olmak yasaktır.

                    </p>

                    <p>

                        <strong>4.8</strong> Kullanıcı, giriş (isim) ve şifre gibi doğru hesap bilgilerini kullanarak web sitesine giriş yapabilir.

                    </p>

                    <p>

                        <h2 class="title" style="font-size:25px;">

                            5. Oyun Kurallarına Uygunluk

                        </h2>

                    </p>

                    <p>

                        <strong>5.1</strong> Kullanıcı, kaydolurken, hizmeti sipariş ettiği Sözleşmeyi akdetme, yürütme, düzenleme ve uzlaştırma hakkına sahip olduğunu beyan eder. Bu, Oyun Kuralları ile tutarlı olduğu veya oyunun sahibi veya bireye bu Sözleşmeyi akdetmesi, yürütmesi, düzenlemesi ve uzlaştırması için izin veren başka bir yetkili kuruluş tarafından verildiği anlamına gelir. Aksi takdirde Kullanıcı, Web Sitesi'ni kullanmayı derhal durdurmak ve Sözleşmeyi sona erdirmekle yükümlüdür ve kayıt aşamasında ise siteye kayıt olamaz. 

                    </p>

                    <p>

                        <strong>5.2</strong> Hizmet Sağlayıcı, Web Sitesi'nin Sözleşmeyi akdetme, yürütme, düzenleme ve sulh etme yetkisi olmayan kişilere açık olmadığını beyan eder. 1. paragrafta belirtilen koşulların yerine getirilmesinden tek başına sorumlu olan kişi Kullanıcı'dır.

                    </p>

                    <p>

                        <strong>5.3</strong> Hizmet Sağlayıcı, Kullanıcı'nın Hizmeti sipariş ettiği Sözleşmeyi akdetme, yürütme, düzenleme ve uzlaştırma hakkına sahip olup olmadığı konusunda doğrulamaya ihtiyaç duymaz.

                    </p>

                    <p>

                        <strong>5.4</strong> Paragraf 1'de belirtilen koşullardan herhangi birine uymazsanız veya uymadıysanız, Hizmet Sağlayıcı size atfedilebilen nedenlerle Sözleşmeyi derhal feshedebilir. Hizmet Sağlayıcı aynı zamanda sizin tarafınızdan akdedilen yürütülen, imzalanan veya sonuçlandırılan (veya akdedilmekte, yürütülmekte, imzalanmakta, sonuçlandırılmakta olan) tüm Sözleşmeleri geçersiz ve hükümsüz ilan edebilir. Hükümsüz ve geçersiz ilan edilen Sözleşmelerden kaynaklanan herhangi bir talebi Kullanıcı'lar kendileri çözecektir. Hükümsüz ilan edilen Sözleşme Ücretleri iade edilmeyecektir.

                    </p>

                    <p>

                        <h2 class="title" style="font-size:25px;">

                            6. Web Sitesindeki Satın Alma İşlemleri

                        </h2>

                    </p>

                    <p>

                        <strong>6.1</strong> Müşteri, Web Sitesi aracılığıyla Sanal Hizmetler (Güçlendirme veya daha yaygın tabiri ile Boosting) satın alabilir. Müşteri, satın almadan önce eloboosttr.com Web Sitesi'nin Genel Hüküm ve Koşullar'ını kabul etmelidir.

                    </p>

                    <p>

                        <strong>6.2</strong> Müşteri, yukarıda listelenen ürünleri bir Kullanıcı olarak (Web Sitesi'nin kayıtlı bir üyesi - bir satın alım yapmadan önce) veya bir Misafir olarak (sitede kayıtlı bir Üye değil - satın almadan önce) satın alabilir.

                    </p>

                    <p>

                        <strong>6.3</strong> Müşteri, ürünü Misafir olarak satın alırsa, hesap Hizmet Sağlayıcı tarafından otomatik olarak oluşturulacak ve daha sonra müşteriye satın alma sürecinde verdiği e-posta adresine elektronik ortamda iletilecektir. 

                    </p>

                    <p>

                        <strong>6.4</strong> Müşteriler, aşağıdaki hizmet ve ürünleri satın almak isteyen kişilerdir: League of Legends adlı sanal bir oyunda hesap güçlendirilmesi(hesabın elo'sunun yükseltilmesi - elo boosting). Müşteri ürünü Web Sitesi'ne yükleyerek yayınlar. Listelenen hizmetlerin yürütülmesi, Kullanıcı ile Hizmet Sağlayıcı arasındaki esas sözleşmeye göre yapılır.

                    </p>

                    <p>

                        <strong>6.5</strong> Hizmetin satın alınmasından sonra Müşteri, yeterli miktarda bilgi vermekle yükümlüdür.

                    </p>

                    <p>

                        <strong>6.5.1</strong> Müşteri, Tekli Güçlendirme(Solo Boosting) Hizmeti satın alırsa, aşağıdaki bilgileri sağlamalıdır: Kullanıcı Adı, Şifre, Sihirdar adı. Müşteri ayrıca “Müşteri Açıklaması” ekleyebilir, bu kısım sadece isteğe bağlıdır.

                    </p>

                    <p>

                        <strong>6.5.2</strong> Müşteri, İkili Güçlendirme(Duo Boosting) Hizmeti satın alırsa, aşağıdaki bilgileri sağlamalıdır: Sihirdar adı. Müşteri ayrıca “Müşteri Açıklaması” ekleyebilir, bu kısım sadece isteğe bağlıdır.

                    </p>

                    <p>

                        <strong>6.6</strong> Bir Hizmetin satın alınmasından sonra müşteri, profilinde Hizmeti özelleştirebileceği (hesap kimlik bilgilerini güncelleyebileceği, hizmeti duraklatabileceği, Güçlendirme(Boost)'nin ilerlemesini kontrol edebileceği, Güçlendirici(Booster) ile planlayacağı ve doğrudan sohbet edebileceği) belirli alanlara erişim kazanır(Benzeri özellikte alanlar Güçlendirici(Booster)'nin profiline de doğru etkileşimlere sahip olabilmeleri adına eklenmiştir).

                    </p>

                    <p>

                        <h2 class="title" style="font-size:25px;">

                            7. Sipariş Süreci, Müşteri ve Hizmet Sağlayıcı Yükümlülükleri, Hizmet hakları

                        </h2>

                    </p>

                    <p>

                        <strong>7.1</strong> Müşteri ile Hizmet Sağlayıcı arasındaki sözleşme, Sanal Hizmet (Güçlendirme ya da daha genel tabiri ile Boosting) teslim edilene kadar geçerlidir.

                    </p>

                    <p>

                        <strong>7.1.1</strong> Müşteri bir Aşama Yükseltme hizmeti satın aldıysa, sözleşme, Hizmet Sağlayıcı tarafından istenen Aşama Yükseltme hizmeti teslim edilene kadar geçerlidir.

                    </p>

                    <p>

                        <strong>7.1.2</strong> Müşteri belirli bir miktar Net Win Boost (Garanti Kazanç Güçlendirmesi) hizmeti satın aldıysa, sözleşme ilgili miktarda oyunlar alakalı hesapta alınıncaya kadar geçerlidir.

                    </p>

                    <p>

                        <strong>7.1.3</strong> Müşteri bir Yerleştirme Maçları Hizmeti satın aldıysa, sözleşme, satın alınan maç miktarı sağlanana kadar geçerlidir.

                    </p>

                    <p>

                        <strong>7.2</strong> Müşteri'nin 120 gün içinde herhangi bir işlem yapmaması; 6.5, 6.51, 6.52 maddelerinde belirtilen yeterli bilgiyi sağlamaması; Güçlendirici(Booster) ile 120 gün veya daha uzun süre boyunca iletişim kurmaması veya 120 gün veya daha uzun süre profiline giriş yapmaması gibi bir durumda; Müşteri ve Hizmet Sağlayıcı arasındaki sözleşme Hizmet Sağlayıcı tarafından feshedilebilir.

                    </p>

                    <p>

                        <strong>7.2.1</strong> Müşteri ile Hizmet Sağlayıcı arasındaki sözleşme, müşterinin eloboosttr.com Web Sitesi'nin aşağıdaki Genel Satış Hüküm ve Koşulları'nı ihlal etmesi durumunda Hizmet Sağlayıcı tarafından feshedilebilir.

                    </p>

                    <p>

                        <strong>7.2.2</strong> Müşteri, bilerek besliyor,Güçlendirici(Booster)'ye baskı yaparak veya hakaret ederek konumunu kötüye kullanıyorsa; Müşteri ile Hizmet Sağlayıcı arasındaki sözleşme Hizmet Sağlayıcı tarafından feshedilebilir. 

                    </p>

                    <p>

                        <strong>7.2.3</strong> Müşteri, hizmet sağlayıcılarımıza veya satıcılarımıza özel fırsatlar sunmak dahil ancak bunlarla sınırlı olmamak üzere herhangi bir dolandırıcılık faaliyetinde bulunursa, EloBoostTR web sitesi dışında hizmet sağlayıcılarımızla iletişime geçierse, yanlış raporlar göndererek ve yöneticilere hizmetiyle ilgili geçersiz bilgiler verirse, Müşteri ve Hizmet Sağlayıcı arasındaki sözleşme Hizmet Sağlayıcı tarafından feshedilebilir, önceden haber vermeksizin siparişi feshetme hakkımızı saklı tutarız.

                    </p>

                    <p>

                        <strong>7.2.4</strong> Hizmet Sağlayıcının sözleşmeyi feshetmesi durumunda, kullanılmayan kredi ve satın alınan hizmetlerin bedeli yönetim ücreti olarak düşülecektir.

                    </p>

                    <p>

                        <strong>7.3</strong> Müşteri olarak siz, Garanti Kazanç Güçlendirmesi(Net Wins Boost) Hizmeti satın alırsanız, belirli bir sayıda kazanılması garanti edilen maç satın aldığınızı kabul etmiş olursunuz. Eğer "x" adet kazanılması garanti edilen maç satın alırsanız, kaybedilen maçlar bu miktara +1, kazanılan oyunlar ise bu miktara - 1 olarak sayılır. Hizmet bu sayı 0'a ulaştığında tamamlanır.

                    </p>

                    <p>

                        <strong>7.3.1</strong> Eğer Güçlendirici(Booster) promosyondaki bir oyunu kaybederse, Güçlendirici(Booster) promosyonu kaybetmediği sürece ekstra bir oyun olarak sayılmaz, bu kural uygulanmaz.

                    </p>

                    <p>

                        <strong>7.4</strong> Müşteri olarak siz, herhangi bir aşamadaki Lig Puanı (bundan sonra LP olarak anılacaktır) maç başına kazancınız 13 LP'nin altındaysa, artırma hesaplayıcısı tarafından belirlenen ekstra bir miktarı(siparişin tutarının %40'ı) ödemeniz gerektiğini kabul edersiniz. Veya sitemizdeki kazanç başına(Net Wins Boost) fiyat hesaplayıcıyı kullanarak siparişinizi Garanti Kazanç Güçlendirmesi(Net Wins Boost) Hizmetine dönüştüreceğiz. Örneğin, Bronz IV'ten Bronz II'ye bir hizmet satın aldıysanız ve hesabınız Bronz aşama IV veya III'te kazanılan maç başına 13 LP'nin altında LP kazancı elde ettiyse, Garanti Kazanç Güçlendirmesi(Net Wins Boost) Hizmetine dönüştürme veya fazladan bir miktar para ödeme seçeneğiniz vardır.

                    </p>

                    <p>

                        <strong>7.4.1</strong> Müşteri olarak, herhangi bir İkili Güçlendirme(Duo Boost) Hizmetinde KDA'nız(Kill/Death/Assist - Oyundaki oyuncunun sergilediği performansın başarı derecesini ifadeleyen bir değerdir.) 1,6'nın altındaysa, siparişinizin fiyatını izleyerek siparişinizi Solo Boost Hizmetlerine dönüştürme hakkımızı saklı tuttuğumuzu kabul edersiniz.

                    </p>

                    <p>

                        <strong>7.5</strong> Müşteri olarak siz, hizmet sırasında satın aldığınız seçili Dereceli Sıra'sı türünde herhangi bir dereceli oyun oynarsanız, LP'nizin Güçlendirici(Booster)'nin bıraktığı LP tutarından düşük olması durumunda aradaki farkı ödemeniz gerektiğini kabul edersiniz.

                    </p>

                    <p>

                        <strong>7.5.1</strong> Müşteri, hizmet sırasında hesabını alakalı Dereceli Sıra'sında Güçlendirici(Booster)'ın en son ulaştırdığı miktardaki LP değerinin altına düşürürse veya hesabının alakalı Dereceli Sıra'da kazanılan maç başına elde ettiği LP miktarı 13LP'nin altında ise, Hizmet Sağlayıcı hizmeti duraklatma hakkını saklı tutar. Hizmet Sağlayıcı, hizmet değerindeki fark karşılandıktan sonra hizmetin devam etmesine izin verecektir.

                    </p>

                    <p>

                        <strong>7.5.2</strong> Müşteri, para farkını karşılamayı reddederse, Hizmet Sağlayıcı, müşteri eylemlerinin neden olduğu finansal zararı ve hizmetin başlangıç ​​değerini hesaplayarak hizmeti farklı bir türe dönüştürme hakkını saklı tutar.

                    </p>

                    <p>

                        <strong>7.5.3</strong> Net Win Boost(Garanti Kazanç Güçlendirmesi) hizmeti sırasında müşterinin herhangi bir League of Legends oyun modunu oynamasına izin verilir.

                    </p>

                    <p>

                        <strong>7.5.4</strong> Bir Aşama Yükseltme(Division Boosting) hizmeti sırasında, müşterinin, yükseltme hizmeti için seçilen oyun modu dışında farklı oyun modlarını oynamasına izin verilir.

                    </p>

                    <p>

                        <strong>7.6</strong> Hizmet Sağlayıcı, Yerleştirme Maçları(Placement Games) Hizmeti sırasında minimum %70'lik kazanma oranı sağlamakla yükümlüdür, Hizmet Sağlayıcı daha düşük bir kazanç yüzdesi sağlarsa, müşteri ücretsiz olarak 1 Aşama Yükseltme hizmeti alacaktır.

                    </p>

                    <p>

                        <strong>7.6.1</strong> Bu kural, müşteri beş veya daha fazla Yerleştirme Maçı satın alırsa gerçekleşir.

                    </p>

                    <p>

                        <strong>7.6.2</strong> Müşteri, beşten az Yerleştirme Maçı satın alırsa ve Hizmet Sağlayıcı %70'den daha az miktarda kazanç sağlarsa, yerleştirme maçlarınızı daha düşük bir kazanma oranıyla gerçekleştirirse, Güçlendirici(booster)'nin kaybettiği maçlar ile aynı sayıda maçı size ücretsiz Net Wins Boost(Garanti Kazanç Güçlendirmesi) hizmeti şeklinde vererek durumu telafi edeceğiz.

                    </p>

                    <p>

                        <strong>7.7</strong> Sanal Hizmet (Güçlendirme - Boosting) tamamlandığında, Hizmet Sağlayıcı elektronik bildirim göndererek müşteriye bilgi verecektir. Bildirim, hizmetin tamamlandığının kanıtını içerecektir.

                    </p>

                    <p>

                        <strong>7.7.1</strong> Hizmetin tamamlandığının kanıtı, ekte Hizmetin tamamlandığını doğrulayan bir ekran görüntüsü içerecektir.

                    </p>

                    <p>

                        <h2 class="title" style="font-size:25px;">

                            8. Boost Hizmetlerinde İade ve Ters İbraz Politikası

                        </h2>

                    </p>

                    <p>

                        <strong>8.1</strong> Müşteri olarak siz, bir Sanal Hizmet (Güçlendirme - Boosting) satın aldığınızda ve Güçlendirici(Booster)'niz atandığında veya Hizmet başladığında, artık geri ödeme almaya uygun olmadığınızı kabul ediyorsunuz. Hizmet tamamlanmadıysa, karşılığı henüz takdim edilmemiş olan ödemenizi herhangi bir hizmetimizden tekabül ettiği oranda yararlanmak için kullanmak hakkında sahipsiniz. Hizmet Sağlayıcı, kullanılmayan ödemelerinizi sonlandıramaz.

                    </p>

                    <p>

                        <strong>8.2</strong> Müşteri olarak siz, sipariş işleme alındıktan veya hizmet başladıktan veya tamamlandıktan sonra bir talep açarsanız, eloboosttr.com'un kullanım şartlarını doğrudan ihlal ettiğinizi ve ya talebi kapatmakla ya da eloboosttr.com tarafından belirlenen ücrete ek olarak aynı tutarı 100 Türk Lirası'ndan az olmamak üzere ve verilen siparişin maliyeti 100 Türk Lirası'ndan fazla ise orijinal tutarın üç katından fazla olmamak üzere geri ödemekle yasal olarak yükümlü olduğunuzu kabul edersiniz. Her iki seçeneğe de uymamanız durumunda, müşteri olarak siz, ilgili mahkemelerce tarafınıza atfedilebilecek olan yaptırımlar ile alakalı tüm sorumluluğu kabul edersiniz.

                    </p>

                    <p>

                        <strong>8.3</strong> Müşteri, satın alma işleminden sonraki 48 saat içinde Sanal Hizmet (Güçlendirme - Boosting) için geri ödeme talep etme hakkına sahiptir, bu, Güçlendirici(Booster)'nin atanmadığı veya hizmetin başlamadığı durumlar için geçerlidir. Müşteri, iletişim formunda bir sorgu göndererek veya canlı sohbet yoluyla bizimle iletişime geçerek geri ödeme talebinde bulunabilir. Müşteri 6.5., 6.5.1, 6.5.2 maddelerindeki yükümlülükleri yerine getirmediyse Hizmet Sağlayıcı sorumlu değildir ve geri ödemeye devam etmeyecektir.

                    </p>

                    <p>

                        <strong>8.3.1</strong> Müşteri olarak siz, geri ödeme talep etmeniz durumunda Hizmetin toplam tutarının %25'i oranında yönetim ücretinin uygulanacağını kabul edersiniz.

                    </p>

                    <p>

                        <strong>8.3.2</strong> Müşteri olarak siz, geri ödeme talep etmeniz durumunda Hizmet Sağlayıcı'nın kullanacağı ödeme sağlayıcının uygulacağı işlem ücretlendirmelerinin sizin sorumluluğunuza olduğunu kabul edersiniz.

                    </p>

                    <p>

                        <strong>8.3.3</strong> Müşteri iade talebinde bulunduktan sonra; Hizmet Sağlayıcı, talebin gönderiliş tarihinden sonraki 14 gün içerisinde geri ödeme işlemini gerçekleştirir.

                    </p>

                    <p>

                        <strong>8.4</strong> Müşteri, satın alma tarihinden itibaren 14 gün sonra Sanal Hizmet (Güçlendirme - Boosting) için para iadesi talep etme hakkına sahiptir, bu, Güçlendirici(Booster)'nin atanmadığı veya hizmetin o sırada başlamadığı durumlar için geçerlidir. Müşteri, iletişim formunda bir sorgu göndererek veya canlı sohbet yoluyla bizimle iletişime geçerek geri ödeme talebinde bulunabilir. Müşteri 6.5., 6.5.1, 6.5.2 maddelerindeki yükümlülükleri yerine getirmediyse Hizmet Sağlayıcı sorumlu değildir ve geri ödemeye devam etmeyecektir.

                    </p>

                    <p>

                        <strong>8.4.1</strong> Müşteri olarak siz, geri ödeme talep etmeniz durumunda yönetim ücretinin uygulanmayacağını kabul edersiniz.

                    </p>

                    <p>

                        <strong>8.4.2</strong> Müşteri olarak siz, geri ödeme talep etmeniz durumunda Hizmet Sağlayıcı'nın ödeme sağlayıcı ücretini sizin sorumluluğunuzda olduğunu kabul edersiniz.

                    </p>

                    <p>

                        <strong>8.4.3</strong> Müşteri iade talebinde bulunduktan sonra; Hizmet Sağlayıcı, talebi gönderdikten sonra 14 gün içinde aşağıdaki geri ödemeyi gerçekleştirir.

                    </p>

                    <p>

                        <strong>8.5</strong> Yukarıda listelenmeyen özel durumlarda, Hizmet Sağlayıcı geri ödeme veya kısmi geri ödeme yapma hakkını saklı tutar. Tüm bu geri ödemeler, site yönetiminin münhasır takdirine bağlı olacaktır.

                    </p>

                    <p>

                        <strong>8.5.1</strong> Siz Müşteri, bu özel durumlarda Hizmetin toplam tutarının %10'u kadarına tekabül eden yönetim ücretinin uygulanacağını kabul edersiniz.

                    </p>

                    <p>

                        <strong>8.5.2</strong> Siz Müşteri, bu özel durumlarda Hizmet Sağlayıcı'nın kullandığı ödeme sağlayıcı işlem ücretlendirmelerinin sizin sorumluluğunuzda olduğunu kabul edersiniz.

                    </p>

                    <p>

                        <strong>8.5.3</strong> Siz Müşteri, bu özel durumlarda Hizmet Sağlayıcı'nın talebi gönderdikten sonra 14 gün içinde aşağıdaki geri ödemeyi gerçekleştireceğini kabul edersiniz.

                    </p>

                    <p>

                        <strong>8.6</strong> Müşteri olarak siz, tamamlanan bir hizmet için yapılan ödemenin ters ibrazını talep etmeniz halinde faturanızın bir alacak tahsilat kuruluşuna gönderileceğini ve ayrıca bu ödemenin karşılanması için ekstra ücretleri ödemeniz gerektiğini kabul etmektesiniz. hem borç tahsildarları acentesi hem de ters ibrazınız nedeniyle öngörülemeyen diğer maliyetler.

                    </p>

                    <p>

                        <h2 class="title" style="font-size:25px;">

                            9. Şikayet

                        </h2>

                    </p>

                    <p>

                        <strong>9.1</strong> Müşteri, satın aldığı hizmet hakkında şikayette bulunma hakkına sahiptir.

                    </p>

                    <p>

                        <strong>9.2</strong> Hizmetin sağlanmaması veya açıklandığı şekilde sağlanmaması durumunda müşteri şikayette bulunabilir.

                    </p>

                    <p>

                        <strong>9.3</strong> Müşteri şikayette bulunmak isterse aşağıdaki adımları izlemelidir:

                    </p>

                    <p>

                        <strong>9.3.1</strong> Web sitesine gidin.

                    </p>

                    <p>

                        <strong>9.3.2</strong> “Bize Ulaşın” Sekmesine gidin.

                    </p>

                    <p>

                        <strong>9.3.3</strong> Neyin yanlış olduğunu açıklayan “Şikayet” konusu(başlığı) ile bir soruşturma gönderin.

                    </p>

                    <p>

                        <strong>9.4</strong> Tüm şikayetler, şikayetin alındığı tarihten itibaren 14 gün içinde cevaplandırılacaktır.

                    </p>

                    <p>

                        <strong>9.5</strong> Şikayeti çözemezsek, bu müşteriyle anlaştığımız anlamına gelmez.

                    </p>

                    <p>

                        <h2 class="title" style="font-size:25px;">

                            10. Web Sitesinin Rolü

                        </h2>

                    </p>

                    <p>

                        <strong>10.1</strong> Hizmet Sağlayıcı, Kullanıcı'lar arasındaki temaslara hiçbir şekilde katılmaz ve aralarında yapılan herhangi bir fiili veya hukuki işlemin tarafı değildir.

                    </p>

                    <p>

                        <strong>10.2</strong> Hizmet Sağlayıcı, Kullanıcı'lar tarafından Web Sitesi'ne yüklenen veya saklanan herhangi bir Veri'nin doğruluğunu ve güvenilirliğini garanti etmez.

                    </p>

                    <p>

                        <strong>10.3</strong> Hizmet Sağlayıcı, özellikle Kullanıcı'lara karşı aşağıdakilerden sorumlu değildir:

                    </p>

                    <p>

                        - Verilerin gerçeğe aykırı olduğu bir durum,

                    </p>

                    <p>

                       - Kullanıcı'lar tarafından yüklenen Veri'lerin doğruluğu ve güvenilirliği, 

                    </p>

                    <p>

                        - Kullanıcı'nın Sözleşmeleri yerine getirme kabiliyeti,

                    </p>

                    <p>

                        - Kullanıcı'ların ödeme gücü.

                    </p>

                    <p>

                        <strong>10.4</strong> Hizmet Sağlayıcı, Kullanıcı'lar arasındaki anlaşmazlıklarda yer almaz. Tüm anlaşmazlıklar Kullanıcı'ların kendileri tarafından çözülür. Hizmet Sağlayıcı, bu tür anlaşmazlıkların çözümü ile bağlantılı herhangi bir yasal işlemde bulunmaz.

                    </p>

                    <p>

                        <strong>10.5</strong> Hizmet Sağlayıcı, özellikle eğitim sorunları veya Kullanıcı'ların Sözleşmeler ve oyunla ilgili davranışlarından, özellikle bir bilgisayara veya diğer oyunlarla ilişkili bağımlılıklardan, oyunların kazanılmamasından, yasa dışı oyunlara katılımdan ve Oyun Kuralları'nın ihlalinden sorumlu değildir. Özellikle, Hizmet Sağlayıcı, oyundaki hesabınızı bloke etmekten veya askıya almaktan veya sizi oyundan men etmekten sorumlu değildir. Böyle bir olay, Hizmet Sağlayıcı'ya veya başka bir Kullanıcı'ya karşı, özellikle ödenen fonları iade etme iddiasında bulunma hakkını vermez.

                    </p>

                    <p>

                       <strong>10.6</strong> Hizmet Sağlayıcı, yasaya veya sosyal bir arada yaşama ilkelerine aykırıysa veya Hizmet Sağlayıcı'nın iyi adını ihlal ediyorsa, Kullanıcı'ya önceden bildirimde bulunmaksızın sözleşmeyi feshedebilir. 

                    </p>

                    <p>

                        <h2 class="title" style="font-size:25px;">

                            11. Web Sitesinin Sorumluluğu

                        </h2>

                    </p>

                    <p>

                        <strong>11.1</strong> Hizmet Sağlayıcı, Kullanıcı'ların Web Sitesi içindeki herhangi bir eyleminden ve/veya ihmalinden sorumlu değildir. Hizmet Sağlayıcı, Web Sitesi'ne yalnızca Kullanıcı sağlar ve Kullanıcı, Web Sitesi'ni yasalara uygun şekilde kullanmakla yükümlüdür. Hizmet Sağlayıcı, Kullanıcı'ların eylem ve/veya ihmallerinin, onlar tarafından saklanan verilerin veya bunlarla ilgili faaliyetlerin hukuka uygun olup olmadığını kontrol etmekle yükümlü değildir. Kullanıcı, Web Sitesi'ni kullanımı sırasındaki eylemlerinden veya ihmallerinden tamamen sorumludur.

                    </p>

                    <p>

                        <strong>11.2</strong> Kullanım Koşulları öngörmediği sürece; Aksi takdirde, Hizmet Sağlayıcı, Kullanıcı'ya karşı, Hizmetlerin yerine getirilmemesinden veya uygunsuz bir şekilde yerine getirilmesinden kaynaklanan zararlardan yalnızca kendisi sorumluysa sorumludur. İddiaların ispatlanması sorumluluğu Kullanıcı'ya aittir.

                    </p>

                    <p>

                        <strong>11.3</strong> Hizmet Sağlayıcı'nın Kullanıcı'ya karşı sorumluluğu yalnızca gerçek zararı kapsayacaktır, kaybedilen kârları (giden kâr) kapsamaz.

                    </p>

                    <p>

                        <strong>11.4</strong> Yasaların izin verdiği azami ölçüde (tüketiciyi koruma hükümleri dahil), Hizmet Sağlayıcı aşağıdakilerden kaynaklanan hiçbir eylem ve/veya zarardan sorumlu değildir:

                    </p>

                    <p>

                        - Kullanıcı'nın kendisini Hizmet Sağlayıcı tarafından kullanılan teknik gereksinimlere göre ayarlayacak bilişim teknolojisi sisteminin olmaması,

                    </p>

                    <p>

                        - Mücbir sebepler, savaşlar, terör saldırısı, yangın, sunucu odasına su basması, bilgisayar korsanlarının saldırısı, arızalar, erişim sağlayıcıların sebepleri, donanım veya yazılım arızaları gibi Hizmet Sağlayıcı'nın kontrolü dışındaki nedenlerle Web Sitesi'ne erişim imkansızlığı. Kullanıcı'lar, sunucu odalarındaki arızalar, diğer üçüncü kişilerden kaynaklanan diğer sebepler (telekomünikasyon, hosting, banka, posta, kurye, e-posta, kayıt ve alan tutma hizmetleri ve benzeri hizmetleri sağlayan kuruluşlar, ödeme işlemini yürüten kuruluş),

                    </p>

                    <p>

                        - Web Sitesi'nin Kullanıcı veya başka bir kişi tarafından yasa dışı kullanımı,

                    </p>

                    <p>

                        - Her İnternet kullanıcısının yasa ihlalleri, kötü niyetli eylemleri veya ihmalleri,

                    </p>

                    <p>

                        - Web Sitesi dışındaki yazılımların sebepleri (örn. Microsoft Windows),

                    </p>

                    <p>

                        - Servis Sağlayıcı e-posta sunucuları tarafından gönderilen e-postaların reddedilmesi (örn. bu sistemlerin filtreleri, blokları veya arızaları sonucu).

                    </p>

                    <p>

                        <strong>11.5</strong> Hizmet Sağlayıcı, Kullanıcı tarafından potansiyel olarak tetiklenebilecek yetkisiz programların kullanımından sorumlu değildir.

                    </p>

                    <p>

                        <strong>11.6</strong> Hizmet Sağlayıcı, e-postalarının Kullanıcı'nın e-postasına spam veya önemsiz olarak ulaşmasından sorumlu değildir.

                    </p>

                    <p>

                        <strong>11.7</strong> Hizmet Sağlayıcı, Web Sitesi'nin işleyişinde teknik bir kesinti yapma hakkına sahiptir. Hizmet Sağlayıcı, böyle bir kesintinin gerekliliği ani ve/veya beklenmedik olmadıkça, Kullanıcı'ya bu tür bir atılımı bir Mesaj ile bildirecektir.

                    </p>

                    <p>

                        <strong>11.8</strong> Hizmet Sağlayıcı, Web Sitesi'nin bazı özellikleri için geçici sınırlamalar oluşturma, bunları belirli saatlerde kullanıma sunma veya sınırlamaların olmamasının Web Sitesi'nin sürekliliğini ve istikrarını etkileyebileceği durumlarda kısıtlamalar getirme hakkına sahiptir. Hizmet Sağlayıcı, yukarıda belirtilen faaliyetlerin sonuçlarından sorumlu değildir.

                    </p>

                    <p>

                        <strong>11.9</strong> Hizmet Sağlayıcı, özellikle Kullanıcı tarafından kullanılan bir sisteme izinsiz giriş ve e-posta olmak üzere, bilgisayarda bir antivirüs yazılımının olmaması veya İnternet'e korumasız bağlantı nedeniyle Kullanıcı tarafından sürüklenen hiçbir zarardan sorumlu değildir, özellikle, Kullanıcı'nın bilgisayar sistemlerinde bulunan bir üçüncü taraf veya virüs tarafından şifre veya kullanıcı adı bilgilerinin ele geçirilmesi gibi bir durumundan.

                    </p>

                    <p>

                        <h2 class="title" style="font-size:25px;">

                            12. Web Sitesi Kullanımının Sonlandırılması

                        </h2>

                    </p>

                    <p>

                        <strong>12.1</strong> Hizmet Sağlayıcı, Web Sitesi'ni Yasa Dışı bir şekilde kullanırsanız, size haber vermeden Web Sitesi'ni kullanımınıza son verebilir.

                    </p>

                    <p>

                        <strong>12.2</strong> Hizmet Sağlayıcı, özellikle aşağıdaki durumlarda web sitesini kullanımınızı sonlandırabilir:

                    </p>

                    <p>

                        - eloboosttr.com'un Genel Satış Hüküm ve Koşulları'nı ihlal ederseniz,

                    </p>

                    <p>

                        - Yasa Dışı Veri yüklerseniz,

                    </p>

                    <p>

                        - Diğer Kullanıcı'ların Kişisel Veri'lerini serbest bırakırsanız,

                    </p>

                    <p>

                        - Hesabın kullanım koşullarını ihlal ettiğinizde,

                    </p>

                    <p>

                       - Kullanıcı'nın tutumlarının şirket çıkarlarının ihlali olarak değerlendirilmesi durumunda. 

                    </p>

                    <p>

                        <h2 class="title" style="font-size:25px;">

                            13. Son Hükümler

                        </h2>

                    </p>

                    <p>

                        <strong>13.1</strong> Kullanım Koşulları'nın Ekleri ayrılmaz birer parçadır. Şunları içerirler:

                    </p>

                    <p>

                        Kullanım Koşulları, "https://eloboosttr.com" adresindeki Web Sitesi'ndeki herhangi bir Kullanıcı'ya ücretsiz olarak sunulur. Web Sitesi'ni kullanmadan önce veya Web Sitesi'ni kullanmaya başladığınız anda, niteliğinizin el verdiği üzere, Bilişim Teknolojileri sisteminizi kullanarak Kullanım Koşulları'nın içeriğini (olağan işlemler sırasında) zaten alabilir, geri yükleyebilir, koruyabilir ve saklayabilirsiniz. Tereddüt halinde Kullanım Koşulları'nı yorumlayarak Hizmet Sağlayıcı'dan açıklama isteyebilirsiniz.

                    </p>

                    <p>

                        <h2 class="title" style="font-size:25px;">

                            14. Ek Şartlar ve Koşullar

                        </h2>

                    </p>

                    <p>

                        <strong>14.1</strong> Web sitemizde bir satın alma işlemi yapmak için PAYTR.COM A.Ş. (bundan böyle "PayTR Ödeme Hizmetleri Sağlayıcısı" olarak anılacaktır) tarafından sağlanan PayTR ödeme hizmetlerini kullandığınızda, satın alma işleminize ilişkin sorumluluk, satın alma işleminizden önce PAYTR.COM A.Ş.'ye devredilecektir. PAYTR.COM, satın alma işleminizle Kayıtlı Tüccar oluyor. PayTR Ödeme Hizmet Sağlayıcısı, bizim yardımımızla birlikte ödeme ve ödemeyle ilgili müşteri desteği için birincil sorumluluğu üstlenir. Ödeme işlemine devam etmek için işlemin konusunu geçici olarak PAYTR.COM'a emanet edersiniz ve PAYTR.COM ürün ve işlem işleme sorumluluğunu alır. PayTR Ödeme Hizmet Sağlayıcı ödemesi aracılığıyla satın alma yapan müşterilerle ilgili olarak, (i) PayTR Ödeme Hizmet Sağlayıcısı'nın Gizlilik Politikası tüm ödemeler için geçerli olacaktır ve herhangi bir satın alma yapmadan önce gözden geçirilmelidir ve (ii) PayTR Ödeme Hizmetleri Sağlayıcısı Geri Ödeme Politikası ilgili tedarikçi tarafından alıcılara önceden açıkça bildirilmedikçe tüm ödemeler için geçerlidir. Ek olarak, belirli ürünlerin satın alınması, alışveriş yapanların, Biz veya PayTR Ödeme Hizmet Sağlayıcısı yerine ürün tedarikçisi tarafından belirlenen ek koşulları içerebilecek bir veya daha fazla Son Kullanıcı Lisans Sözleşmesi'ni kabul etmesini gerektirebilir.

                    </p>

                    <p>

                        Öğelerinizin satın alınması ve teslimi ile ilgili ödeme hizmetleri sağlayıcıları ile olan ilişkiniz nedeniyle uygulanan ücretlerden veya yerel gümrük görevlileriniz veya diğer düzenleyici kurumlar tarafından uygulanan harç ve vergilerden kaynaklanan her türlü ücret, vergi veya diğer masraflardan siz sorumlusunuz.

                    </p>

                    <p>

                        Müşteri hizmetleri soruları veya anlaşmazlıkları için support@eloboosttr.com adresinden e-posta yoluyla bizimle iletişime geçebilirsiniz. PayTR ödeme servis sağlayıcı ödemesi yoluyla yapılan ödemelerle ilgili sorular paytr@hs01@kep.tr adresine iletilmelidir. Mümkün olduğunda, satın alımınızdan kaynaklanan tüm anlaşmazlıkları çözmek için sizinle ve/veya web sitemizde satış yapan herhangi bir Kullanıcı ile birlikte çalışacağız.

                    </p>

                </div>



            </section>





        <!--Join Us Section Start---------------------------------------------------------------------------------------------------------------------------------------------------------------------->



        </main>



        <footer class="footer">

            <div class="subscribe flex">

                <h2>Subscribe To Our <span>Newsletter</span></h2>

                <div class="form-email">

                    <input type="text" class="enteremail" placeholder="Enter your email address" />

                    <span class="highlight"></span>

                </div>

                <input type="button" class="btn btn--primary subbutton" value="Subscribe" />

            </div>

            <div class="footer__wrapper">

                <img src="images/garen.png" class="footer__mascot" />

                <div class="footer__widgets flex">

                    <div class="footer__widget footer__widget--about">

                        <h4 class="footer__widget__header">About Us</h4>

                        <p class="footer__about__text">EloBoostTR is a veteran League of Legends elo boosting website. We require each booster to undergo verification so we know your account is in good hands.

                            We offer an array of services,

                            including smurf accounts and coaching. If you’re looking for the cheapest boosting available, you’ve come to the right place!</p>

                        <div class="footer__about__info">

                            EloBoostTR is in no way affiliated with, associated with or endorsed by Riot Games, Inc or League of Legends, TM

                        </div>

                    </div>



                    <div class="footer__widget footer__widget--list">

                        <h4 class="footer__widget__header">Useful Links</h4>

                        <ul class="footer__widget__list">

                            <li><a href="about" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> About Us</a></li>

                            <li><a href="join_us" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> Join Us</a></li>

                            <li><a href="terms_of_service" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> Terms of Service</a></li>

                            <li><a href="privacy_policy" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> Privacy Policy</a></li>

                        </ul>

                    </div>



                    <div class="footer__widget footer__widget--contact">

                        <h4 class="footer__widget__header">Contact</h4>

                        <ul class="list-inline mb-20">

                      <ul class="list-inline mb-20" style="margin-top:50px;">

                      <li class="btn btn--primary btn--block footer_widget_contact_list_element" style="padding: 0.5rem 1.75rem;width: 15rem;text-align: left;"><i class="fa fa-map-marker"></i>  Çankaya, Ankara</li>

                      <li class="btn btn--primary btn--block footer_widget_contact_list_element" style="padding: 0.5rem 1.75rem;width: 15rem;text-align: left;"><i class="fa fa-envelope-o"></i>  support@eloboosttr.com</li>

                      <li class="btn btn--primary btn--block footer_widget_contact_list_element" style="font-size: 0.735rem;padding: 0.5rem 1.75rem;width: 15rem;text-align: left;"><i class="fas fa-balance-scale-right"></i> Vergi No: 1180343860 / Çankaya</li>

                    </ul>

                    </ul>

                    </div>

                </div>

            </div>

            <div class="footer__copyright">Copyright © 2021 <span class="text__highlight"><b>EloBoostTR</b></span>. All Rights Reserved.</div>

        </footer>

<!-- FOOTER END ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>





        <!-- SCRIPTS START ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>



        <!--Start of Tawk.to Script-->

        <script type="text/javascript" src="js/tawkTojs.js"></script>

        <!--End of Tawk.to Script-->



    </body>

</html>

