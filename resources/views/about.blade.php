<x-layout>

    <section class="section">
        <div class="section__wrapper">
            <header class="section__header flex">
                <h2 class="section__header__title">About <span>Page</span></h2>
                <p class="section__header__sub"><a href="/">Home</a> / <span style="color: #ff4719;">About</span></p>
            </header>
            <section class="about flex">
                <p class="about__text">
                    Eloboosts.com is a veteran League of Legends elo boosting website. We require each booster to undergo verification so we know your account is in good hands.
                    We offer an array of services, including smurf accounts and coaching. If you’re looking for the cheapest boosting available, you’ve come to the right place!
                </p>
                <img class="about__image" src="https://lolboost.novundev.com/img/domain-search-img/01.png" />
            </section>
        </div>
    </section>

    @include('includes/statistics');
    @include('includes/user-feedback');
    @include('includes/global-features');
</x-layout>