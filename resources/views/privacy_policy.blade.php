<!DOCTYPE html>

<html lang="en">

    <head>

        <meta charset="UTF-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>EloBoostTR Best Elo Boosting Services</title>

            

        <script type="text/javascript" src="js/scrollFunction.js"></script>

        <!-------------------------------------------------------------------------------------------------------------------------->

        <link rel='stylesheet' href='css/ExternalCss/bootstrap.min.css' type='text/css' media='all' />

        <link rel='stylesheet' href='css/ExternalCss/hostwhmcs.css' type='text/css' media='all' />  

       

        <!-- External Style Sheets End ----------------------------------------------------------------------------------------------------------------------------------------------------------------->

        <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>

        <script src="https://use.fontawesome.com/d983741785.js"></script>

        <script src="https://kit.fontawesome.com/870b7dbf87.js" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">

        <link href="css/icon-font/lineicons.css" rel="stylesheet">

        <link rel="stylesheet" href="css/style.css">

        <link rel="stylesheet" href="css/calculator.css">

        <link rel="stylesheet" href="css/my_custom_inline_style.css">

    </head>

    <body>

        <header class="header ">

            <div class="header__navbar " id="header__navbar">

                <nav class="navbar flex">

                    <a href="/" class="navbar__brand" style="color:#ff4719;margin-right: 430px;">EloBoostTR</a>



                    <ul class="navbar__list flex" id="navbar__list">

                        <li><a href="/" class="navbar__list__item navbar__list__item--active" style="color:#ff4719;">Home</a></li>

                        <li><a href="boost" class="navbar__list__item">LoL Boosting</a></li>

                        <li><a href="demopage" class="navbar__list__item">Demo Page</a></li>

                        <li><a href="contact_us" class="navbar__list__item">Contact Us</a></li>

                        <li><a href="login" class="navbar__list__button btn btn--primary">Boost Area</a></li>

                    </ul>

                    <button class="hamburger hamburger--collapse" id="header__hamburger" type="button">

                  <span class="hamburger-box">

                    <span class="hamburger-inner"></span>

                  </span>

                    </button>

                </nav>

            </div>

            

        </header>



        <main>

            <!--Join Us Section Start---------------------------------------------------------------------------------------------------------------------------------------------------------------------->



            <div id="pageHeader" class="global ">

                <div class="container">

                    <div class="section--title">

                        <div class="row">

                            <div class="col-md-6">

                                <h2 class="join_us_h2">Privacy Policy<span> Page</span></h2>                

                            </div>

                            

                            <div class="col-md-6 ">

                                <div class="page-header--breadcrumb woo-breadcrumbs">

                                    <ul class="breadcrumb">

                                        <li>

                                            <a href="/" class="home_a">Home</a>

                                        </li>

                                        <li>

                                            Privacy Policy

                                        </li>

                                    </ul>

                                </div> 

                            </div>

                        </div>

                    </div>

                </div>

            </div>



            <section class="tosContainer">

                

                <div class="container">



                    <p>

                        <h2 style="text-align: center;">

                            Gizlilik Sözleşmesi

                            <br />

                            <br />

                        </h2>

                    </p>

                    <p>

                        <h2>

                            <br />

                            Gizlilik ve Güvenlik Politikası:

                        </h2>

                    </p>

                    <p>

                        <strong>EloBoostTR</strong> olarak, gizliliğinizin sizin için önemli olduğunu ve kişisel verilerinizin nasıl kullanıldığını önemsediğinizi anlıyoruz. Bu web sitesini ziyaret eden herkesin mahremiyetine saygı duyuyor ve değer veriyoruz, <strong>https://eloboosttr.com</strong> Sitemiz, kişisel verileri yalnızca burada açıklanan şekillerde ve aşağıdaki yükümlülüklerimiz ve haklarınızla ve kanunlarla tutarlı ve uyumlu bir şekilde toplayacak ve kullanacaktır.

                    </p>

                    <p>

                        Lütfen bu <strong>Gizlilik Politikası</strong>'nı dikkatlice okuyun ve anladığınızdan emin olun. Bu Gizlilik Politikası'nı kabul etmeniz, Sitemizi kullanmaya başladığınız ilk andan itibaren gerçekleşmekte sayılmaktadır. Bu <strong>Gizlilik Politikası</strong>'nı kabul etmiyor ve onaylamıyorsanız, Sitemizi kullanmayı derhal bırakmalısınız. 

                    </p>

                    <p>

                        <h2>

                            <br />

                            Bu <strong>Gizlilik Politikası</strong> Neleri Kapsıyor? 

                        </h2>

                    </p>

                    <p>

                        Bu <strong>Gizlilik Politikası</strong> yalnızca Sitemiz'i kullanımınız için geçerlidir. Sitemiz diğer web sitelerine bağlantılar içerebilir. Verilerinizin diğer web siteleri tarafından nasıl toplandığı, saklandığı veya kullanıldığı üzerinde hiçbir kontrolümüz olmadığını lütfen unutmayın. Bu tür web sitelerine herhangi bir veri sağlamadan önce gizlilik politikalarını kontrol etmenizi tavsiye ederiz. 

                    </p>

                    <p>

                        <h2>

                            <br />

                            Hangi Verileri Topluyoruz? 

                        </h2>

                    </p>

                    <p>

                        Sitemizi kullanımınıza bağlı olarak, aşağıdaki kişisel ve kişisel olmayan verilerin bir kısmını veya tamamını toplayabiliriz.

                    </p>

                    <p>

                        <strong>[</strong>İsim;<strong>]</strong>

                    </p>

                    <p>

                        <strong>[</strong>E-posta adresi;<strong>]</strong>

                    </p>

                    <p>

                        <strong>[</strong>Ödeme bilgileri;<strong>]</strong>

                    <p>

                        <strong>[</strong>IP adresi;<strong>]</strong>

                    </p>

                    <p>

                        <strong>[</strong>Yaş;<strong>]</strong>

                    </p>

                    <p>

                        <strong>[</strong>Ülke;<strong>]</strong>

                    </p>

                    <p>

                        <strong>[</strong>Diller;<strong>]</strong>

                    </p>

                    <p>

                        <strong>[</strong>League of Legends oyunuyla ilgili "Potansiyel Güçlendirici(Booster)" hakkında bilgi.<strong>]</strong>

                    </p>

                    <p>

                        <h2>

                            <br />

                            Kişisel Verilerimi Ne Kadar Süreyle Tutacaksınız? 

                        </h2>

                    </p>

                    <p>

                        Kişisel verilerinizi, ilk toplanma neden(ler)inin ışığında gerekli olandan daha uzun süre saklamayacağız. Bu nedenle kişisel verileriniz aşağıdaki süreler boyunca saklanacaktır:

                    </p>

                    <p>

                        <strong>-</strong> <strong>[</strong>E-posta; bu bilgiler sadece web sitemizde bulunan bir hesap için kullanılacaktır. Bilgiler, yalnızca müşteri, hesaptaki son etkinliğinden 5 yıl sonra hesabına giriş yapmazsa kaldırılacaktır.<strong>]</strong>

                    </p>

                    <p>

                        <strong>-</strong> <strong>[</strong>Ad; bu bilgiler yalnızca Güçlendirici(Booster)'lerimize ödeme göndermek için kullanılacaktır. Bu bilgiler, alakalı şahıs için son ödeme yaptıldıktan sonra 2 yıl süreyle veri tabanımızda tutulacaktır.<strong>]</strong>

                    </p>

                    <p>

                        <strong>-</strong> <strong>[</strong>Ödeme bilgileri; ödeme bilgileri sadece Güçlendirici(Booster)'lerimize ödeme göndermek için kullanılacaktır. Bu bilgiler, alakalı şahıs için son ödeme yapıldıktan sonra 2 yıl süreyle veri tabanımızda tutulacaktır.<strong>]</strong>

                    </p>

                    <p>

                        <strong>-</strong> <strong>[</strong>IP adresi; bu bilgiler yalnızca tüketicilerimize hizmet sağlamak için kullanılacaktır. Bu bilgiler hizmet tamamlandıktan 1 yıl sonra silinecektir.<strong>]</strong>

                    </p>

                    <p>

                        <strong>-</strong> <strong>[</strong>Yaş; bu özel bilgiler yalnızca iş başvuru sürecinde kullanılacaktır. İş başvurusu talebini aldıktan 2 yıl sonra tüm iş başvuruları veri tabanımızdan kaldırılacaktır.<strong>]</strong>

                    </p>

                    <p>

                        <strong>-</strong> <strong>[</strong>Ülke; bu özel bilgiler yalnızca iş başvuru sürecinde kullanılacaktır. İş başvurusu talebini aldıktan 2 yıl sonra tüm iş başvuruları veri tabanımızdan kaldırılacaktır.<strong>]</strong>

                    </p>

                    <p>

                        <strong>-</strong> <strong>[</strong>Diller; bu özel bilgiler yalnızca iş başvuru sürecinde kullanılacaktır. İş başvurusu talebini aldıktan 2 yıl sonra tüm iş başvuruları veri tabanımızdan kaldırılacaktır.<strong>]</strong>

                    </p>

                    <p>

                        <strong>-</strong> <strong>[</strong>League of Legends oyunuyla ilgili “Potansiyel Güçlendirici(Booster)” hakkında bilgi; League of Legends ile ilgili bu özel içerik yalnızca iş başvuru sürecinde kullanılacaktır. İş başvurusu talebini aldıktan 2 yıl sonra tüm iş başvuruları veri tabanımızdan kaldırılacaktır.<strong>]</strong> 

                    </p>

                    <p>

                        <h2>

                            <br />

                            Bilgileri Saklayabilir Miyim?

                        </h2>

                    </p>

                    <p>

                        Sitemize herhangi bir kişisel veri sağlamadan erişebilirsiniz. Ancak, Sitemizde bulunan tüm özellikleri ve işlevleri kullanmak için belirli verilerin toplanmasına izin vermeniz veya göndermeniz gerekebilir.

                    </p>

                    <p>

                        Çerez(Cookie) kullanımımızı kısıtlayabilirsiniz.

                    </p>

                    <p>

                        <h2>

                            <br />

                            Kredi Kartı Bilgileri Güvenliği:

                        </h2>

                    </p>

                    <p>

                        Kredi kartı güvenliğinizi en az sizin kadar firmamız da önemsemektedir. Firmamız ödeme işlemlerinin gerçekleştirilebilmesi için Türkiye'nin en güvenilen ödeme aracı sistemlerini sunan, yöneten ve işleten <strong>PayTR Ödeme Sağlayıcısı</strong> ile birlikte çalışmaktadır.

                    </p>

                    <p>

                            Biz, <strong>PayTR Ödeme Sağlayıcısı</strong>'nın sunduğu ödeme sistemleri olanaklarını yalnızca sitemize entegre ederiz. Sizin ilgili bölümlere ÖDEME YAPMAK MAKSADI ile girdiğiniz bilgilerin tarafımızca saklanması/depolanması bir yana, görüntülenmesi dahi mümkün değildir. Alakalı bölümlere girdiğiniz kredi kartı bilgileriniz sitemize entegrasyonunu gerçekleştirdiğimiz <strong>PayTR Ödeme Sağlayıcısı</strong> altyapısı aracılığı ile onlar tarafına <strong>128 bit SSL(Secure Socket Layer) protokolü</strong> ile şifrelenerek, onlar tarafından da ilgili banka tarafına aktarılır ve girilen bilgiler için ödeme onayı verilmesi durumunda onay bilgisi tarafımıza aktarılır. Bu aşamada ödemenin başarılı bir şekilde gerçekleştiğini varsayar ve satın alımı gerçekleştirilen servis/hizmetleri size büyük bir özen içerisinde sunarız.

                    </p>

                    <p>

                            Web sitemizin gerek kullanım koşulları sayfasını, gerek iletişim sayfasını, gerekse alakalı bir çok bölümünü incelediğinizde resmi şirket adımız, kayıtlı(resmi) şirket adresimiz ve yine kayıtlı resmi Vergi NO'muz ve mükellefi olduğumuz Vergi Dairesi Müdürlüğü bilgisi gibi bilgilere erişebildiğinizi görürsünüz. İstediğiniz her an ilgili bilgiler aracılığı ile bizimle iletişim kurabilir, güvenilirliğimizi ve yasallığımızı teyit edebilirsiniz.

                    </p>

                    <p>

                        <strong>Not:</strong> İnternet üzerinden kullanacağınız hizmetlerde de öncelikli olarak dikkat etmeniz/kontrol etmeniz gereken olay budur. Kendisi/Şirketi hakkında elle tutulur ve yasal iletişim bilgileri sunmayan firmalara itibar etmeyiniz!

                    </p>

                    <p>

                        <h2>

                            <br />

                            İstisnai Haller:

                        </h2>

                    </p>

                    <p>

                        Aşağıda belirtilen istisnai durumlarda Firmamız, işbu "Gizlilik Politikası" hükümleri dışında kullanıcılara ait bilgileri üçüncü kişilere açıklayabilir. Bu durumlara şunlar örnek olarak verilebilir;

                    </p>

                    <p>

                        <strong>-</strong> Kanun, Kanun Hükmünde Kararname, Yönetmelik vb. yetkili hukuki otorite tarafından çıkarılan ve yürürlülükte olan hukuk kurallarının getirdiği bir durum,

                    </p>

                    <p>

                        <strong>-</strong> Mağazamızın kullanıcıları ile akdedilen "Kullanım Koşulları"nın ve diğer anlaşmaların gerekliliklerini yerine getirmek ve bunları uygulamaya koymak gerekliliği durumu,

                    </p>

                    <p>

                        <strong>-</strong> Yetkili idari ve adli otorite tarafından usulüne göre yürütülen bir araştırma veya soruşturmanın yürütümü amacıyla kullanıcılarla ilgili bilgi talep edilmesi durumu,

                    </p>

                    <p>

                        <strong>-</strong> Kullanıcıların hakları veya güvenliklerini korumak için bilgi vermenin gerekli görüldüğü haller.

                    </p>

                    <p>

                        <h2>

                            <br />

                            Çocuk Gizliliği:

                        </h2>

                    </p>

                    <p>

                        Firmamız, 16 yaşını doldurmamış veya diğer ülkelerin kanunları çerçevesinde belirlenmiş olan "Çocuk" olarak tanımlanabilecek yaşta olan herhangi bir bireyden kasıtlı olarak kişisel bilgi toplamamaktadır.

                    </p>

                    <p>

                            <strong>-</strong> Eğer siz alakalı ülkedeki çocuk yaşta olan kullanıcılarımızın tespitini gerçekleştirmiş olan bir ebeveyn iseniz, lütfen bizimle bu Gizlilik Sözleşmesi'nin, "İletişim" bölümünde bulunan bilgiler aracılığı ile bizimle iletişime geçin ve olayın çözümü için(Şayet var ise, elimizdeki bütün bilgilerin silinimi, ilgili Kullanıcı Hesabı'nın siteye erişiminin kısıtlanması vb.) harekete geçelim.

                    </p>

                    <p>

                            <strong>-</strong> Eğer siz, ülkenizdeki yasalar çerçevesinde "Çocuk" olarak tabir edilen bir yaşta iseniz, öncesinde bizimle sizin hakkınızda toplanmış bilgilerin tarafımızca silinmesi ve hesabınızın kısıtlanması/kaldırılması adına iletişime geçmeniz tavsiye olunur, sonrasında da derhal bu Web Sitesi'nin kullanımını  kati surette bırakmanızın gerekliliği ve önemliliği belirtilir.

                    </p>

                    <p>

                        <h2>

                            <br />

                            Çerez(Cookie)'leri Nasıl Kullanıyoruz?

                        </h2>

                    </p>

                    <p>

                        Sitemiz, bilgisayarınıza veya cihazınıza belirli birinci taraf Çerezler yerleştirebilir ve bunlara erişebilir. Birinci taraf Çerezler, doğrudan bizim tarafımızdan yerleştirilenlerdir ve yalnızca bizim tarafımızdan kullanılır. Çerezleri Sitemizdeki deneyiminizi kolaylaştırmak ve iyileştirmek ve hizmetlerimizi ve ürünlerimizi sağlamak ve geliştirmek için kullanıyoruz. Sitemizi kullanarak, bilgisayarınıza veya cihazınıza belirli üçüncü taraf Çerezleri de alabilirsiniz. Üçüncü Taraf Çerezleri, bizim dışımızdaki web siteleri, hizmetler ve/veya taraflarca yerleştirilen çerezlerdir. Sitemizde reklam ve analiz hizmetleri için Üçüncü Kişi Çerezleri kullanılmaktadır ve bunlar Çerezleri de kullanmaktadır. Web sitesi analitiği, kullanım istatistiklerini toplamak ve analiz etmek için kullanılan ve insanların Sitemizi nasıl kullandığını daha iyi anlamamızı sağlayan bir dizi araç anlamına gelir.

                    </p>

                    <p>

                        <h2>

                            <br />

                            Bu Gizlilik Politikasındaki Değişiklikler:

                        </h2>

                    </p>

                    <p>

                        Bu Gizlilik Bildirimini zaman zaman değiştirebiliriz. Bu, örneğin yasa değişirse veya işimizi kişisel verilerin korunmasını etkileyecek şekilde değiştirirsek gerekli olabilir.

                        Herhangi bir değişiklik anında Sitemizde yayınlanacak ve değişiklikleri takiben Sitemizi ilk kullanımınızda Gizlilik Politikası şartlarını kabul etmiş sayılacaksınız. Güncel kalmak için bu sayfayı düzenli olarak kontrol etmenizi öneririz.

                    </p>

                    <p>

                        <h2>

                            İletişim:

                        </h2>

                    </p>

                    <p>

                        Şirketimiz, <strong>KAVAKLIDERE VERGİ DAİRESİ MÜDÜRLÜĞÜ</strong>, 1180343860 vergi NO'lu mükellefidir.

                    </p>

                    <p>

                        Gizlilik politikamız hakkında herhangi türden sorunuz, kafanıza takılan herhangi konu veya merak ettiğiniz alakalı durumlar ile ilgili bizimle aşağıdaki şekillerde iletişim kurabilirsiniz;

                    </p>

                    <p>

                        <strong>-</strong> Canlı Destek(Web Sitesi'nin hemen her sayfasında sağ altta kullanımı kolay ve Web Site'miz canlı destek yetkilimiz ile anlık erişim imkanı sağlayan bir sekmedir) aracılığı ile,

                    </p>

                    <p>

                        <strong>-</strong> Web Site'miz sayfalarından <strong>"Bize Ulaşın"</strong> veya benzeri şekilde isimlendirilmiş olan sayfa içerisindeki iletişim formunu alakalı şekilde doldurup, formu göndererek,

                    </p>

                    <p>

                        <strong>-</strong> Web Site'mizin resmi e-posta adresi olan: <strong>support@eloboosttr.com</strong> adresine alakalı konu başlığı ve açıklama ile e-posta göndererek,

                    </p>

                    <p>

                        <strong>-</strong> Kayıtlı(resmi) şirket posta adresimiz olan;

                    </p>

                    <p>

                            &nbsp;&nbsp;&nbsp;&nbsp;<strong>*</strong> Kavaklıdere Mahallesi, Esat Caddesi,

                    </p>

                    <p>

                            &nbsp;&nbsp;&nbsp;&nbsp;<strong>*</strong> Bina NO: 12, İç Kapı NO: 1,

                    </p>

                    <p>

                            &nbsp;&nbsp;&nbsp;&nbsp;<strong>*</strong> ÇANKAYA/ ANKARA.

                    </p>

                    <p>

                        adresine iletmek istediğiniz mesajı uygunca hazırlanmış fiziksel mektup veya diğer alakalı formlarda ileterek.

                    </p>

                </div>



            </section>





        <!--Join Us Section Start---------------------------------------------------------------------------------------------------------------------------------------------------------------------->



        </main>



        <footer class="footer">

            <div class="subscribe flex">

                <h2>Subscribe To Our <span>Newsletter</span></h2>

                <div class="form-email">

                    <input type="text" class="enteremail" placeholder="Enter your email address" />

                    <span class="highlight"></span>

                </div>

                <input type="button" class="btn btn--primary subbutton" value="Subscribe" />

            </div>

            <div class="footer__wrapper">

                <img src="images/garen.png" class="footer__mascot" />

                <div class="footer__widgets flex">

                    <div class="footer__widget footer__widget--about">

                        <h4 class="footer__widget__header">About Us</h4>

                        <p class="footer__about__text">EloBoostTR is a veteran League of Legends elo boosting website. We require each booster to undergo verification so we know your account is in good hands.

                            We offer an array of services,

                            including smurf accounts and coaching. If you’re looking for the cheapest boosting available, you’ve come to the right place!</p>

                        <div class="footer__about__info">

                            EloBoostTR is in no way affiliated with, associated with or endorsed by Riot Games, Inc or League of Legends, TM

                        </div>

                    </div>



                    <div class="footer__widget footer__widget--list">

                        <h4 class="footer__widget__header">Useful Links</h4>

                        <ul class="footer__widget__list">

                            <li><a href="about" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> About Us</a></li>

                            <li><a href="join_us" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> Join Us</a></li>

                            <li><a href="terms_of_service" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> Terms of Service</a></li>

                            <li><a href="privacy_policy" class="footer__widget__list__item"><i class="lni lni-angle-double-right"></i> Privacy Policy</a></li>

                        </ul>

                    </div>



                    <div class="footer__widget footer__widget--contact">

                        <h4 class="footer__widget__header">Contact</h4>

                        <ul class="list-inline mb-20">

                      <ul class="list-inline mb-20" style="margin-top:50px;">

                      <li class="btn btn--primary btn--block footer_widget_contact_list_element" style="padding: 0.5rem 1.75rem;width: 15rem;text-align: left;"><i class="fa fa-map-marker"></i>  Çankaya, Ankara</li>

                      <li class="btn btn--primary btn--block footer_widget_contact_list_element" style="padding: 0.5rem 1.75rem;width: 15rem;text-align: left;"><i class="fa fa-envelope-o"></i>  support@eloboosttr.com</li>

                      <li class="btn btn--primary btn--block footer_widget_contact_list_element" style="font-size: 0.735rem;padding: 0.5rem 1.75rem;width: 15rem;text-align: left;"><i class="fas fa-balance-scale-right"></i> Vergi No: 1180343860 / Çankaya</li>

                    </ul>

                    </ul>

                    </div>

                </div>

            </div>

            <div class="footer__copyright">Copyright © 2021 <span class="text__highlight"><b>EloBoostTR</b></span>. All Rights Reserved.</div>

        </footer>

<!-- FOOTER END ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>





        <!-- SCRIPTS START ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>



        <!--Start of Tawk.to Script-->

        <script type="text/javascript" src="js/tawkTojs.js"></script>

        <!--End of Tawk.to Script-->



    </body>

</html>

